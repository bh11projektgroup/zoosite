/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.utilities.mapper;

import java.time.LocalDateTime;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.entity.Image;
import repository.entity.Program;
import service.dto.ImageDTO;
import service.dto.ProgramDTO;

/**
 *
 * @author Kenwulf
 */
@ExtendWith(MockitoExtension.class)
public class ProgramMapperTest {

    private Program entity;
    private ProgramDTO dto;
    private ImageDTO imageDto;
    private Image image;
    @Mock
    private ImageMapper mapper;

    @InjectMocks
    private ProgramMapper underTest;

    @BeforeEach
    void init() {
        Long id = 1L;
        String name = "name";
        String description = "description";
        String location = "location";
        LocalDateTime startTime = LocalDateTime.MIN;
        String duration = "duration";
        String pictureUri = "pictureUri";
        int capacity = 100;
        int ticketPrice = 100;

        dto = new ProgramDTO();
        dto.setCapacity(capacity);
        dto.setDescription(description);
        dto.setDuration(duration);
        dto.setId(id);
        dto.setLocation(location);
        dto.setName(name);
        dto.setPictureUri(pictureUri);
        dto.setStartTime(startTime);
        dto.setTicketPrice(ticketPrice);
        imageDto = new ImageDTO();
        dto.setImageDto(imageDto);

        entity = new Program();
        entity.setCapacity(capacity);
        entity.setDescription(description);
        entity.setDuration(duration);
        entity.setId(id);
        entity.setLocation(location);
        entity.setName(name);
        entity.setPictureUri(pictureUri);
        entity.setStartTime(startTime);
        entity.setTicketPrice(ticketPrice);
        image = new Image();
        entity.setImage(image);
    }

    @Test
    void mapToEntityTest() {
        //given
        Mockito.when(mapper.mapToEntity(imageDto)).thenReturn(image);
        //when
        Program result = underTest.mapToEntity(dto);
        //then
        assertEquals(entity, result);
    }

    @Test
    void mapToDTOTest() {
        //given
        Mockito.when(mapper.mapToDTO(image)).thenReturn(imageDto);
        //when
        ProgramDTO result = underTest.mapToDTO(entity);
        //then
        assertEquals(dto, result);
    }
}
