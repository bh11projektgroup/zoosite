/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.utilities.mapper;

import java.time.LocalDateTime;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.entity.User;
import service.dto.UserDTO;

/**
 *
 * @author Kenwulf
 */
@ExtendWith(MockitoExtension.class)
public class UserMapperTest {

    private User entity;
    private UserDTO dto;

    @InjectMocks
    private UserMapper underTest;

    @BeforeEach
    void init() {
        Long id = 1L;
        String accountName = "accountName";
        String address = "address";
        String city = "city";
        String email = "email";
        String imageUri = "imageUri";
        String name = "name";
        String password = "password";
        String postalCode = "postalCode";
        LocalDateTime timeOfRegistration = LocalDateTime.MIN;

        dto = new UserDTO();
        dto.setAccountName(accountName);
        dto.setAddress(address);
        dto.setCity(city);
        dto.setEmail(email);
        dto.setId(id);
        dto.setImageUri(imageUri);
        dto.setName(name);
        dto.setPassword(password);
        dto.setPostalCode(postalCode);
        dto.setTimeOfRegistration(timeOfRegistration);

        entity = new User();
        entity.setAccountName(accountName);
        entity.setAddress(address);
        entity.setCity(city);
        entity.setEmail(email);
        entity.setId(id);
        entity.setImageUri(imageUri);
        entity.setName(name);
        entity.setPassword(password);
        entity.setPostalCode(postalCode);
        entity.setTimeOfRegistration(timeOfRegistration);
    }

    @Test
    void mapToEntityTest() {
        //given
        //when
        User result = underTest.mapToEntity(dto);
        //then
        assertEquals(entity, result);
    }

    @Test
    void mapToDTOTest() {
        //given
        //when
        UserDTO result = underTest.mapToDTO(entity);
        //then
        assertEquals(dto, result);
    }
}
