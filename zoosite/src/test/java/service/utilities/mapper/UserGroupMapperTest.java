/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.utilities.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.entity.UserGroup;
import service.dto.UserGroupDto;

/**
 *
 * @author Kenwulf
 */
@ExtendWith(MockitoExtension.class)
public class UserGroupMapperTest {

    private UserGroup entity;
    private UserGroupDto dto;

    @InjectMocks
    private UserGroupMapper underTest;

    @BeforeEach
    void init() {
        Long id = 1L;
        String accountName = "accountName";
        String groupName = "groupName";

        dto = new UserGroupDto();
        dto.setAccountName(accountName);
        dto.setGroupName(groupName);
        dto.setId(id);

        entity = new UserGroup();
        entity.setAccountName(accountName);
        entity.setGroupName(groupName);
        entity.setId(id);
    }

    @Test
    void mapToEntityTest() {
        //given
        //when
        UserGroup result = underTest.mapToEntity(dto);
        //then
        assertEquals(entity, result);
    }

    @Test
    void mapToDTOTest() {
        //given
        //when
        UserGroupDto result = underTest.mapToDTO(entity);
        //then
        assertEquals(dto, result);
    }

}
