/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.utilities.mapper;

import java.time.LocalDateTime;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.entity.ZooInfo;
import service.dto.ZooInfoDTO;

/**
 *
 * @author Kenwulf
 */
@ExtendWith(MockitoExtension.class)
public class ZooInfoMapperTest {

    private ZooInfoDTO dto;
    private ZooInfo entity;

    @InjectMocks
    private ZooInfoMapper underTest;

    @BeforeEach
    void init() {
        Long id = 1L;
        String address = "address";
        String description = "description";
        String email = "email";
        String openingHours = "openingHours";
        String phone = "phone";

        dto = new ZooInfoDTO();
        dto.setAddress(address);
        dto.setDescription(description);
        dto.setEmail(email);
        dto.setId(id);
        dto.setOpeningHours(openingHours);
        dto.setPhone(phone);

        entity = new ZooInfo();
        entity.setAddress(address);
        entity.setDateOfCreation(LocalDateTime.MIN);
        entity.setDescription(description);
        entity.setEmail(email);
        entity.setId(id);
        entity.setOpeningHours(openingHours);
        entity.setPhone(phone);
    }

    @Test
    void mapToEntityTest() {
        //given
        //when
        ZooInfo result = underTest.mapToEntity(dto);
        //this is done by JPA
        result.setDateOfCreation(LocalDateTime.MIN);
        //then
        assertEquals(entity, result);
    }

    @Test
    void mapToDTOTest() {
        //given
        //when
        ZooInfoDTO result = underTest.mapToDTO(entity);
        //then
        assertEquals(dto, result);
    }
}
