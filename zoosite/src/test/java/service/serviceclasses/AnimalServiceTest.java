package service.serviceclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.entity.Animal;
import repository.repositoryclasses.AnimalDAO;
import service.dto.AnimalDTO;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import org.mockito.InjectMocks;
import service.exceptions.AnimalNotFoundException;
import service.utilities.mapper.AnimalMapper;

/**
 *
 * @author Niki
 */
@ExtendWith(MockitoExtension.class)
public class AnimalServiceTest {

    @Mock
    private AnimalDAO dao;
    @Mock
    private Animal entity;
    @Mock
    private AnimalDTO dto;
    @Mock
    private AnimalMapper mapper;

    @InjectMocks
    private final AnimalService underTest = new AnimalService();

    @Test
    void testGetAllAnimalsWhenDaoReturnsEmptyList() {
        // Given
        when(dao.findAll()).thenReturn(new ArrayList<>());
        // When
        List<AnimalDTO> animals = underTest.getAllAnimals();
        // Then
        assertEquals(new ArrayList<>(), animals);
    }

    @Test
    void testGetAllAnimalsWhenDaoReturns10Elements() {
        // Given
        List<Animal> entityList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            entityList.add(entity);
        }
        when(dao.findAll()).thenReturn(entityList);
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        List<AnimalDTO> dtoList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            dtoList.add(dto);
        }
        // When
        List<AnimalDTO> animals = underTest.getAllAnimals();
        // Then
        assertEquals(dtoList, animals);
    }

    @Test
    void testGetAnimalByIdWhenReturnsAnimal() {
        // Given
        Long id = 1L;
        when(dao.findById(id)).thenReturn(Optional.of(entity));
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        // When
        AnimalDTO result = underTest.getAnimalById(id);
        // Then
        assertEquals(dto, result);
    }

    @Test
    void testGetAnimalByIdWhenNoResult() {
        // Given
        Long id = 1L;
        when(dao.findById(id)).thenReturn(Optional.empty());
        // When        
        // Then
        assertThrows(AnimalNotFoundException.class,
                () -> {
                    underTest.getAnimalById(id);
                });
    }

    @Test
    void shouldDeleteByIDReturnFalseWhenDeletionFailed() {
        //given
        Long id = 1L;
        when(dao.existsById(id)).thenReturn(true);
        //when
        Boolean result = underTest.deleteById(id);
        //then
        verify(dao).deleteById(id);
        assertFalse(result);
    }

    @Test
    void shouldDeleteByIDReturnTrueWhenDeletionSucceeded() {
        //given
        Long id = 1L;
        when(dao.existsById(id)).thenReturn(false);
        //when
        Boolean result = underTest.deleteById(id);
        //then
        verify(dao).deleteById(id);
        assertTrue(result);
    }

    @Test
    void shouldSaveReturnTrueWhenPersistenceSuccess() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        when(dao.save(entity)).thenReturn(entity);
        //when
        AnimalDTO result = underTest.save(dto);
        //then
        assertNotNull(result);
    }

    @Test
    void shouldSaveReturnTrueWhenPersistenceFailed() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(dao.save(entity)).thenReturn(null);
        //when
        AnimalDTO result = underTest.save(dto);
        //then
        assertNull(result);
    }
}
