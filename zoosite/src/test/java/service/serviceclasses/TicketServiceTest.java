/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.serviceclasses;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.repositoryclasses.TicketDAO;
import service.dto.ProgramDTO;
import service.dto.UserDTO;
import service.utilities.mapper.TicketMapper;
import service.utilities.TicketPDFGenerator;

/**
 *
 * @author tamas
 */
@ExtendWith(MockitoExtension.class)
public class TicketServiceTest {

    private UserDTO costumerDTO;
    private ProgramDTO programDTO;
    @Mock
    private TicketDAO dao;
    @Mock
    private ProgramService pSer;
    @Mock
    private UserService uSer;
    @Mock
    private TicketPDFGenerator tGen;
    @Mock
    private TicketMapper mapper;

    @InjectMocks
    private TicketService underTest;

    @BeforeEach
    void init() {

    }

    @Test
    void generateTicketsInPDFWithValidParamsForSingleEvenAndSiingleTicket() {
        
    }
}
