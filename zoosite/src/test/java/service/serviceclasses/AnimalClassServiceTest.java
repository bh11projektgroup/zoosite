package service.serviceclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.entity.AnimalClass;
import repository.repositoryclasses.AnimalClassDAO;
import service.dto.AnimalClassDTO;
import service.exceptions.AnimalClassNotFoundException;
import service.utilities.mapper.AnimalClassMapper;

/**
 *
 * @author Niki
 */
@ExtendWith(MockitoExtension.class)
public class AnimalClassServiceTest {

    @Mock
    private AnimalClassDAO dao;

    @Mock
    private AnimalClass entity;

    @Mock
    private AnimalClassDTO dto;

    @Mock
    private AnimalClassMapper mapper;

    @InjectMocks
    private AnimalClassService underTest;

    @Test
    void testGetAllAnimalClassesWhenDaoReturnsEmptyList() {
        // Given
        when(dao.findAll()).thenReturn(new ArrayList<>());
        // When
        List<AnimalClassDTO> animalClassDtos = underTest.getAllAnimalClasses();
        // Then
        assertEquals(new ArrayList<>(), animalClassDtos);

    }

    @Test
    void testGetAllAnimalClassesWhenDaoReturns10Element() {
        // Given
        List<AnimalClass> entityList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {

            entityList.add(entity);
        }
        when(dao.findAll()).thenReturn(entityList);
        when(mapper.mapToDTO(entity)).thenReturn(dto);

        List<AnimalClassDTO> dtoList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            dtoList.add(dto);
        }

        // When
        List<AnimalClassDTO> animalClassDtos = underTest.getAllAnimalClasses();

        // Then
        assertEquals(dtoList, animalClassDtos);
    }

    @Test
    void testGetAnimalClassByIdWhenNoResult() {
        // Given
        Long id = 1L;

        when(dao.findById(id)).thenReturn(Optional.empty());
        //When

        //Then
        assertThrows(AnimalClassNotFoundException.class,
                () -> {
                    underTest.getAnimalClassById(id);
                });

    }

    @Test
    void testGetAnimalClassByIdWhenReturnsAnimalClass() {
        // Given
        Long id = 1L;
        when(dao.findById(id)).thenReturn(Optional.of(entity));
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        // When
        AnimalClassDTO result = underTest.getAnimalClassById(id);
        // Then
        assertEquals(dto, result);
    }

    @Test
    void shouldDeleteByIDReturnFalseWhenDeletionFailed() {
        //given
        Long id = 1L;
        when(dao.existsById(id)).thenReturn(true);
        //when
        Boolean result = underTest.deleteById(id);
        //then
        verify(dao).deleteById(id);
        assertFalse(result);
    }

    @Test
    void shouldDeleteByIDReturnTrueWhenDeletionSucceeded() {
        //given
        Long id = 1L;
        when(dao.existsById(id)).thenReturn(false);
        //when
        Boolean result = underTest.deleteById(id);
        //then
        verify(dao).deleteById(id);
        assertTrue(result);
    }

    @Test
    void shouldSaveReturnTrueWhenPersistenceSuccess() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(dao.save(entity)).thenReturn(entity);
        //when
        Boolean result = underTest.save(dto);
        //then
        assertTrue(result);
    }

    @Test
    void shouldSaveReturnTrueWhenPersistenceFailed() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(dao.save(entity)).thenReturn(null);
        //when
        Boolean result = underTest.save(dto);
        //then
        assertFalse(result);
    }
}
