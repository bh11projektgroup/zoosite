package service.serviceclasses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.entity.Image;
import repository.repositoryclasses.ImageDao;
import service.dto.ImageDTO;
import service.utilities.mapper.ImageMapper;

/**
 *
 * @author Niki
 */
@ExtendWith(MockitoExtension.class)
public class ImageServiceTest {
    
    @Mock
    private ImageDao dao;
    
    @Mock
    private ImageMapper mapper;
    
    @InjectMocks
    private ImageService underTest;
    
    private Image entity;
    private ImageDTO dto;
    
    @BeforeEach
    void init() {
        entity = new Image();
        entity.setId(1l);
        dto = new ImageDTO();
        dto.setId(1l);
    }
    
    @Test
    void getImageByIdWithMatchFound() {
        //GIVEN
        long id = 1l;
        when(dao.findById(id)).thenReturn(java.util.Optional.of(entity));
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        //WHEN
        ImageDTO result = underTest.findByID(id);
        // THEN
        assertEquals(entity.getId(), result.getId());
        
    }
}
