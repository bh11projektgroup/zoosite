/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.serviceclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.entity.NewsItem;
import repository.repositoryclasses.NewsItemDAO;
import service.dto.NewsItemDTO;
import service.utilities.mapper.NewsItemMapper;

/**
 *
 * @author Kenwulf
 */
@ExtendWith(MockitoExtension.class)
public class NewsItemServiceTest {

    @Mock
    private NewsItemDAO dao;
    @Mock
    private NewsItemMapper mapper;
    @Mock
    private NewsItem entity;
    @Mock
    private NewsItemDTO dto;

    @InjectMocks
    private NewsItemService underTest;

    @Test
    void TestGetAllWhenDAOReturnsEmptyList() {
        //GIVEN
        List<NewsItem> list = new ArrayList<>();
        when(dao.getNewsItems()).thenReturn(list);
        //WHEN
        List<NewsItemDTO> resultList = underTest.getAll();
        //THEN
        assertEquals(0, resultList.size());
    }

    @Test
    void TestGetAllWhenDAOReturnsListWith4Elements() {
        //GIVEN
        List<NewsItem> list = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            list.add(entity);
        }
        when(dao.getNewsItems()).thenReturn(list);
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        //WHEN
        List<NewsItemDTO> resultList = underTest.getAll();
        //THEN
        assertEquals(4, resultList.size());
    }

    @Test
    void TestGetTop4WhenDAOReturnsEmptyList() {
        //GIVEN
        List<NewsItem> list = new ArrayList<>();
        when(dao.getTop4OrderByCreationTime()).thenReturn(list);
        //WHEN
        List<NewsItemDTO> resultList = underTest.getTop4();
        //THEN
        assertEquals(0, resultList.size());
    }

    @Test
    void TestGetTop4WhenDAOReturnsListWith3Items() {
        //GIVEN
        List<NewsItem> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            list.add(entity);
        }
        when(dao.getTop4OrderByCreationTime()).thenReturn(list);
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        //WHEN
        List<NewsItemDTO> resultList = underTest.getTop4();
        //THEN
        assertEquals(3, resultList.size());
    }

    @Test
    void TestGetSingleNewsWhenDAOReturnsSingleEntity() {
        //GIVEN
        long id = 0L;
        when(dao.findById(id)).thenReturn(Optional.of(entity));
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        //WHEN
        NewsItemDTO result = underTest.getSingleNews(id);
        //THEN
        assertNotNull(result);
    }

    @Test
    void TestGetSingleNewsWhenDAOReturnsNull() {
        //GIVEN
        long id = 0L;
        when(dao.findById(id)).thenReturn(Optional.ofNullable(null));
        //WHEN
        NewsItemDTO result = underTest.getSingleNews(id);
        //THEN
        assertNull(result);
    }

    @Test
    void SaveShouldReturnDTOWhenDTOIsValid() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        when(dao.save(entity)).thenReturn(entity);
        //when
        NewsItemDTO result = underTest.save(dto);
        //then
        assertNotNull(result);
    }

    @Test
    void SaveShouldReturnFalseWhenCouldNotPersistToDB() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(dao.save(entity)).thenReturn(null);
        //when
        NewsItemDTO result = underTest.save(dto);
        //then
        assertNull(result);
    }

    @Test
    void shouldDeleteByIDReturnFalseWhenDeletionFailed() {
        //given
        Long id = 1L;
        when(dao.existsById(id)).thenReturn(true);
        //when
        Boolean result = underTest.deleteById(id);
        //then
        verify(dao).deleteById(id);
        assertFalse(result);
    }

    @Test
    void shouldDeleteByIDReturnTrueWhenDeletionSucceeded() {
        //given
        Long id = 1L;
        when(dao.existsById(id)).thenReturn(false);
        //when
        Boolean result = underTest.deleteById(id);
        //then
        verify(dao).deleteById(id);
        assertTrue(result);
    }

    @Test
    void shouldSaveReturnDTOWhenPersistenceSuccess() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(dao.save(entity)).thenReturn(entity);
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        //when
        NewsItemDTO result = underTest.save(dto);
        //then
        assertNotNull(result);
    }

    @Test
    void shouldSaveReturnTrueWhenPersistenceFailed() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(dao.save(entity)).thenReturn(null);
        //when
        NewsItemDTO result = underTest.save(dto);
        //then
        assertNull(result);
    }
}
