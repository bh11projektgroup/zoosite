package service.serviceclasses;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.entity.ZooInfo;
import repository.repositoryclasses.ZooInfoDAO;
import service.dto.ZooInfoDTO;
import service.utilities.mapper.ZooInfoMapper;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ZooInfoServiceClassTest {

    @Mock
    private ZooInfoDAO dao;

    @Mock
    private ZooInfoMapper mapper;

    @InjectMocks
    private ZooInformationService underTest;

    private ZooInfo entity;
    private ZooInfoDTO dto;

    @BeforeEach
    void init() {
        entity = new ZooInfo();
        entity.setId(1l);
        dto = new ZooInfoDTO();
        dto.setId(1l);
    }

    @Test
    void getZooInfosWithEmptyList() {
        //GIVEN
        List<ZooInfo> list = new ArrayList<>();
        when(dao.findAll()).thenReturn(list);
        //WHEN
        List<ZooInfoDTO> resultList = underTest.getZooInfos();
        //THEN
        assertEquals(0, resultList.size());
    }

    @Test
    void getZooInfosWithOneItem() {
        //GIVEN
        List<ZooInfo> list = new ArrayList<>();
        list.add(entity);
        when(dao.findAll()).thenReturn(list);
        //WHEN
        List<ZooInfoDTO> resultList = underTest.getZooInfos();
        //THEN
        assertEquals(1, resultList.size());
    }

    @Test
    void getZooInfoByIdWithMatchFound() {
        //GIVEN
        long id = 1l;
        when(dao.existsById(id)).thenReturn(true);
        when(dao.findById(id)).thenReturn(java.util.Optional.of(entity));
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        //WHEN
        ZooInfoDTO result = underTest.getZooInfoById(id);
        //THEN
        assertEquals(entity.getId(), result.getId());
    }

    @Test
    void shouldSaveReturnTrueWhenPersistenceSuccess() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(dao.save(entity)).thenReturn(entity);
        //when
        Boolean result = underTest.save(dto);
        //then
        assertTrue(result);
    }

    @Test
    void shouldSaveReturnTrueWhenPersistenceFailed() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(dao.save(entity)).thenReturn(null);
        //when
        Boolean result = underTest.save(dto);
        //then
        assertFalse(result);
    }

    @Test
    void shouldThrowIllegalArgumentWhengetZooinfoByIdFails() {
        //given
        long id = -1L;
        when(dao.existsById(id)).thenReturn(false);
        //when
        //then
        assertThrows(IllegalArgumentException.class, () -> {
            underTest.getZooInfoById(id);
        });
    }
}
