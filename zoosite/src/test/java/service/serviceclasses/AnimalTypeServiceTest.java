/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.serviceclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.entity.AnimalType;
import repository.repositoryclasses.AnimalTypeDAO;
import service.dto.AnimalTypeDTO;
import service.exceptions.AnimalTypeNotFoundException;
import service.utilities.mapper.AnimalTypeMapper;

/**
 *
 * @author Niki
 */
@ExtendWith(MockitoExtension.class)
public class AnimalTypeServiceTest {

    @Mock
    private AnimalTypeDAO dao;

    @Mock
    private AnimalType entity;

    @Mock
    private AnimalTypeDTO dto;

    @Mock
    private AnimalTypeMapper mapper;

    @InjectMocks
    private AnimalTypeService underTest;

    @Test
    void testGetAllAnimalTypesWhenDaoReturnsEmptyList() {
        // Given
        when(dao.findAll()).thenReturn(new ArrayList<>());
        // When
        List<AnimalTypeDTO> animalTypes = underTest.getAllAnimalTypes();
        //Then
        assertEquals(new ArrayList<>(), animalTypes);
    }

    @Test
    void testGetAllAnimalTypesWhenDaoReturns10Elements() {
        // Given
        List<AnimalType> entityList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {

            entityList.add(entity);
        }
        when(dao.findAll()).thenReturn(entityList);
        when(mapper.mapToDTO(entity)).thenReturn(dto);

        List<AnimalTypeDTO> dtoList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            dtoList.add(dto);
        }

        // When
        List<AnimalTypeDTO> animalTypeDtos = underTest.getAllAnimalTypes();

        // Then
        assertEquals(dtoList, animalTypeDtos);
    }

    @Test
    void testGetAnimalTyByIdWhenNoResult() {
        // Given
        Long id = 1L;

        when(dao.findById(id)).thenReturn(Optional.empty());
        // When      

        // Then
        assertThrows(AnimalTypeNotFoundException.class,
                () -> {
                    underTest.getAnimalTypeById(id);
                });
    }

    @Test
    void testGetAnimalTypeByIdWhenReturnsAnimalType() {
        // Given
        Long id = 1L;
        when(dao.findById(id)).thenReturn(Optional.of(entity));
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        // When
        AnimalTypeDTO result = underTest.getAnimalTypeById(id);
        // Then
        assertEquals(dto, result);

    }

    @Test
    void shouldDeleteByIDReturnFalseWhenDeletionFailed() {
        //given
        Long id = 1L;
        when(dao.existsById(id)).thenReturn(true);
        //when
        Boolean result = underTest.deleteById(id);
        //then
        verify(dao).deleteById(id);
        assertFalse(result);
    }

    @Test
    void shouldDeleteByIDReturnTrueWhenDeletionSucceeded() {
        //given
        Long id = 1L;
        when(dao.existsById(id)).thenReturn(false);
        //when
        Boolean result = underTest.deleteById(id);
        //then
        verify(dao).deleteById(id);
        assertTrue(result);
    }

    @Test
    void shouldSaveReturnDTOWhenPersistenceSuccess() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        when(dao.save(entity)).thenReturn(entity);
        //when
        AnimalTypeDTO result = underTest.save(dto);
        //then
        assertNotNull(result);
    }

    @Test
    void shouldSaveReturnTrueWhenPersistenceFailed() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(dao.save(entity)).thenReturn(null);
        //when
        AnimalTypeDTO result = underTest.save(dto);
        //then
        assertNull(result);
    }
}
