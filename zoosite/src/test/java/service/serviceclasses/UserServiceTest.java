/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.serviceclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.commons.codec.digest.DigestUtils;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.entity.User;
import repository.entity.UserGroup;
import repository.repositoryclasses.UserDAO;
import repository.repositoryclasses.UserGroupDao;
import service.dto.UserDTO;
import service.dto.UserGroupDto;
import service.utilities.mapper.UserGroupMapper;
import service.utilities.mapper.UserMapper;

/**
 *
 * @author Kenwulf
 */
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private User entity;
    @Mock
    private UserDTO dto;
    @Mock
    private UserDAO dao;
    @Mock
    private UserMapper mapper;
    @Mock
    private UserGroupDao groupDao;
    @Mock
    private UserGroup groupEntity;
    @Mock
    private UserGroupDto groupDto;
    @Mock
    private UserGroupMapper groupMapper;

    @InjectMocks
    private UserService underTest;

    @Test
    void shouldReturnDTOWhenFindByIDSucceeds() {
        //GIVEN
        long id = 0L;
        when(dao.findById(id)).thenReturn(Optional.of(entity));
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        //WHEN
        UserDTO result = underTest.findByID(id);
        //THEN
        assertEquals(dto, result);
    }

    @Test
    void shouldReturnNullWhenFindByIDFails() {
        //GIVEN
        long id = 0L;
        when(dao.findById(id)).thenReturn(Optional.empty());
        //WHEN
        UserDTO result = underTest.findByID(id);
        //THEN
        assertNull(result);
    }

    @Test
    void shouldDeleteByIDReturnFalseWhenDeletionFailed() {
        //given
        Long id = 1L;
        when(dao.existsById(id)).thenReturn(true);
        //when
        Boolean result = underTest.deleteById(id);
        //then
        verify(dao).deleteById(id);
        assertFalse(result);
    }

    @Test
    void shouldDeleteByIDReturnTrueWhenDeletionSucceeded() {
        //given
        Long id = 1L;
        when(dao.existsById(id)).thenReturn(false);
        //when
        Boolean result = underTest.deleteById(id);
        //then
        verify(dao).deleteById(id);
        assertTrue(result);
    }

    @Test
    void shouldSaveReturnTrueWhenPersistenceSuccess() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(dao.save(entity)).thenReturn(entity);
        //when
        Boolean result = underTest.save(dto);
        //then
        assertTrue(result);
    }

    @Test
    void shouldSaveReturnTrueWhenPersistenceFailed() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(dao.save(entity)).thenReturn(null);
        //when
        Boolean result = underTest.save(dto);
        //then
        assertFalse(result);
    }

    @Test
    void getAllUserWhenDaoReturnEmptyList() {
        //given
        when(dao.findAll()).thenReturn(new ArrayList<>());
        //when
        List<UserDTO> result = underTest.getAllUsers();
        //then
        assertEquals(0, result.size());
    }

    @Test
    void getAllUserWhenDaoReturnNotEmptyList() {
        //given
        List<User> list = new ArrayList<>();
        list.add(entity);
        when(dao.findAll()).thenReturn(list);
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        List<UserDTO> expected = new ArrayList<>();
        expected.add(dto);
        //when
        List<UserDTO> result = underTest.getAllUsers();
        //then
        assertEquals(expected, result);
    }

    @Test
    void TestEncyptToSha256() {
        //given
        String test = "test";
        String expected = DigestUtils.sha256Hex(test);
        //when
        String result = underTest.encryptToSha256(test);
        //then
        assertEquals(expected, result);
    }

    @Test
    void getAllUserGroupsWhenDaoReturnEmptyList() {
        //given
        when(groupDao.findAll()).thenReturn(new ArrayList<>());
        //when
        List<UserGroupDto> result = underTest.getAllUserGroups();
        //then
        assertEquals(0, result.size());
    }

    @Test
    void getAllUserGroupsWhenDaoReturnNotEmptyList() {
        //given
        List<UserGroup> list = new ArrayList<>();
        list.add(groupEntity);
        when(groupDao.findAll()).thenReturn(list);
        when(groupMapper.mapToDTO(groupEntity)).thenReturn(groupDto);
        List<UserGroupDto> expected = new ArrayList<>();
        expected.add(groupDto);
        //when
        List<UserGroupDto> result = underTest.getAllUserGroups();
        //then
        assertEquals(expected, result);
    }

    @Test
    void deleteUserGroupShouldReturnFalseWhenDaoReturnsFalse() {
        //given
        long id = 1L;
        when(groupDao.deleteById(id)).thenReturn(false);
        //when
        boolean result = underTest.deleteUserGroup(id);
        //then
        assertFalse(result);
    }

    @Test
    void deleteUserGroupShouldReturnTrueWhenDaoReturnsTrue() {
        //given
        long id = 1L;
        when(groupDao.deleteById(id)).thenReturn(true);
        //when
        boolean result = underTest.deleteUserGroup(id);
        //then
        assertTrue(result);
    }

    @Test
    void saveUserGroupShouldReturnTrueWhenSaveSuccessful() {
        //given
        when(groupMapper.mapToEntity(groupDto)).thenReturn(groupEntity);
        when(groupDao.findByName(groupDto.getAccountName())).thenReturn(Optional.of(groupEntity));
        //when
        boolean result = underTest.saveUserGroup(groupDto);
        //then
        assertTrue(result);
        Mockito.verify(groupDao).save(groupEntity);
    }
    
    @Test
    void saveUserGroupShouldReturnFalseWhenSaveFails() {
        //given
        when(groupMapper.mapToEntity(groupDto)).thenReturn(groupEntity);
        when(groupDao.findByName(groupDto.getAccountName())).thenReturn(Optional.empty());
        //when
        boolean result = underTest.saveUserGroup(groupDto);
        //then
        assertFalse(result);
        Mockito.verify(groupDao).save(groupEntity);
    }
}
