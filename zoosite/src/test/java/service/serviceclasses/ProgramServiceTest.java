/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.serviceclasses;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.entity.Program;
import repository.repositoryclasses.ProgramDAO;
import service.dto.ProgramDTO;
import service.utilities.mapper.ProgramMapper;

/**
 *
 * @author Kenwulf
 */
@ExtendWith(MockitoExtension.class)
public class ProgramServiceTest {

    @Mock
    private ProgramDAO dao;
    @Mock
    private ProgramDTO dto;
    @Mock
    private Program entity;
    @Mock
    private ProgramMapper mapper;

    @InjectMocks
    private ProgramService underTest;

    @Test
    void shouldReturnDTOWhenFindByIDSucceeds() {
        //GIVEN
        long id = 0L;
        when(dao.findById(id)).thenReturn(Optional.of(entity));
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        //WHEN
        ProgramDTO result = underTest.findByID(id);
        //THEN
        assertEquals(dto, result);
    }

    @Test
    void shouldReturnNullWhenFindByIDFails() {
        //GIVEN
        long id = 0L;
        when(dao.findById(id)).thenReturn(Optional.ofNullable(null));
        //WHEN
        ProgramDTO result = underTest.findByID(id);
        //THEN
        assertNull(result);
    }

    @Test
    void shouldDeleteByIDReturnFalseWhenDeletionFailed() {
        //given
        Long id = 1L;
        when(dao.existsById(id)).thenReturn(true);
        //when
        Boolean result = underTest.deleteById(id);
        //then
        verify(dao).deleteById(id);
        assertFalse(result);
    }

    @Test
    void shouldDeleteByIDReturnTrueWhenDeletionSucceeded() {
        //given
        Long id = 1L;
        when(dao.existsById(id)).thenReturn(false);
        //when
        Boolean result = underTest.deleteById(id);
        //then
        verify(dao).deleteById(id);
        assertTrue(result);
    }

    @Test
    void shouldSaveReturnTrueWhenPersistenceSuccess() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(dao.save(entity)).thenReturn(entity);
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        //when
        ProgramDTO result = underTest.save(dto);
        //then
        assertNotNull(result);
    }

    @Test
    void shouldSaveReturnTrueWhenPersistenceFailed() {
        //given
        when(mapper.mapToEntity(dto)).thenReturn(entity);
        when(dao.save(entity)).thenReturn(null);
        //when
        ProgramDTO result = underTest.save(dto);
        //then
        assertNull(result);
    }

    @Test
    void TestGetProgramsOrderedByDateWithNotEmptyResult() {
        //given
        List<Program> list = new LinkedList<>();
        List<ProgramDTO> expList = new LinkedList<>();
        for (int i = 0; i < 4; i++) {
            list.add(entity);
            expList.add(dto);
        }
        when(dao.getAllProgramsOrderedByDate()).thenReturn(list);
        when(mapper.mapToDTO(entity)).thenReturn(dto);
        //when
        List<ProgramDTO> result = underTest.getProgramsOrderedByDate();
        //then
        assertEquals(expList, result);
    }

    @Test
    void TestGetProgramsOrderedByDateWithEmptyResult() {
        //given
        List<Program> list = new LinkedList<>();
        List<ProgramDTO> expList = new LinkedList<>();
        when(dao.getAllProgramsOrderedByDate()).thenReturn(list);
        //when
        List<ProgramDTO> result = underTest.getProgramsOrderedByDate();
        //then
        assertEquals(expList, result);
    }
}
