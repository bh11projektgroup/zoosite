/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.repositoryclasses;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import repository.CRUDRepository;
import repository.entity.User;

/**
 *
 * @author Kiki
 */
@Stateless
@LocalBean
public class UserDAO implements CRUDRepository<User, Long> {

    @PersistenceContext(unitName = "EL_PU")
    private EntityManager em;

    @Override
    public long count() {
        return 0L;
    }

    @Override
    public void delete(User entity) {
        em.remove(entity);
    }

    @Override
    public void deleteAll() {
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM User").executeUpdate();
    }

    @Override
    public void deleteAll(Iterable<? extends User> entities) {
        List<Long> ids = new LinkedList<>();
        for (User next : entities) {
            if (next == null) {
                throw new IllegalArgumentException();
            }
            ids.add(next.getId());
        }
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM User u WHERE u.id in (:ids)")
                .setParameter("ids", ids)
                .executeUpdate();
    }

    @Override
    public void deleteById(Long id) {
        em.createQuery("DELETE FROM User u WHERE u.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public boolean existsById(Long id) {
        return findById(id).isPresent();
    }

    @Override
    public List<User> findAll() {
        return em.createQuery("SELECT u FROM User u").getResultList();
    }

    @Override
    public Iterable<User> findAllById(Iterable<Long> ids) {
        return em.createQuery("SELECT u FROM User u WHERE u.id in (:ids)")
                .setParameter("ids", ids)
                .getResultList();
    }

    @Override
    public Optional<User> findById(Long id) {
        try {
            User a = (User) em.createQuery("SELECT a FROM User a WHERE a.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
            return Optional.of(a);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public <S extends User> S save(S entity) {
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        if (existsById(entity.getId())) {
            em.merge(entity);
        } else {
            em.persist(entity);
        }
        em.flush();
        Optional<User> n = findById(entity.getId());
        return (S) n.orElse(null);
    }

    @Override
    public <S extends User> Iterable<S> saveAll(Iterable<S> entities) {
        List<S> entityList = new LinkedList<>();
        for (S next : entities) {
            if (next == null) {
                throw new IllegalArgumentException();
            }
            entityList.add(save(next));
        }
        return entityList;
    }

}
