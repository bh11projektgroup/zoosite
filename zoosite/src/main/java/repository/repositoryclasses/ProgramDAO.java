/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.repositoryclasses;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import repository.CRUDRepository;
import repository.entity.Program;

/**
 *
 * @author tamas
 */
@Stateless
@LocalBean
public class ProgramDAO implements CRUDRepository<Program, Long> {

    @PersistenceContext(unitName = "EL_PU")
    private EntityManager em;

    public List<Program> getAllProgramsOrderedByDate() {
        return em.createQuery("SELECT p FROM Program p ORDER BY p.startTime").getResultList();
    }

    @Override
    public long count() {
        return (Long) em.createQuery("SELECT Count (p.id) FROM Program p").getSingleResult();
    }

    @Override
    public void delete(Program entity) {
        em.remove(entity);
    }

    @Override
    public void deleteAll() {
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM Program").executeUpdate();

    }

    @Override
    public void deleteAll(Iterable<? extends Program> entities) {
        List<Long> ids = new LinkedList<>();
        for (Program next : entities) {
            if (next == null) {
                throw new IllegalArgumentException();
            }
            ids.add(next.getId());
        }
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM Program p WHERE p.id in (:ids)")
                .setParameter("ids", ids)
                .executeUpdate();
    }

    @Override
    public void deleteById(Long id) {
        em.createQuery("DELETE FROM Program p WHERE p.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public boolean existsById(Long id) {
        return findById(id).isPresent();
    }

    @Override
    public List<Program> findAll() {
        return em.createQuery("SELECT p FROM Program p").getResultList();
    }

    @Override
    public Iterable<Program> findAllById(Iterable<Long> ids) {
        return em.createQuery("SELECT p FROM Program p WHERE p.id in (:ids)")
                .setParameter("ids", ids)
                .getResultList();
    }

    @Override
    public Optional<Program> findById(Long id) {
        try {
            Program a = (Program) em.createQuery("SELECT a FROM Program a WHERE a.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
            return Optional.of(a);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public <S extends Program> S save(S entity) {
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        if (existsById(entity.getId())) {
            em.merge(entity);
        } else {
            em.persist(entity);
        }
        em.flush();
        Optional<Program> n = findById(entity.getId());
        return (S) n.orElse(null);
    }

    @Override
    public <S extends Program> Iterable<S> saveAll(Iterable<S> entities) {
        List<S> entityList = new LinkedList<>();
        for (S next : entities) {
            if (next == null) {
                throw new IllegalArgumentException();
            }
            entityList.add(save(next));
        }
        return entityList;
    }
}
