/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.repositoryclasses;

import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import repository.entity.UserGroup;

/**
 *
 * @author Kenwulf
 */
@Stateless
@LocalBean
@Slf4j
public class UserGroupDao {

    @PersistenceContext
    private EntityManager em;

    public Optional<UserGroup> findByName(String accountName) {
        UserGroup entity = null;
        try {
            entity = em.createQuery("SELECT e FROM UserGroup e WHERE e.accountName = :name",
                    UserGroup.class)
                    .setParameter("name", accountName)
                    .getSingleResult();
        } catch (NoResultException es) {
            log.debug("No result found!");
        } catch (NonUniqueResultException ex) {
            log.error("Account name was not unique!");
        }
        return Optional.of(entity);
    }

    public UserGroup save(UserGroup entity) {
        if (findByName(entity.getAccountName()).isPresent()) {
            em.merge(entity);
        } else {
            em.persist(entity);
        }
        Optional<UserGroup> result = findByName(entity.getAccountName());
        if (result.isPresent()) {
            return result.get();
        } else {
            return null;
        }
    }

    public List<UserGroup> findAll() {
        return em.createQuery("SELECT e FROM UserGroup e")
                .getResultList();
    }

    public Optional<UserGroup> findById(long id) {
        return Optional.of(em.find(UserGroup.class, id));
    }

    public boolean existsById(long id) {
        return findById(id).isPresent();
    }

    public boolean deleteById(long id) {
        em.createQuery("DELETE FROM UserGroup e WHERE e.id = :id")
                .setParameter("id", id)
                .executeUpdate();
        em.flush();
        return existsById(id);
    }
}
