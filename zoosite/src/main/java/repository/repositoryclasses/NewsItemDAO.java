/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.repositoryclasses;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import repository.CRUDRepository;
import repository.entity.NewsItem;

/**
 *
 * @author tamas
 */
@Startup
@Singleton
@LocalBean
public class NewsItemDAO implements CRUDRepository<NewsItem, Long> {

    @PersistenceContext(unitName = "EL_PU")
    private EntityManager em;

    /**
     * Executes a SELECT statment for ALL the NewsItems in the DB.
     *
     * @return A List with NewsItem entities, or an empty List if there aren't
     * any
     */
    public List<NewsItem> getNewsItems() {
        return em.createNamedQuery("NewsItem.findAll").getResultList();
    }

    /**
     * Executes a SELECT statment for the three newest NewsItems
     *
     * @return A list containing the top three NewsItems, or an empty List if
     * there aren't any
     */
    public List<NewsItem> getTop4OrderByCreationTime() {
        return em.createNamedQuery("NewsItem.Top4OrderByTimeOfCreation")
                .setMaxResults(4)
                .getResultList();
    }

    @Override
    public long count() {
        return (Long) em.createQuery("SELECT Count (n.id) FROM NewsItem n").getSingleResult();
    }

    @Override
    public void delete(NewsItem entity) {
        em.remove(entity);
    }

    @Override
    public void deleteAll() {
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM NewsItem").executeUpdate();

    }

    @Override
    public void deleteAll(Iterable<? extends NewsItem> entities) {
        List<Long> ids = new LinkedList<>();
        for (NewsItem next : entities) {
            if (next == null) {
                throw new IllegalArgumentException();
            }
            ids.add(next.getId());
        }
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM NewsItem n WHERE n.id in (:ids)")
                .setParameter("ids", ids)
                .executeUpdate();

    }

    @Override
    public void deleteById(Long id) {
        em.createQuery("DELETE FROM NewsItem n WHERE n.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public boolean existsById(Long id) {
        return findById(id).isPresent();
    }

    @Override
    public List<NewsItem> findAll() {
        return em.createQuery("SELECT n FROM NewsItem n").getResultList();
    }

    @Override
    public Iterable<NewsItem> findAllById(Iterable<Long> ids) {
        return em.createQuery("SELECT n FROM NewsItem n WHERE n.id in (:ids)")
                .setParameter("ids", ids)
                .getResultList();
    }

    @Override
    public Optional<NewsItem> findById(Long id) {
        try {
            NewsItem a = (NewsItem) em.createQuery("SELECT a FROM NewsItem a WHERE a.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
            return Optional.of(a);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public <S extends NewsItem> S save(S entity) {
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        if (existsById(entity.getId())) {
            em.merge(entity);
        } else {
            em.persist(entity);
        }
        em.flush();
        Optional<NewsItem> n = findById(entity.getId());
        return (S) n.orElse(null);
    }

    @Override
    public <S extends NewsItem> Iterable<S> saveAll(Iterable<S> entities) {
        List<S> entityList = new LinkedList<>();
        for (S next : entities) {
            if (next == null) {
                throw new IllegalArgumentException();
            }
            entityList.add(save(next));
        }
        return entityList;
    }
}
