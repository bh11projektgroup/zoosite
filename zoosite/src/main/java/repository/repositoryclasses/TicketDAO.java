/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.repositoryclasses;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import repository.CRUDRepository;
import repository.entity.Ticket;
import repository.entity.id.TicketID;

/**
 *
 * @author tamas
 */
@Stateless
@LocalBean
public class TicketDAO implements CRUDRepository<Ticket, TicketID> {

    @PersistenceContext(unitName = "EL_PU")
    private EntityManager em;

    @Override
    public long count() {
        return (Long) em.createQuery("SELECT Count (t.id) FROM Ticket t").getSingleResult();
    }

    @Override
    public void delete(Ticket entity) {
        em.remove(entity);
    }

    @Override
    public void deleteAll() {
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM Ticket").executeUpdate();
    }

    @Override
    public void deleteAll(Iterable<? extends Ticket> entities) {
        List<TicketID> ids = new LinkedList<>();
        for (Ticket next : entities) {
            if (next == null) {
                throw new IllegalArgumentException();
            }
            ids.add(next.getId());
        }
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM Ticket t WHERE t.id in (:ids)")
                .setParameter("ids", ids)
                .executeUpdate();
    }

    @Override
    public void deleteById(TicketID id) {
        em.createQuery("DELETE FROM Ticket t WHERE t.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public boolean existsById(TicketID id) {
        return findById(id).isPresent();
    }

    @Override
    public Iterable<Ticket> findAll() {
        return em.createQuery("SELECT t FROM Ticket t").getResultList();
    }

    @Override
    public Iterable<Ticket> findAllById(Iterable<TicketID> ids) {
        return em.createQuery("SELECT t FROM Ticket t WHERE t.id in (:ids)")
                .setParameter("ids", ids)
                .getResultList();
    }

    @Override
    public Optional<Ticket> findById(TicketID id) {
        try {
            Ticket a = (Ticket) em.createQuery("SELECT a FROM Ticket a WHERE a.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
            return Optional.of(a);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public <S extends Ticket> S save(S entity) {
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        if (existsById(entity.getId())) {
            em.merge(entity);
        } else {
            em.persist(entity);
        }
        em.flush();
        Optional<Ticket> n = findById(entity.getId());
        return (S) n.orElse(null);
    }

    @Override
    public <S extends Ticket> Iterable<S> saveAll(Iterable<S> entities) {
        List<S> entityList = new LinkedList<>();
        for (S next : entities) {
            if (next == null) {
                throw new IllegalArgumentException();
            }
            entityList.add(save(next));
        }
        return entityList;
    }
}
