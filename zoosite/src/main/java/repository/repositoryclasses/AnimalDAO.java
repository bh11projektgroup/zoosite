package repository.repositoryclasses;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import repository.CRUDRepository;
import repository.entity.Animal;

/**
 *
 * @author Niki
 */
@Singleton
@LocalBean
public class AnimalDAO implements CRUDRepository<Animal, Long> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public long count() {
        return (Long) em.createQuery("SELECT COUNT (a.id) FROM Animal a")
                .getSingleResult();
    }

    @Override
    public void delete(Animal entity) {
        em.remove(entity);
    }

    @Override
    public void deleteAll() {
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM Animal").executeUpdate();
    }

    @Override
    public void deleteAll(Iterable<? extends Animal> entities) {
        List<Long> ids = new ArrayList();
        for (Animal entity : entities) {
            ids.add(entity.getId());
        }
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM Animal a WHERE a.id in (:ids)")
                .setParameter("ids", ids)
                .executeUpdate();
    }

    @Override
    public void deleteById(Long id) {
        em.createQuery("DELETE FROM Animal a WHERE a.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public boolean existsById(Long id) {
        return findById(id).isPresent();
    }

    @Override
    public List<Animal> findAll() {
        return em.createQuery("SELECT a FROM Animal a")
                .getResultList();
    }

    @Override
    public Iterable<Animal> findAllById(Iterable<Long> ids) {
        return em.createQuery("SELECT a FROM Animal a WHERE a.id in (:ids)")
                .setParameter("ids", ids)
                .getResultList();
    }

    @Override
    public Optional<Animal> findById(Long id) {
        try {
            Animal a = (Animal) em.createQuery("SELECT a FROM Animal a WHERE a.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
            return Optional.of(a);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public <S extends Animal> S save(S entity) {
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        if (existsById(entity.getId())) {
            em.merge(entity);
        } else {
            em.persist(entity);
        }
        em.flush();
        Optional<Animal> n = findById(entity.getId());
        return (S) n.orElse(null);
    }

    @Override
    public <S extends Animal> Iterable<S> saveAll(Iterable<S> entities) {
        List<S> entityList = new LinkedList<>();
        for (S next : entities) {
            if (next == null) {
                throw new IllegalArgumentException();
            }
            entityList.add(save(next));
        }
        return entityList;
    }

}
