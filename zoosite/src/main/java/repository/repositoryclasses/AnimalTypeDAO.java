package repository.repositoryclasses;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import repository.CRUDRepository;
import repository.entity.AnimalType;

/**
 *
 * @author Niki
 */
@Stateless
@LocalBean
public class AnimalTypeDAO implements CRUDRepository<AnimalType, Long> {

    @PersistenceContext
    private EntityManager em;

    public AnimalType findByName(String name) {
        return em.createQuery("SELECT t FROM AnimalType t WHERE t.type = :name", AnimalType.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public long count() {
        return (Long) em.createQuery("SELECT COUNT (at.id) FROM AnimalType at")
                .getSingleResult();
    }

    @Override
    public void delete(AnimalType entity) {
        em.remove(entity);
    }

    @Override
    public void deleteAll() {
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM AnimalType").executeUpdate();
    }

    @Override
    public void deleteAll(Iterable<? extends AnimalType> entities) {
        List<Long> ids = new ArrayList();
        for (AnimalType entity : entities) {
            ids.add(entity.getId());
        }
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM AnimalType at WHERE at.id in (:ids)")
                .setParameter("ids", ids)
                .executeUpdate();
    }

    @Override
    public void deleteById(Long id) {
        em.createQuery("DELETE FROM AnimalType at WHERE at.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public boolean existsById(Long id) {
        return findById(id).isPresent();
    }

    @Override
    public List<AnimalType> findAll() {
        return em.createQuery("SELECT a FROM AnimalType a")
                .getResultList();
    }

    @Override
    public Iterable<AnimalType> findAllById(Iterable<Long> ids) {
        return em.createQuery("SELECT at FROM AnimalType at WHERE at.id in (:ids)")
                .setParameter("ids", ids)
                .getResultList();
    }

    @Override
    public Optional<AnimalType> findById(Long id) {
        try {
            AnimalType a = (AnimalType) em.createQuery("SELECT a FROM AnimalType a WHERE a.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
            return Optional.of(a);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public <S extends AnimalType> S save(S entity) {
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        if (existsById(entity.getId())) {
            em.merge(entity);
        } else {
            em.persist(entity);
        }
        em.flush();
        Optional<AnimalType> n = findById(entity.getId());
        return (S) n.orElse(null);
    }

    @Override
    public <S extends AnimalType> Iterable<S> saveAll(Iterable<S> entities) {
        List<S> entityList = new LinkedList<>();
        for (S next : entities) {
            if (next == null) {
                throw new IllegalArgumentException();
            }
            entityList.add(save(next));
        }
        return entityList;
    }
}
