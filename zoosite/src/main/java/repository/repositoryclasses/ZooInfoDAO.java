package repository.repositoryclasses;

import repository.CRUDRepository;
import repository.entity.ZooInfo;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.persistence.NoResultException;

@Singleton
@LocalBean
public class ZooInfoDAO implements CRUDRepository<ZooInfo, Long> {

    @PersistenceContext(unitName = "EL_PU")
    EntityManager em;

    @Override
    public long count() {
        return (long) em.createQuery("SELECT COUNT(z.id) FROM ZooInfo z").getSingleResult();
    }

    @Override
    public void delete(ZooInfo entity) {
        em.remove(entity);
    }

    @Override
    public void deleteAll() {
        em.createQuery("DELETE FROM ZooInfo ").executeUpdate();
    }

    @Override
    public void deleteAll(Iterable<? extends ZooInfo> entities) {
        List<Long> ids = new ArrayList<>();
        for (ZooInfo zooInfo : entities) {
            if (zooInfo == null) {
                throw new IllegalArgumentException();
            }
            ids.add(zooInfo.getId());
        }
        em.createQuery("DELETE FROM ZooInfo n WHERE n.id in (:ids)")
                .setParameter("ids", ids)
                .executeUpdate();
    }

    @Override
    public void deleteById(Long id) {
        em.createQuery("DELETE FROM ZooInfo zi WHERE zi.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public boolean existsById(Long id) {
        return findById(id).isPresent();
    }

    @Override
    public List<ZooInfo> findAll() {
        return em.createQuery("SELECT n FROM ZooInfo n").getResultList();
    }

    @Override
    public List<ZooInfo> findAllById(Iterable<Long> ids) {
        return em.createQuery("SELECT n FROM ZooInfo n WHERE n.id IN (:ids)")
                .setParameter("ids", ids)
                .getResultList();
    }

    @Override
    public Optional<ZooInfo> findById(Long id) {
        try {
            ZooInfo a = (ZooInfo) em.createQuery("SELECT a FROM ZooInfo a WHERE a.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
            return Optional.of(a);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public <S extends ZooInfo> S save(S entity) {
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        if (existsById(entity.getId())) {
            em.merge(entity);
        } else {
            em.persist(entity);
        }
        em.flush();
        Optional<ZooInfo> n = findById(entity.getId());
        return (S) n.orElse(null);
    }

    @Override
    public <S extends ZooInfo> Iterable<S> saveAll(Iterable<S> entities) {
        List<S> entityList = new LinkedList<>();
        for (S next : entities) {
            if (next == null) {
                throw new IllegalArgumentException();
            }
            entityList.add(save(next));
        }
        return entityList;
    }

    public ZooInfo getLatestZooInfo() {
        return (ZooInfo) em.createQuery("SELECT e FROM ZooInfo e ORDER BY e.dateOfCreation DESC")
                .setMaxResults(1).getSingleResult();
    }
}
