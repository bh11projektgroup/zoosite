package repository.repositoryclasses;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import repository.CRUDRepository;
import repository.entity.AnimalClass;

/**
 *
 * @author Niki
 */
@Stateless
@LocalBean
public class AnimalClassDAO implements CRUDRepository<AnimalClass, Long> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public long count() {
        return (Long) em.createQuery("SELECT COUNT (ac.id) FROM AnimalClass ac")
                .getSingleResult();
    }

    @Override
    public void delete(AnimalClass entity) {
        em.remove(entity);
    }

    @Override
    public void deleteAll() {
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM AnimalClass").executeUpdate();
    }

    @Override
    public void deleteAll(Iterable<? extends AnimalClass> entities) {
        List<Long> ids = new ArrayList();
        for (AnimalClass entity : entities) {
            ids.add(entity.getId());
        }
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM AnimalClass ac WHERE ac.id in (:ids)")
                .setParameter("ids", ids)
                .executeUpdate();
    }

    @Override
    public void deleteById(Long id) {
        em.createQuery("DELETE FROM AnimalClass a WHERE a.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public boolean existsById(Long id) {
        return findById(id).isPresent();
    }

    @Override
    public List<AnimalClass> findAll() {
        return em.createQuery("SELECT a FROM AnimalClass a")
                .getResultList();
    }

    @Override
    public Iterable<AnimalClass> findAllById(Iterable<Long> ids) {
        return em.createQuery("SELECT ac FROM AnimalClass ac WHERE ac.id in (:ids)")
                .setParameter("ids", ids)
                .getResultList();
    }

    @Override
    public Optional<AnimalClass> findById(Long id) {
        try {
            AnimalClass a = (AnimalClass) em.createQuery("SELECT a FROM AnimalClass a WHERE a.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
            return Optional.of(a);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public <S extends AnimalClass> S save(S entity) {
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        if (existsById(entity.getId())) {
            em.merge(entity);
        } else {
            em.persist(entity);
        }
        em.flush();
        Optional<AnimalClass> n = findById(entity.getId());
        return (S) n.orElse(null);
    }

    @Override
    public <S extends AnimalClass> Iterable<S> saveAll(Iterable<S> entities) {
        List<S> entityList = new LinkedList<>();
        for (S next : entities) {
            if (next == null) {
                throw new IllegalArgumentException();
            }
            entityList.add(save(next));
        }
        return entityList;
    }
}
