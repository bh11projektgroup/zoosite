/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.utilities;

import java.sql.SQLException;
import java.util.Locale;
import java.util.Vector;

import org.eclipse.persistence.config.SessionCustomizer;
import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.internal.helper.DatabaseField;
import org.eclipse.persistence.mappings.AggregateObjectMapping;
import org.eclipse.persistence.mappings.Association;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.DirectMapMapping;
import org.eclipse.persistence.mappings.ObjectReferenceMapping;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.tools.schemaframework.IndexDefinition;

/**
 *
 * @author Kenwulf
 */
public class CamelCaseSessionCustomizer implements SessionCustomizer {

    @Override
    public void customize(Session session) throws SQLException {
        for (ClassDescriptor descriptor : session.getDescriptors().values()) {
            // Only change the table name for non-embedable entities with no
            // @Table already
            if (!descriptor.getTables().isEmpty() && descriptor.getAlias().equalsIgnoreCase(descriptor.getTableName())) {
                String tableName = addUnderscores(descriptor.getTableName());
                descriptor.setTableName(tableName);
                for (IndexDefinition index : descriptor.getTables().get(0).getIndexes()) {
                    index.setTargetTable(tableName);
                }
            }
            custumizeColumns(descriptor.getMappings(), session);

        }
    }

    private static String addUnderscores(String name) {
        if ("begintime".equalsIgnoreCase(name)) {
            System.err.println();
        }
        StringBuffer buf = new StringBuffer(name.replace('.', '_'));
        for (int i = 1; i < buf.length() - 1; i++) {
            if (Character.isLowerCase(buf.charAt(i - 1)) && Character.isUpperCase(buf.charAt(i))) {
                buf.insert(i++, '_');
            }
        }
        return buf.toString().toLowerCase(Locale.ENGLISH);
    }

    private void custumizeColumns(Vector<DatabaseMapping> mappings, Session session) {
        for (DatabaseMapping mapping : mappings) {
            // Only change the column name for non-embedable entities with
            // no @Column already

            if (mapping instanceof AggregateObjectMapping) {
                for (Association association : ((AggregateObjectMapping) mapping).getAggregateToSourceFieldAssociations()) {
                    DatabaseField field = (DatabaseField) association.getValue();
                    field.setName(addUnderscores(field.getName()));

                    for (DatabaseMapping attrMapping : session.getDescriptor(((AggregateObjectMapping) mapping).getReferenceClass()).getMappings()) {
                        if (attrMapping.getAttributeName().equalsIgnoreCase((String) association.getKey())) {
                            ((AggregateObjectMapping) mapping).addFieldTranslation(field, addUnderscores(attrMapping.getAttributeName()));
                            ((AggregateObjectMapping) mapping).getAggregateToSourceFields().remove(association.getKey());
                            break;
                        }
                    }
                }
            } else if (mapping instanceof ObjectReferenceMapping) {
                for (DatabaseField foreignKey : ((ObjectReferenceMapping) mapping).getForeignKeyFields()) {
                    foreignKey.setName(addUnderscores(foreignKey.getName()));
                }
            } else if (mapping instanceof DirectMapMapping) {
                for (DatabaseField referenceKey : ((DirectMapMapping) mapping).getReferenceKeyFields()) {
                    referenceKey.setName(addUnderscores(referenceKey.getName()));
                }
                for (DatabaseField sourceKey : ((DirectMapMapping) mapping).getSourceKeyFields()) {
                    sourceKey.setName(addUnderscores(sourceKey.getName()));
                }
            } else {
                DatabaseField field = mapping.getField();
                if (field != null && !mapping.getAttributeName().isEmpty() && field.getName().equalsIgnoreCase(mapping.getAttributeName())) {
                    field.setName(addUnderscores(mapping.getAttributeName()));
                }
            }
        }
    }

}
