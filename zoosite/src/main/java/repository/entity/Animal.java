package repository.entity;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;

/**
 *
 * @author Niki
 */
@Data
@Entity
@Table(name = "animals")
public class Animal implements Serializable {

    @Transient
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "type_id")
    private AnimalType type;

    private String name;

    private String sex;

    private int age;

    private LocalDate dateOfArrival;

    @Lob
    private String profilePictureUri;
    
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Image image;

    @PrePersist
    public void init() {
        dateOfArrival = LocalDate.now();
    }
}
