package repository.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;

/**
 *
 * @author Niki
 */
@Data
@Entity
@Table(name = "animal_classes")
public class AnimalClass implements Serializable {

    @Transient
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String animalClass;

    @Lob
    String description;
    
    @Lob
    String profilePictureUri;

    @OneToMany(mappedBy = "animalClass", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<AnimalType> animalTypes;

}
