package repository.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;

/**
 *
 * @author Kiki
 */
@Entity
@Data
@Table(name = "users")
public class User implements Serializable {

    @Transient
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String accountName;
    private String address;
    private String city;
    private String email;
    @Lob
    private String imageUri;
    private String name;
    private String password;
    private String postalCode;
    private LocalDateTime timeOfRegistration;

    @PrePersist
    public void prePersist() {
        timeOfRegistration = LocalDateTime.now();
    }
}
