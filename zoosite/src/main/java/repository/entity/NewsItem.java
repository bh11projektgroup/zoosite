package repository.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;

/**
 *
 * @author tamas
 */
@Entity
@Data
@Table(name = "news")
@NamedQueries({
    @NamedQuery(name = "NewsItem.Top4OrderByTimeOfCreation",
            query = "SELECT ni FROM NewsItem ni ORDER BY ni.creationDate")
    ,
    @NamedQuery(name = "NewsItem.findAll", query = "SELECT ni FROM NewsItem ni"),
    @NamedQuery(name = "NewsItem.findByID", query = "SELECT ni FROM NewsItem ni WHERE ni.id=:id")
})
public class NewsItem implements Serializable {

    @Transient
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String title;
    private String description;
    @Lob
    private String body;
    private LocalDateTime creationDate;
    @Lob
    private String imageUri;
    
    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.PERSIST)
    private Image image;

    @PrePersist
    public void prePersist() {
        creationDate = LocalDateTime.now();
    }
}
