package repository.entity;

import lombok.Data;

import javax.persistence.Lob;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.PrePersist;

@Entity
@Data
@Table(name = "zoo_info")

public class ZooInfo implements Serializable {

    @Transient
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String address;
    private String phone;
    private String email;

    @Lob
    private String description;

    @Lob
    private String openingHours;
    private LocalDateTime dateOfCreation;

    @PrePersist
    public void init() {
        dateOfCreation = LocalDateTime.now();
    }

}
