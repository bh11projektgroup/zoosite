package repository.entity;

import repository.entity.id.TicketID;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;

/**
 *
 * @author tamas
 */
@Entity
@Data
@Table(name = "tickets")
public class Ticket implements Serializable {

    @Transient
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private TicketID id;

    @MapsId("programId")
    @ManyToOne(fetch = FetchType.LAZY)
    private Program program;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private User user;
}
