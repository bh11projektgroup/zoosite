package repository.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;

/**
 *
 * @author tamas
 */
@Entity
@Data
@Table(name = "programs")
public class Program implements Serializable {

    @Transient
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    @Lob
    private String description;
    private String location;
    private LocalDateTime startTime;
    private String duration;
    @Lob
    private String pictureUri;
    private int capacity;
    private int ticketPrice;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Image image;

    public void setId(Long id) {
        System.out.println(toString());
        this.id = id;
    }

    public void setName(String name) {
        System.out.println(toString());
        this.name = name;
    }

    public void setTicketPrice(int ticketPrice) {
        System.out.println(toString());
        this.ticketPrice = ticketPrice;
    }
}
