/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.entity.id;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import lombok.Data;

/**
 *
 * @author tamas
 */
@Embeddable
@Data
public class TicketID implements Serializable {
    @Transient
    private static final long serialVersionUID = 1L;
    
    private Long programId;
    private String id;
}
