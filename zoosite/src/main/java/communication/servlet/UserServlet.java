package communication.servlet;

import communication.ServletErrorHandler;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import service.dto.UserDTO;
import service.serviceclasses.UserService;

/**
 *
 * @author Kenwulf
 */
@Slf4j
@WebServlet(name = "UserServlet", urlPatterns = {"/users"})
public class UserServlet extends HttpServlet implements ServletErrorHandler {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient UserService service;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String caller = req.getParameter("caller");
        String delete = req.getParameter("delete");
        if (isEmpty(caller)) {
            log.debug("caller is a required parameter on /users!");
            return;
        }
        if (delete.isEmpty() || "false".equalsIgnoreCase(delete)) {
            handlePersistence(caller, req, resp);
        } else {
            handleDeletion(caller, req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String caller;
        String id;
        try {
            caller = (String) req.getAttribute("caller");
            id = (String) req.getAttribute("id");
        } catch (ClassCastException ex) {
            log.debug("caller or id attribute was not in the expected type on /users!");
            return;
        }
        if (isEmpty(caller)) {
            log.debug("caller is a required attribute on /users!");
            return;
        }
        if (isEmpty(id)) {
            req.setAttribute("users", service.getAllUsers());
        } else {
            try {
                req.setAttribute("user", service.findByID(Long.parseLong(id)));
            } catch (NumberFormatException ex) {
                log.debug("id was not a number on /users!");
                return;
            }
        }
        req.getRequestDispatcher(caller).forward(req, resp);
    }

    private void handlePersistence(String caller, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accountName = req.getParameter("accountName");
        String address = req.getParameter("address");
        String city = req.getParameter("city");
        String email = req.getParameter("email");
        String imageUri = req.getParameter("imageUri");
        String name = req.getParameter("name");
        String password = req.getParameter("password");
        String postalCode = req.getParameter("postalCode");
        String id = req.getParameter("id");
        if (existsEmpty(accountName)) {
            log.debug("One or more required parameters were not set properly on /users POST!");
            return;
        }
        UserDTO dto = new UserDTO();
        dto.setAccountName(accountName);
        dto.setAddress(address);
        dto.setCity(city);
        dto.setEmail(email);
        dto.setName(name);
        dto.setPassword(service.encryptToSha256(password));
        dto.setPostalCode(postalCode);
        if (!isEmpty(imageUri)) {
            dto.setImageUri(imageUri);
        }
        if (!isEmpty(id)) {
            try {
                dto.setId(Long.parseLong(id));
            } catch (NumberFormatException ex) {
                log.debug("id was not a number on /users POST!");
                return;
            }
        }
        req.setAttribute("result", service.save(dto));
        req.getRequestDispatcher(caller).forward(req, resp);
    }

    private void handleDeletion(String caller, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        try {
            req.setAttribute("result", service.deleteById(Long.parseLong(id)));
            req.getRequestDispatcher(caller).forward(req, resp);
        } catch (NumberFormatException ex) {
            log.debug("id was not a number on /users POST!");
        }
    }
}
