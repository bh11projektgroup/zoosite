package communication.servlet;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.serviceclasses.NewsItemService;

/**
 *
 * @author Niki
 */
@WebServlet(name = "AdminNewsServlet", urlPatterns = {"/edit_news", "/select_news", "/delete_news", })
@ServletSecurity(@HttpConstraint(rolesAllowed = "admin"))
public class AdminNewsServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient NewsItemService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getServletPath().contains("edit_news")) {

            if (request.getParameter("id") != null) {

                String id = request.getParameter("id");
                request.setAttribute("newsItem", service.getSingleNews(Long.parseLong(id)));
                
            }

            request.getRequestDispatcher("WEB-INF/Admin/editNews.jsp").forward(request, response);
        } else {
            
            if (request.getServletPath().contains("delete_news")) {
                String id = request.getParameter("id");
                service.deleteById(Long.parseLong(id));
            }
            
            request.setAttribute("NewsItems", service.getAll());
            request.getRequestDispatcher("WEB-INF/Admin/selectNews.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
