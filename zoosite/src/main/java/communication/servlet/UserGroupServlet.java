/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication.servlet;

import communication.ServletErrorHandler;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import service.dto.UserGroupDto;
import service.serviceclasses.UserService;

/**
 *
 * @author Kenwulf
 */
@Slf4j
@WebServlet(name = "UserGroupServlet", urlPatterns = {"/usergroups"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = "admin"))
public class UserGroupServlet extends HttpServlet implements ServletErrorHandler {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient UserService service;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            String caller = (String) req.getAttribute("caller");
            if (isEmpty(caller)) {
                log.debug("caller was empty!");
                return;
            }
            req.setAttribute("userGroups", service.getAllUserGroups());
            req.getRequestDispatcher(caller).forward(req, resp);
        } catch (ClassCastException ex) {
            log.debug("Caller was the wrong type!");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String caller = req.getParameter("caller");
        String delete = req.getParameter("delete");
        if (isEmpty(caller)) {
            log.debug("caller was empty!");
            return;
        }
        if (isEmpty(delete) || "false".equalsIgnoreCase(delete)) {
            handleDeletion(caller, req, resp);
        } else {
            handlePersistence(caller, req, resp);
        }

    }

    private void handleDeletion(String caller, HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            String id = req.getParameter("id");
            req.setAttribute("result", service.deleteUserGroup(Long.parseLong(id)));
            req.getRequestDispatcher(caller).forward(req, resp);
        } catch (NumberFormatException ex) {
            log.debug("id was not a number!");
        }
    }

    private void handlePersistence(String caller, HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String id = req.getParameter("id");
        String accountName = req.getParameter("accountName");
        String groupName = req.getParameter("groupName");

        if (existsEmpty(accountName, groupName)) {
            log.debug("Required parameter was empty!");
            return;
        }
        UserGroupDto dto = new UserGroupDto();
        dto.setAccountName(accountName);
        dto.setGroupName(groupName);
        if (!isEmpty(id)) {
            try {
                dto.setId(Long.parseLong(id));
            } catch (NumberFormatException ex) {
                log.debug("id was not a number!");
                return;
            }
        }
        req.setAttribute("result", service.saveUserGroup(dto));
        req.getRequestDispatcher(caller).forward(req, resp);
    }

}
