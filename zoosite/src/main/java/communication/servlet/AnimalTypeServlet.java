package communication.servlet;

import communication.ServletErrorHandler;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import lombok.extern.slf4j.Slf4j;
import service.dto.AnimalTypeDTO;
import service.dto.ImageDTO;
import service.serviceclasses.AnimalService;
import service.serviceclasses.AnimalTypeService;
import service.serviceclasses.AnimalClassService;
import service.serviceclasses.ImageService;
import service.utilities.FileSystemService;
import service.utilities.FileSystemService.Folder;

/**
 *
 * @author Niki
 */
@Slf4j
@WebServlet(name = "AnimalTypeServlet", urlPatterns = {"/internal/animaltype"})
@MultipartConfig
@ServletSecurity(httpMethodConstraints = {
    @HttpMethodConstraint(value = "POST", rolesAllowed = "admin")})
public class AnimalTypeServlet extends HttpServlet implements ServletErrorHandler {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient AnimalService animalService;

    @Inject
    private transient AnimalTypeService animalTypeService;

    @Inject
    private transient AnimalClassService animalClassService;

    @Inject
    private transient FileSystemService fileSystemService;

    @Inject
    private transient ImageService imageService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String caller;
        try {
            caller = (String) request.getAttribute("caller");
        } catch (Exception e) {
            caller = "/WEB-INF/AnimalType.jsp";
        }

        if (request.getParameter("id") != null) {

            Long id = Long.parseLong(request.getParameter("id"));

            AnimalTypeDTO animalType = animalTypeService.getAnimalTypeById(id);

            request.setAttribute("animalTypes", animalType);

        }
        List<AnimalTypeDTO> animalTypes = animalTypeService.getAllAnimalTypes();

        request.setAttribute("animalTypes", animalTypes);

        request.getRequestDispatcher(caller).forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String caller = req.getParameter("caller");
        String delete = req.getParameter("delete");
        if (isEmpty(caller)) {
            log.debug("caller was empty!");
            return;
        }
        if (isEmpty(delete) || "false".equalsIgnoreCase(delete)) {
            handleAnimalTypePersistence(caller, req, resp);
        } else {
            req.setAttribute("result", animalTypeService.deleteById(parseID(req, resp)));
            req.getRequestDispatcher(caller).forward(req, resp);
        }
    }

    private void handleAnimalTypePersistence(String caller, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String classId = req.getParameter("classId");
        String type = req.getParameter("type");
        String description = req.getParameter("description");
        String profilePictureUri = req.getParameter("profilePictureUri");

        if (existsEmpty(classId, type)) {
            log.debug("type and classId are required parameters on /animals!");
            return;
        }
        AnimalTypeDTO dto = new AnimalTypeDTO();
        String id = req.getParameter("id");
        if (!isEmpty(id)) {
            try {
                dto.setId(Long.parseLong(id));
            } catch (NumberFormatException ex) {
                log.debug("id was not a number on /news!");
                return;
            }
        } else {
            dto.setAnimals(new ArrayList<>());
        }
        try {
            dto.setClassId(Long.parseLong(classId));
        } catch (NumberFormatException ex) {
            log.debug("classId was not a number on /animals!");
            return;
        }
        dto.setType(type);
        if (!isEmpty(description)) {
            dto.setDescription(description);
        }
        if (!isEmpty(profilePictureUri)) {
            dto.setProfilePictureUri(profilePictureUri);
        }

        ImageDTO imageDto = parseImageDto(id, req);

        dto.setImageDto(imageDto);

        AnimalTypeDTO newDTO = animalTypeService.save(dto);

        resp.sendRedirect(caller + "?id=" + newDTO.getId());
    }

    private ImageDTO parseImageDto(String id, HttpServletRequest req) throws IOException, ServletException {
        ImageDTO imageDto = new ImageDTO();
        Part filePart = req.getPart("animalTypeImg");
        try {
            if (filePart.getSize() > 0) {

                fileSystemService.saveFile(Folder.ANIMALTYPE, filePart);
                imageDto = new ImageDTO();
                imageDto.setType(Folder.ANIMALTYPE.toString());
                imageDto.setName(Paths.get(filePart.getSubmittedFileName()).getFileName().toString());
            } else {
                if (isNumber(id)) {
                    final AnimalTypeDTO t = animalTypeService.getAnimalTypeById(Long.parseLong(id));
                    imageDto = t.getImageDto();
                }
            }
        } catch (NullPointerException ex) {
            log.debug(ex.toString());
            return null;
        }
        return imageDto;
    }

    private long parseID(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        try {
            return Long.parseLong(id);
        } catch (NumberFormatException ex) {
            log.debug("id could not be parsed properly on /animals!");
            return -1L;
        }
    }
}