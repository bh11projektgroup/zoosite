package communication.servlet;

import communication.ServletErrorHandler;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import service.dto.ZooInfoDTO;
import service.serviceclasses.ZooInformationService;

/**
 *
 * @author Kenwulf
 */
@Slf4j
@WebServlet(name = "ZooInformationServlet", urlPatterns = {"/info"})
@ServletSecurity(httpMethodConstraints = {
    @HttpMethodConstraint(value = "POST", rolesAllowed = "admin")})
public class ZooInformationServlet extends HttpServlet implements ServletErrorHandler {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient ZooInformationService service;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String caller;
        try {
            caller = (String) req.getAttribute("caller");
        } catch (ClassCastException ex) {
            log.debug("Attribute was set as an invalid type on /info");
            return;
        }
        if (isEmpty(caller)) {
            log.debug("caller attribute was not set properly on /info");
            return;
        }
        try {
            Object result = service.getZooInfo();
            req.setAttribute("zooInfo", result);
            req.getRequestDispatcher(caller).forward(req, resp);
        } catch (IllegalArgumentException ex) {
            log.debug("Could not find the requested information!");
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String caller = req.getParameter("caller");
        if (isEmpty(caller)) {
            log.debug("Caller parameter was not set properly on /info!");
            return;
        }
        handlePersistence(caller, req, resp);
    }

    private void handlePersistence(String caller, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String address = req.getParameter("address");
        String phone = req.getParameter("phone");
        String email = req.getParameter("email");
        String description = req.getParameter("description");
        String openingHours = req.getParameter("openingHours");
        if (existsEmpty(address, phone, email, description, openingHours)) {
            log.debug("One or more of the required parameters was not ser properly on /info!");
            return;
        }

        ZooInfoDTO dto = new ZooInfoDTO();
        String id = req.getParameter("id");
        if (!isEmpty(id)) {
            try {
                dto.setId(Long.parseLong(id));
            } catch (NumberFormatException ex) {
                log.debug("id was not a number on /info!");
                return;
            }
        }
        dto.setAddress(address);
        dto.setDescription(description);
        dto.setEmail(email);
        dto.setOpeningHours(openingHours);
        dto.setPhone(phone);
        try {
            service.save(dto);
            resp.sendRedirect(caller);
        } catch (IllegalArgumentException ex) {
            log.debug("Could not find the requested information!");
        }
    }
}