package communication.servlet;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.serviceclasses.NewsItemService;
import communication.ServletErrorHandler;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Kenwulf
 */
@Slf4j
@WebServlet(name = "SingleNewsServlet", urlPatterns = {"/newsitems/*"})
@ServletSecurity(httpMethodConstraints = {
    @HttpMethodConstraint(value = "POST", rolesAllowed = "admin")})
public class SingleNewsServlet extends HttpServlet implements ServletErrorHandler {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient NewsItemService service;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idParam = req.getPathInfo().substring(1);
        int id;
        try {
            id = Integer.parseInt(idParam);
            Object newsItem = service.getSingleNews(id);
            if (newsItem != null) {
                req.setAttribute("newsItem", service.getSingleNews(id));
                req.getRequestDispatcher("/WEB-INF/SingleNews.jsp").forward(req, resp);
            } else {
                log.debug("The requested news item does not exist!");
            }
        } catch (NumberFormatException | NullPointerException ex) {
            log.debug("The ID could not be read from the url. Try using ../newsItems/<id>");
        }
    }

}
