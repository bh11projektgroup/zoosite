package communication.servlet;

import communication.ServletErrorHandler;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Locale;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import lombok.extern.slf4j.Slf4j;
import service.dto.AnimalDTO;
import service.dto.ImageDTO;
import service.serviceclasses.AnimalClassService;
import service.serviceclasses.AnimalService;
import service.serviceclasses.AnimalTypeService;
import service.utilities.AnimalDataCategory;
import service.utilities.FileSystemService;
import service.utilities.FileSystemService.Folder;

/**
 *
 * @author Kenwulf
 */
@Slf4j
@WebServlet(name = "AnimalServlet", urlPatterns = {"/animals"})
@MultipartConfig
@ServletSecurity(httpMethodConstraints = {
    @HttpMethodConstraint(value = "POST", rolesAllowed = "admin")})
public class AnimalServlet extends HttpServlet implements ServletErrorHandler {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient AnimalClassService classService;
    @Inject
    private transient AnimalTypeService typeService;
    @Inject
    private transient AnimalService animalService;

    @Inject
    private transient FileSystemService fileSystemService;

    private final String animalClassServletPath = "/internal/animalclass";
    private final String animalTypeServletPath = "/internal/animaltype";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String caller = null;
        try {
            caller = (String) req.getAttribute("caller");
        } catch (ClassCastException ex) {
            log.debug("Attribute didn't contain the required type on /animalclasses!");
        }

        if (isEmpty(caller)) {
            log.debug("Attribute caller was not set properly on /animalclasses");
        }

        if (req.getParameter("id") != null) {
            req.setAttribute("animal", animalService.getAnimalById(Long.parseLong(req.getParameter("id"))));
        }

        req.setAttribute("animals", animalService.getAllAnimals());
        req.setAttribute("animalClasses", classService.getAllAnimalClasses());
        req.setAttribute("animalTypes", typeService.getAllAnimalTypes());

        req.getRequestDispatcher(caller).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String categoryParam = req.getParameter("dataCategory");
        String caller = req.getParameter("caller");

        if (existsEmpty(categoryParam, caller)) {
            log.debug("Requred paramaters were not set properly on /animals!"
                    + " dataCategory and caller has to contain a valid parameter!");
            return;
        }
        try {
            AnimalDataCategory category = AnimalDataCategory.valueOf(categoryParam.toUpperCase(Locale.ENGLISH));
            switch (category) {
                case ANIMAL:
                    handleAnimal(caller, req, resp);
                    break;
                case ANIMALCLASS:
                    req.getRequestDispatcher(animalClassServletPath).forward(req, resp);
                    break;
                case ANIMALTYPE:
                    req.getRequestDispatcher(animalTypeServletPath).forward(req, resp);
            }
        } catch (IllegalArgumentException ex) {
            log.debug("Category parameter value was invalid on /animals! Check API.md for more information!");
        }
    }

    private void handleAnimal(String caller, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String delete = req.getParameter("delete");
        if (isEmpty(delete) || "false".equalsIgnoreCase(delete)) {
            handleAnimalPersistence(caller, req, resp);
        } else {
            req.setAttribute("result", animalService.deleteById(parseID(req, resp)));
            req.getRequestDispatcher(caller).forward(req, resp);
        }
    }

    private void handleAnimalPersistence(String caller, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getParameter("type");
        String name = req.getParameter("name");
        String sex = req.getParameter("sex");
        String age = req.getParameter("age");
        String profilePictureUri = req.getParameter("profilePictureUri");

        if (existsEmpty(name, type)) {
            log.debug("name and type are required parameters for an animal on /animals!");
            return;
        }
        AnimalDTO dto = new AnimalDTO();
        String id = req.getParameter("id");
        if (!isEmpty(id)) {
            try {
                dto.setId(Long.parseLong(id));
            } catch (NumberFormatException ex) {
                log.debug("id was not a number on /news!");
                return;
            }
        }
        dto.setName(name);
        dto.setType(type);
        if (!isEmpty(age)) {
            try {
                dto.setAge(Integer.parseInt(age));
            } catch (NumberFormatException ex) {
                log.debug("age parameter was not a number on /animals!");
                return;
            }
        }
        if (!isEmpty(sex)) {
            dto.setSex(sex);
        }
        if (!isEmpty(profilePictureUri)) {
            dto.setProfilePictureUri(profilePictureUri);
        }

        ImageDTO imageDto = parseImage(id, req);

        dto.setImageDto(imageDto);
        
        AnimalDTO newDto = animalService.save(dto);
        
        resp.sendRedirect(caller + "?id=" + newDto.getId());
    }

    private ImageDTO parseImage(String id, HttpServletRequest req) throws IOException, ServletException {
        ImageDTO imageDto = new ImageDTO();
        Part filePart = req.getPart("animalImg");
        try {
            if (filePart.getSize() > 0) {

                fileSystemService.saveFile(Folder.ANIMAL, filePart);
                imageDto = new ImageDTO();
                imageDto.setType(Folder.ANIMAL.toString());
                imageDto.setName(Paths.get(filePart.getSubmittedFileName()).getFileName().toString());
            } else {
                if (isNumber(id)) {
                    final AnimalDTO t = classService.getAllAnimalClasses().stream()
                            .flatMap(x -> x.getAnimalTypes().stream())
                            .flatMap(a -> a.getAnimals().stream())
                            .filter(i -> i.getId() == Long.parseLong(id))
                            .findFirst().get();
                    imageDto = t.getImageDto();
                }
            }
        } catch (NullPointerException ex) {
            log.debug("Nullpointer:", ex);
            return null;
        }
        return imageDto;
    }

    private long parseID(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        try {
            return Long.parseLong(id);
        } catch (NumberFormatException ex) {
            log.debug("id could not be parsed properly on /animals!");
            return -1L;
        }
    }
}