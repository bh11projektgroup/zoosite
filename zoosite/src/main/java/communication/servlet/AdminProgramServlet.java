package communication.servlet;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.serviceclasses.ProgramService;

/**
 *
 * @author Niki
 */
@WebServlet(name = "AdminProgramServlet", urlPatterns = {"/edit_program", "/select_program", "/delete_program"})
public class AdminProgramServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient ProgramService programService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getServletPath().contains("edit_program")) {

            if (request.getParameter("id") != null) {

                String id = request.getParameter("id");
                request.setAttribute("program", programService.findByID(Long.parseLong(id)));

            }

            request.getRequestDispatcher("WEB-INF/Admin/editProgram.jsp").forward(request, response);

        } else {

            if (request.getServletPath().contains("delete_program")) {
                String id = request.getParameter("id");
                programService.deleteById(Long.parseLong(id));
            }

            request.setAttribute("programs", programService.getProgramsOrderedByDate());
            request.getRequestDispatcher("WEB-INF/Admin/selectProgram.jsp").forward(request, response);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
