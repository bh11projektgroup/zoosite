package communication.servlet;

import communication.ServletErrorHandler;
import java.io.IOException;
import javax.inject.Inject;
 
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.serviceclasses.AnimalClassService;
import service.serviceclasses.AnimalTypeService;

/**
 *
 * @author Niki
 */
@WebServlet(name = "AdminAnimalTypeServlet", urlPatterns = {"/edit_animaltypes"})
@ServletSecurity(@HttpConstraint(rolesAllowed = "admin"))
public class AdminAnimalTypeServlet extends HttpServlet implements ServletErrorHandler {
    
    private static final long serialVersionUID = 1L;
    
    @Inject
    private transient AnimalClassService classService;
    @Inject
    private transient AnimalTypeService typeService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setAttribute("animalTypes", typeService.getAllAnimalTypes());
        request.setAttribute("animalClasses", classService.getAllAnimalClasses());
        
        String id = request.getParameter("id");
       
        if (isNumber(id)) {
            request.setAttribute("animalType", typeService.getAnimalTypeById(Long.parseLong(id)));
        }
        
        request.getRequestDispatcher("WEB-INF/Admin/editAnimalType.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {        
    } 
}