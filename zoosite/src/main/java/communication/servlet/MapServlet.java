package communication.servlet;

import communication.ServletErrorHandler;
import service.dto.ZooInfoDTO;
import service.serviceclasses.ZooInformationService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebServlet(name = "MapServlet", urlPatterns = {"/map"})
public class MapServlet extends HttpServlet implements ServletErrorHandler {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient ZooInformationService service;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            List<ZooInfoDTO> zooInfos = service.getZooInfos();
            req.setAttribute("zooInfos", zooInfos);
            //    req.getRequestDispatcher(caller).forward(req, resp);
        } catch (IllegalArgumentException ex) {
            log.debug("Could not find the requested information!", req, resp);
        }
        req.getRequestDispatcher("map.jsp").forward(req, resp);

    }
}