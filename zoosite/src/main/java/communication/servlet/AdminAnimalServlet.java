package communication.servlet;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.dto.AnimalClassDTO;
import service.dto.AnimalDTO;
import service.dto.AnimalTypeDTO;
import service.serviceclasses.AnimalClassService;

/**
 *
 * @author Niki
 */
@WebServlet(name = "AdminAnimalServlet", urlPatterns = {"/edit_animals"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = "admin"))
public class AdminAnimalServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient AnimalClassService classService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<AnimalClassDTO> animalClasses = classService.getAllAnimalClasses();
        AnimalDTO animal = new AnimalDTO();

        if (request.getParameter("id") != null) {
            animal = animalClasses.stream()
                    .flatMap(t -> t.getAnimalTypes().stream())
                    .flatMap(a -> a.getAnimals().stream())
                    .filter(i -> i.getId() == Long.parseLong(request.getParameter("id")))
                    .findFirst().get();
        }

        List<AnimalDTO> animals = animalClasses.stream()
                .flatMap(t -> t.getAnimalTypes().stream())
                .flatMap(a -> a.getAnimals().stream())
                .collect(Collectors.toList());

        List<AnimalTypeDTO> animalTypes = animalClasses.stream()
                .flatMap(t -> t.getAnimalTypes().stream())
                .collect(Collectors.toList());

        request.setAttribute("animal", animal);

        request.setAttribute("animals", animals);

        request.setAttribute("animalTypes", animalTypes);

        request.setAttribute("animalClasses", animalClasses);

        request.getRequestDispatcher("WEB-INF/Admin/editAnimal.jsp").forward(request, response);


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
