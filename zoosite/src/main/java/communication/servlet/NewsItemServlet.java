package communication.servlet;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.serviceclasses.NewsItemService;
import communication.ServletErrorHandler;
import java.nio.file.Paths;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.http.Part;
import lombok.extern.slf4j.Slf4j;
import service.dto.ImageDTO;
import service.dto.NewsItemDTO;
import service.serviceclasses.ImageService;
import service.utilities.FileSystemService;
import service.utilities.FileSystemService.Folder;

/**
 *
 * @author tamas
 */
@Slf4j
@WebServlet(name = "NewsItemServlet", urlPatterns = {"/news"})
@MultipartConfig
@ServletSecurity(httpMethodConstraints = {
    @HttpMethodConstraint(value = "POST", rolesAllowed = "admin")})
public class NewsItemServlet extends HttpServlet implements ServletErrorHandler {

    private static final long serialVersionUID = 1L;
    @Inject
    private transient NewsItemService service;

    @Inject
    private transient FileSystemService fileSystemService;

    @Inject
    private transient ImageService imageService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String caller = (String) req.getAttribute("caller");
            boolean top4 = (boolean) req.getAttribute("top4");
            if (isEmpty(caller)) {
                log.debug("caller attribute was not set properly on /news");
                return;
            }
            if (top4) {
                req.setAttribute("newsItems", service.getTop4());
            } else {
                req.setAttribute("newsItems", service.getAll());
            }
            req.getRequestDispatcher(caller).forward(req, resp);
        } catch (ClassCastException ex) {
            log.debug("Attribute was set as an invalid type on /news");
        } catch (NullPointerException ex) {
            log.debug("top4 was not set!");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        String caller = req.getParameter("caller");
        String delete = req.getParameter("delete");
        if (isEmpty(caller)) {
            log.debug("caller was no set properly on /news!");
            return;
        }
        
        if (isEmpty(delete) || "false".equalsIgnoreCase(delete)) {
            handlePersistence(caller, req, resp);
        } else {
            handleDeletetion(caller, req, resp);            
        }
    }

    private void handlePersistence(String caller, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        NewsItemDTO dto = new NewsItemDTO();
        String title = req.getParameter("title");
        String description = req.getParameter("description");
        String body = req.getParameter("bodyContent");
        String imageUri = req.getParameter("imageUri");
        if (existsEmpty(title, description, body, imageUri)) {
            log.debug("One of the required paramaters was not set properly on /news!");
            return;
        }
        String id = req.getParameter("id");
        if (isNumber(id)) {
            try {
                dto.setId(Long.parseLong(id));
            } catch (NumberFormatException ex) {
                log.debug("id was not a number on /news!");
                return;
            }
        }

        ImageDTO imageDto = null;
        Part filePart = req.getPart("newsImg");

        try {
            if (filePart.getSize() > 0) {

                fileSystemService.saveFile(Folder.NEWS, filePart);
                imageDto = new ImageDTO();
                imageDto.setType(Folder.NEWS.toString());
                imageDto.setName(Paths.get(filePart.getSubmittedFileName()).getFileName().toString());
            } else {
                if (isNumber(id)) {
                    final NewsItemDTO news = service.getSingleNews(Long.parseLong(id));
                    imageDto = news.getImageDto();
                }
            }
        } catch (NullPointerException ex) {
            log.debug(ex.toString());
        }

        dto.setImageDto(imageDto);

        dto.setTitle(title);
        dto.setDescription(description);
        dto.setBody(body);
        dto.setImageUri(imageUri);

        NewsItemDTO newDTO = service.save(dto);
        resp.sendRedirect(caller + "?id=" + newDTO.getId());
    }

    private void handleDeletetion(String caller, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (isEmpty(id)) {
            log.debug("id was not set properly for deletion on /news!");
            return;
        }
        try {
            req.setAttribute("result", service.deleteById(Long.parseLong(id)));
            req.getRequestDispatcher(caller).forward(req, resp);
        } catch (NumberFormatException ex) {
            log.debug("id was not a number on /news!");
        }
    }
}
