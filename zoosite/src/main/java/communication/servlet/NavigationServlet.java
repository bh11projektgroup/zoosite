/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication.servlet;

import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.dto.AnimalClassDTO;
import service.serviceclasses.AnimalClassService;
import service.serviceclasses.AnimalService;

/**
 *
 * @author Niki
 */
@WebServlet(name = "NavigationServlet", urlPatterns = {"/navigation"})
public class NavigationServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient AnimalService animalService; 
    
    @Inject
    private transient AnimalClassService animalClassService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        List<AnimalClassDTO> animalClasses = animalClassService.getAllAnimalClasses();

        for (AnimalClassDTO animalClass : animalClasses) {
            System.out.println(animalClass.getAnimalClass());
        }

        request.setAttribute("animalClasses", animalClasses);

        request.getRequestDispatcher("index.jsp").forward(request, response);

    }

}