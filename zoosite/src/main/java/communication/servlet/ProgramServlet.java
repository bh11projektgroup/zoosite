package communication.servlet;

import communication.ServletErrorHandler;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import service.dto.ImageDTO;
import lombok.extern.slf4j.Slf4j;
import service.dto.ProgramDTO;
import service.serviceclasses.ImageService;
import service.serviceclasses.ProgramService;
import service.utilities.FileSystemService;
import service.utilities.FileSystemService.Folder;

/**
 *
 * @author Kenwulf
 */
@Slf4j
@WebServlet(name = "ProgramServlet", urlPatterns = "/programs")
@MultipartConfig
@ServletSecurity(httpMethodConstraints = {
    @HttpMethodConstraint(value = "POST", rolesAllowed = "admin")})
public class ProgramServlet extends HttpServlet implements ServletErrorHandler {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient ProgramService service;

    @Inject
    private transient FileSystemService fileSystemService;

    @Inject
    private transient ImageService imageService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String caller = req.getParameter("caller");

        if (isEmpty(caller)) {
            caller = "WEB-INF/Programs.jsp";
        }

        req.setAttribute("programs", service.getProgramsOrderedByDate());
        req.getRequestDispatcher("events.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String caller = req.getParameter("caller");
        String delete = req.getParameter("delete");

        if (isEmpty(caller)) {
            log.debug("Caller parameter was not set on /programs!");
            return;
        }

        if (isEmpty(delete) || "false".equalsIgnoreCase(delete)) {
            handlePersistence(caller, req, resp);
        } else {
            handleDeletion(caller, req, resp);
        }

    }

    private void handlePersistence(String caller, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String description = req.getParameter("description");
        String location = req.getParameter("location");
        String startTime = req.getParameter("startTime");
        String duration = req.getParameter("duration");
        String pictureUri = req.getParameter("pictureUri");
        String capacity = req.getParameter("capacity");
        String ticketPrice = req.getParameter("ticketPrice");
        if (existsEmpty(name, description, location, startTime, duration, pictureUri,
                capacity, ticketPrice)) {
            log.debug("One of the required parameters was not set on /programs!");
            return;
        }

        ProgramDTO dto = new ProgramDTO();

        String id = req.getParameter("id");
        if (isNumber(id)) {
            try {
                dto.setId(Long.parseLong(id));
            } catch (NumberFormatException ex) {
                log.debug("id was not a number on /programs!");
                return;
            }
        }
        ImageDTO imageDto = null;
        Part filePart = req.getPart("programImg");
        try {
            if (filePart.getSize() > 0) {

                fileSystemService.saveFile(Folder.PROGRAM, filePart);
                imageDto = new ImageDTO();
                imageDto.setType(Folder.PROGRAM.toString());
                imageDto.setName(Paths.get(filePart.getSubmittedFileName()).getFileName().toString());
            } else {
                if (isNumber(id)) {
                    final ProgramDTO program = service.findByID(Long.parseLong(id));
                    imageDto = program.getImageDto();
                }
            }
        } catch (NullPointerException ex) {
            log.debug(ex.toString());
        }

        dto.setImageDto(imageDto);

        dto.setName(name);
        dto.setDescription(description);
        dto.setDuration(duration);
        dto.setLocation(location);
        dto.setPictureUri(pictureUri);
        try {
            dto.setTicketPrice(Integer.parseInt(ticketPrice));
            dto.setCapacity(Integer.parseInt(capacity));
            dto.setStartTime(LocalDateTime.parse(startTime));
        } catch (NumberFormatException ex) {
            log.debug("capacity or ticketPrice was not a number on /programs!");
            return;
        } catch (DateTimeParseException ex) {
            log.debug("startTime was in invalid format on /programs!");
            return;
        }

        ProgramDTO newDTO = service.save(dto);

        resp.sendRedirect(caller + "?id=" + newDTO.getId());
    }

    private void handleDeletion(String caller, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String errorMsgEnding = " parameter was not set on /programs!";
        String id = req.getParameter("id");
        if (isEmpty(id)) {
            log.debug("id" + errorMsgEnding);
            return;
        }
        try {
            req.setAttribute("result", service.deleteById(Long.parseLong(id)));
            req.getRequestDispatcher(caller).forward(req, resp);
        } catch (NumberFormatException ex) {
            log.debug("id was not a number on /programs!");
        }
    }

}
