package communication.servlet;

import communication.ServletErrorHandler;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import service.dto.ImageDTO;
import service.serviceclasses.ImageService;
import service.utilities.FileSystemService;
import service.utilities.FileSystemService.Folder;

/**
 *
 * @author Niki
 */
@Slf4j
@WebServlet(name = "ImageServlet", urlPatterns = {"/images"})
public class ImageServlet extends HttpServlet implements ServletErrorHandler {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient ImageService imageService;

    @Inject
    private transient FileSystemService fileSystemService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("image/jpg");

        if (request.getParameter("id") != null) {

            try {
                Long imageId = Long.parseLong(request.getParameter("id"));
                ImageDTO image = imageService.findByID(imageId);
                fileSystemService.writeImageIntoOutputstream(Folder.valueOf(image.getType()), image.getName(), response.getOutputStream());
            } catch (NumberFormatException ex) {
                log.debug("Number format exception when parsing parameter id" + ex.toString());
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
