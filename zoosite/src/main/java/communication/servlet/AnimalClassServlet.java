/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication.servlet;

import communication.ServletErrorHandler;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import service.dto.AnimalClassDTO;
import service.serviceclasses.AnimalClassService;

/**
 *
 * @author Niki
 */
@Slf4j
@WebServlet(name = "AnimalClassServlet", urlPatterns = {"/internal/animalclass"})
@ServletSecurity(httpMethodConstraints = {
    @HttpMethodConstraint(value = "POST", rolesAllowed = "admin")})
public class AnimalClassServlet extends HttpServlet implements ServletErrorHandler {

    private static final long serialVersionUID = 1L;
    @Inject
    private transient AnimalClassService animalClassService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String caller = (String) request.getAttribute("caller");

        int id = (int) request.getAttribute("id");

        AnimalClassDTO animalClass = animalClassService.getAnimalClassById(id);

        request.setAttribute("animalClass", animalClass);

        request.getRequestDispatcher(caller).forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String caller = req.getParameter("caller");
        String delete = req.getParameter("delete");
        if (isEmpty(caller)) {
            log.debug("caller was empty!");
            return;
        }
        if (isEmpty(delete) || "false".equalsIgnoreCase(delete)) {
            handleAnimalClassPersistence(caller, req, resp);
        } else {
            req.setAttribute("result", animalClassService.deleteById(parseID(req, resp)));
            req.getRequestDispatcher(caller).forward(req, resp);
        }
    }

    private long parseID(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        try {
            return Long.parseLong(id);
        } catch (NumberFormatException ex) {
            log.debug("id could not be parsed properly on /animals!");
            return -1L;
        }
    }

    private void handleAnimalClassPersistence(String caller, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String animalClass = req.getParameter("animalClass");
        String description = req.getParameter("description");
        String profilePictureUri = req.getParameter("profilePictureUri");

        if (isEmpty(animalClass)) {
            log.debug("animalClass parameter is required on /animals!");
            return;
        }
        AnimalClassDTO dto = new AnimalClassDTO();
        dto.setId(parseID(req, resp));
        if (!isEmpty(description)) {
            dto.setDescription(description);
        }
        if (!isEmpty(profilePictureUri)) {
            dto.setProfilePictureUri(profilePictureUri);
        }
        req.setAttribute("result", animalClassService.save(dto));
        req.setAttribute("animalClasses", animalClassService.getAllAnimalClasses());
        req.getRequestDispatcher(caller).forward(req, resp);
    }
}
