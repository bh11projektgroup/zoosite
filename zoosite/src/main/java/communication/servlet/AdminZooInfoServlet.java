package communication.servlet;

import communication.ServletErrorHandler;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import service.serviceclasses.ZooInformationService;

/**
 *
 * @author Niki
 */
@Slf4j
@WebServlet(name = "AdminZooInfoServlet", urlPatterns = {"/edit_zooinfo"})
@ServletSecurity(httpMethodConstraints = {
    @HttpMethodConstraint(value = "POST", rolesAllowed = "admin")})
public class AdminZooInfoServlet extends HttpServlet implements ServletErrorHandler {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient ZooInformationService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            Object result = service.getZooInfo();
            request.setAttribute("zooInfo", result);

            request.getRequestDispatcher("WEB-INF/Admin/editZooInfo.jsp").forward(request, response);

        } catch (IllegalArgumentException ex) {
            log.debug("Could not find the requested information!");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
