/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package communication;

/**
 *
 * @author Kenwulf
 */
public interface ServletErrorHandler {

    public default boolean isEmpty(String attribute) {
        return attribute == null || "".equals(attribute) || "null".equalsIgnoreCase(attribute);
    }
    
    public default boolean isNumber(String attribute) {
        String regex = "\\d+";
        return attribute != null && attribute.matches(regex);
    }

    public default boolean existsEmpty(String... args) {
        for (String arg : args) {
            if (isEmpty(arg)) {
                return true;
            }
        }
        return false;
    }
}
