package service.dto;

import lombok.Data;

@Data
public class ZooInfoDTO {
    private Long id;
    private String address;
    private String phone;
    private String email;
    private String description;
    private String openingHours;
}
