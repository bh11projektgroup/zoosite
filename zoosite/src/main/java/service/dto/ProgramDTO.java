package service.dto;

import java.time.LocalDateTime;
import lombok.Data;

/**
 *
 * @author Kenwulf
 */
@Data
public class ProgramDTO {

    private Long id;
    private String name;
    private String description;
    private String location;
    private LocalDateTime startTime;
    private String duration;
    private String pictureUri;
    private int capacity;
    private int ticketPrice;
    private ImageDTO imageDto;

}
