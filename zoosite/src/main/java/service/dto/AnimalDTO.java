/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.dto;

import java.time.LocalDate;
import lombok.Data;

/**
 *
 * @author Niki
 */
@Data
public class AnimalDTO {

    private Long id;

    private String type;

    private String name;

    private String sex;

    private int age;

    private LocalDate dateOfArrival;

    private String profilePictureUri;
    
    private ImageDTO imageDto;

}
