/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.dto;

import lombok.Data;

/**
 *
 * @author Kenwulf
 */
@Data
public class TicketDTO {

    private String ticketId;
    private ProgramDTO programDTO;
    private UserDTO userDTO;
}
