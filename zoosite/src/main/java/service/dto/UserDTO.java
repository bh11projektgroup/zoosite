/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.dto;

import java.time.LocalDateTime;
import lombok.Data;

/**
 *
 * @author Kiki
 */
@Data
public class UserDTO {

    private Long id;
    private String accountName;
    private String address;
    private String city;
    private String email;
    private String imageUri;
    private String name;
    private String password;
    private String postalCode;
    private LocalDateTime timeOfRegistration;

}
