/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.dto;

import java.util.List;
import lombok.Data;

/**
 *
 * @author Niki
 */
@Data
public class AnimalTypeDTO {

    private Long id;
    private Long classId;
    private String type;
    private String description;
    private String profilePictureUri;
    private List<AnimalDTO> animals;
    private ImageDTO imageDto;

}
