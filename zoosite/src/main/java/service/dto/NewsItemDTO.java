/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.dto;

import java.time.LocalDateTime;
import lombok.Data;

/**
 *
 * @author tamas
 */
@Data
public class NewsItemDTO {

    private Long id;
    private String title;
    private String description;
    private String body;
    private LocalDateTime creationDate;
    private String imageUri;
    private ImageDTO imageDto;

}
