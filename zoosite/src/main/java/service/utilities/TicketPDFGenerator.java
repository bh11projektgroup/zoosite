/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.utilities;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.util.Matrix;
import service.dto.TicketDTO;

/**
 *
 * @author Kenwulf
 */
@Stateless
public class TicketPDFGenerator {

    private PDDocument doc;
    private List<TicketDTO> ticketList;

    public TicketPDFGenerator() {
        doc = new PDDocument();
    }

    public byte[] getPDFByteArray(List<TicketDTO> ticketList) {
        this.ticketList = ticketList;
        buildPages();
        PDStream stream = new PDStream(doc);
        byte[] result;
        try {
            result = stream.toByteArray();
            doc.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            result = null;
            Logger.getLogger(TicketPDFGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    private void generatePage(TicketDTO dto) throws IOException {
        PDPage page = new PDPage();
        doc.addPage(page);
        page.setRotation(90);
        page.setMediaBox(PDRectangle.A6);
        PDPageContentStream cs = new PDPageContentStream(doc, page);
        cs.transform(new Matrix(0, 1, -1, 0, page.getMediaBox().getWidth(), 0));
        buildTicketInfo(dto, page, cs);
        buildPageBase(page, cs);
        cs.close();
    }

    private void buildPageBase(PDPage page, PDPageContentStream cs) throws IOException {
        cs.beginText();
        cs.newLineAtOffset(20, 240);
        cs.setFont(PDType1Font.COURIER, 10);
        cs.showText("TicketID:");
        cs.newLineAtOffset(0, -30);
        cs.showText("Location:");
        cs.newLineAtOffset(0, -20);
        cs.showText("Event starting time:");
        cs.newLineAtOffset(0, -20);
        cs.showText("Duration:");
        cs.newLineAtOffset(0, -20);
        cs.showText("Title:");
        cs.endText();

        cs.setNonStrokingColor(Color.DARK_GRAY);
        cs.addRect(20, page.getMediaBox().getWidth() - 20, page.getMediaBox().getHeight() - 40, 10);
        cs.addRect(20, 20, page.getMediaBox().getHeight() - 40, 10);
        cs.fill();
    }

    private void buildTicketInfo(TicketDTO dto, PDPage page, PDPageContentStream cs) throws IOException {
        cs.beginText();
        cs.newLineAtOffset(20, 230);
        cs.setFont(PDType1Font.COURIER_BOLD, 10);
        cs.showText("" + dto.getProgramDTO().getId());
        cs.newLineAtOffset(0, -10);
        cs.showText(dto.getTicketId());
        cs.newLineAtOffset(0, -20);
        cs.showText(dto.getProgramDTO().getLocation());
        cs.newLineAtOffset(0, -20);
        cs.showText(dto.getProgramDTO().getStartTime().toString());
        cs.newLineAtOffset(0, -20);
        cs.showText("" + dto.getProgramDTO().getDuration() + " hours");
        cs.newLineAtOffset(0, -20);
        cs.showText(dto.getProgramDTO().getName());
        cs.endText();
        cs.drawImage(getQRImg(dto),
                page.getMediaBox().getHeight() - 140,
                140,
                120, 120);
    }

    private void buildPages() {
        ticketList.forEach(t -> {
            try {
                generatePage(t);
            } catch (IOException ex) {
                ex.printStackTrace();
                Logger.getLogger(TicketPDFGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    private PDImageXObject getQRImg(TicketDTO dto) throws IOException {
        return PDImageXObject.createFromByteArray(doc, getQRBytes(dto), "QR");
    }

    private byte[] getQRBytes(TicketDTO dto) {
        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            ImageIO.write(getQRBufferedImg(dto), "png", os);
            return os.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(TicketPDFGenerator.class.getName()).log(Level.SEVERE, null, ex);
            return new byte[0];
        }
    }

    private BufferedImage getQRBufferedImg(TicketDTO dto) {
        QRCodeWriter barcodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = null;
        try {
            bitMatrix = barcodeWriter.encode("" + dto.getProgramDTO().getId()
                    + "/" + dto.getTicketId(),
                    BarcodeFormat.QR_CODE, 120, 120);
        } catch (WriterException ex) {
            Logger.getLogger(TicketPDFGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return MatrixToImageWriter.toBufferedImage(bitMatrix);
    }

}
