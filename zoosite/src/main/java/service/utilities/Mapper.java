/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.utilities;

/**
 *
 * @author Kenwulf
 */
public interface Mapper<E, D> {
    E mapToEntity(D dto);
    D mapToDTO(E entity);
}
