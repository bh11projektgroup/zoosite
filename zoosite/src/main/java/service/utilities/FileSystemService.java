package service.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.servlet.http.Part;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Niki
 */
@Slf4j
@Startup
@Singleton
public class FileSystemService {

    private final String PAYARA_HOME = System.getProperty("com.sun.aas.instanceRoot");

    private final String UPLOAD_DIR_NAME = "upload";

    private String uploadFolder;

    @PostConstruct
    public void init() {

        // creates the directory if it does not exist
        File uploadDir = new File(PAYARA_HOME + File.separatorChar + UPLOAD_DIR_NAME);
        System.out.println("Upload DIR: " + uploadDir);
        if (!uploadDir.exists()) {
            boolean isUploadDirCreated = uploadDir.mkdir();
            if (!isUploadDirCreated) {
                log.debug("Upload dir couldn't be created");
            }
        }
        uploadFolder = uploadDir.getPath();

        for (Folder folder : Folder.values()) {
            try {
                createSubfolder(folder.getName());
            } catch (Exception ex) {
                log.debug(FileSystemService.class.getName() + Level.SEVERE, ex.toString());
            }
        }

    }

    private void createSubfolder(String folderName) {
        File subFolder = new File(uploadFolder + File.separatorChar + folderName);
        if (!subFolder.exists()) {
            boolean isFolderCreated = subFolder.mkdir();
            if (!isFolderCreated) {
                log.debug("Upload dir couldn't be created");
            }
        }

    }

    public void saveFile(Folder folderName, Part filePart) {

        String fileName = "";
        try {
            fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        } catch (NullPointerException ex) {
            log.debug(FileSystemService.class.getName() + Level.SEVERE, ex.toString());
        }

        try (
                InputStream in = filePart.getInputStream();
                FileOutputStream out = new FileOutputStream(new File(uploadFolder + File.separatorChar + folderName.getName() + File.separatorChar + fileName));) {

            int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }

        } catch (IOException ex) {
            log.debug(FileSystemService.class.getName() + Level.SEVERE, ex.toString());
        }
    }

    public void writeImageIntoOutputstream(Folder folderName, String fileName, OutputStream os) {

        try (FileInputStream fis = new FileInputStream(new File(uploadFolder + File.separatorChar + folderName.getName() + File.separatorChar + fileName))) {
            int c;
            while ((c = fis.read()) != -1) {
                os.write(c);
            }
        } catch (FileNotFoundException ex) {
            log.debug(FileSystemService.class.getName() + Level.SEVERE, ex.toString());
        } catch (IOException ex) {
            log.debug(FileSystemService.class.getName() + Level.SEVERE, ex.toString());
        }

    }

    @Getter
    public enum Folder {
        PROGRAM("programs"), NEWS("news"), ANIMALTYPE("animalType"), ANIMAL("animal");

        private String name;

        private Folder(String name) {
            this.name = name;
        }
    }
}
