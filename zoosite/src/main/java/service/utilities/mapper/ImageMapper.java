package service.utilities.mapper;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import lombok.SneakyThrows;
import org.apache.commons.beanutils.BeanUtils;
import repository.entity.Image;
import service.dto.ImageDTO;
import service.utilities.Mapper;

/**
 *
 * @author Niki
 */
@Stateless
@LocalBean
public class ImageMapper implements Mapper<Image, ImageDTO> {

    @SneakyThrows
    @Override
    public Image mapToEntity(ImageDTO dto) {
        Image entity = new Image();
        BeanUtils.copyProperties(entity, dto);
        return entity;
    }

    @Override
    @SneakyThrows
    public ImageDTO mapToDTO(Image entity) {
        ImageDTO dto = new ImageDTO();
        BeanUtils.copyProperties(dto, entity);
        return dto;
    }
}
