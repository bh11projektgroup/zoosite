/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.utilities.mapper;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import repository.entity.UserGroup;
import service.dto.UserGroupDto;
import service.utilities.Mapper;

/**
 *
 * @author Kenwulf
 */
@Slf4j
@Stateless
@LocalBean
public class UserGroupMapper implements Mapper<UserGroup, UserGroupDto> {

    @SneakyThrows
    @Override
    public UserGroup mapToEntity(UserGroupDto dto) {
        UserGroup entity = new UserGroup();
        BeanUtils.copyProperties(entity, dto);
        return entity;
    }

    @SneakyThrows
    @Override
    public UserGroupDto mapToDTO(UserGroup entity) {
        UserGroupDto dto = new UserGroupDto();
        BeanUtils.copyProperties(dto, entity);
        return dto;
    }

}
