/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.utilities.mapper;

import java.util.stream.Collectors;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import repository.entity.AnimalClass;
import service.dto.AnimalClassDTO;
import service.utilities.Mapper;

/**
 *
 * @author Kenwulf
 */
@Stateless
@LocalBean
public class AnimalClassMapper implements Mapper<AnimalClass, AnimalClassDTO> {

    @Inject
    private AnimalTypeMapper typeMapper;

    @Override
    public AnimalClass mapToEntity(AnimalClassDTO dto) {
        AnimalClass entity = new AnimalClass();
        entity.setId(dto.getId());
        entity.setAnimalClass(dto.getAnimalClass());
        entity.setDescription(dto.getDescription());
        entity.setProfilePictureUri(dto.getProfilePictureUri());
        entity.setAnimalTypes(dto.getAnimalTypes()
                .stream()
                .map(typeMapper::mapToEntity)
                .collect(Collectors.toList())
        );
        return entity;
    }

    @Override
    public AnimalClassDTO mapToDTO(AnimalClass entity) {
        AnimalClassDTO dto = new AnimalClassDTO();
        dto.setId(entity.getId());
        dto.setAnimalClass(entity.getAnimalClass());
        dto.setDescription(entity.getDescription());
        dto.setProfilePictureUri(entity.getProfilePictureUri());
        dto.setAnimalTypes(entity.getAnimalTypes()
                .stream()
                .map(typeMapper::mapToDTO)
                .collect(Collectors.toList())
        );
        return dto;
    }

}
