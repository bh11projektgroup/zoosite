package service.utilities.mapper;

import java.util.ArrayList;
import java.util.stream.Collectors;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import repository.entity.AnimalType;
import repository.repositoryclasses.AnimalClassDAO;
import service.dto.AnimalTypeDTO;
import service.utilities.Mapper;

/**
 *
 * @author Kenwulf
 */
@Stateless
@LocalBean
public class AnimalTypeMapper implements Mapper<AnimalType, AnimalTypeDTO> {

    @Inject
    private AnimalMapper animalMapper;
    
    @Inject
    private ImageMapper imageMapper;

    @Inject
    private AnimalClassDAO classDAO;

    @Override
    public AnimalType mapToEntity(AnimalTypeDTO dto) {
        AnimalType entity = new AnimalType();
        if (dto.getId() != null) {
            entity.setId(dto.getId());
        }
        entity.setDescription(dto.getDescription());
        entity.setProfilePictureUri(dto.getProfilePictureUri());
        entity.setType(dto.getType());
        entity.setAnimalClass(classDAO.findById(dto.getClassId()).get());
        
        if (dto.getAnimals() != null) {
            entity.setAnimals(dto.getAnimals()
                    .stream()
                    .map(animalMapper::mapToEntity)
                    .collect(Collectors.toList())
            );
        } else {
            entity.setAnimals(new ArrayList<>());
        }
        if (dto.getImageDto() != null) {
            entity.setImage(imageMapper.mapToEntity(dto.getImageDto()));
        }
        
        return entity;
    }

    @Override
    public AnimalTypeDTO mapToDTO(AnimalType entity) {
//        System.out.println("In mapToDTO:" + entity.toString());
        AnimalTypeDTO dto = new AnimalTypeDTO();
        dto.setId(entity.getId());
        dto.setClassId(entity.getAnimalClass().getId());
        dto.setDescription(entity.getDescription());
        dto.setProfilePictureUri(entity.getProfilePictureUri());
        dto.setType(entity.getType());
        dto.setAnimals(entity.getAnimals()
                .stream()
                .map(animalMapper::mapToDTO)
                .collect(Collectors.toList())
        );
        if (entity.getImage() != null) {
            dto.setImageDto(imageMapper.mapToDTO(entity.getImage()));
        }
        return dto;
    }

}
