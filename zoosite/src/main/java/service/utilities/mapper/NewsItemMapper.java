package service.utilities.mapper;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import repository.entity.NewsItem;
import service.dto.NewsItemDTO;
import service.utilities.Mapper;

/**
 *
 * @author Kenwulf
 */
@Stateless
@LocalBean
@Slf4j
public class NewsItemMapper implements Mapper<NewsItem, NewsItemDTO> {
    
    @Inject
    private ImageMapper mapper;

    @SneakyThrows
    @Override
    public NewsItem mapToEntity(NewsItemDTO dto) {
        
        NewsItem entity = new NewsItem();
        
        BeanUtils.copyProperties(entity, dto);
        if (dto.getImageDto() != null) {
            entity.setImage(mapper.mapToEntity(dto.getImageDto()));
        }
        return entity;
    }

    @SneakyThrows
    @Override
    public NewsItemDTO mapToDTO(NewsItem entity) {
        
        NewsItemDTO dto = new NewsItemDTO();

        BeanUtils.copyProperties(dto, entity);
        if (entity.getImage() != null) {
            dto.setImageDto(mapper.mapToDTO(entity.getImage()));
        }
        return dto;
    }
}