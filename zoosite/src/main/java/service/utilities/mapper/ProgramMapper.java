package service.utilities.mapper;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import repository.entity.Program;
import service.dto.ProgramDTO;
import service.utilities.Mapper;

/**
 *
 * @author Kenwulf
 */
@Slf4j
@Stateless
@LocalBean
public class ProgramMapper implements Mapper<Program, ProgramDTO> {

    @Inject
    private ImageMapper imageMapper;

    @SneakyThrows
    @Override
    public Program mapToEntity(ProgramDTO dto) {
        Program entity = new Program();

        BeanUtils.copyProperties(entity, dto);
        if (dto.getImageDto() != null) {
            entity.setImage(imageMapper.mapToEntity(dto.getImageDto()));
        }
        return entity;
    }

    @SneakyThrows
    @Override
    public ProgramDTO mapToDTO(Program entity) {
        ProgramDTO dto = new ProgramDTO();

        BeanUtils.copyProperties(dto, entity);
        if (entity.getImage() != null) {
            dto.setImageDto(imageMapper.mapToDTO(entity.getImage()));
        }
        return dto;
    }

}
