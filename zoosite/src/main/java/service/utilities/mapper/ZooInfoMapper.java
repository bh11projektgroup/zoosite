package service.utilities.mapper;

import repository.entity.ZooInfo;
import service.dto.ZooInfoDTO;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import service.utilities.Mapper;

@Stateless
@LocalBean
public class ZooInfoMapper implements Mapper<ZooInfo, ZooInfoDTO> {

    @Override
    public ZooInfo mapToEntity(ZooInfoDTO zooInfoDTO) {
        ZooInfo zooInfo = new ZooInfo();
        zooInfo.setId(zooInfoDTO.getId());
        zooInfo.setAddress(zooInfoDTO.getAddress());
        zooInfo.setPhone(zooInfoDTO.getPhone());
        zooInfo.setEmail(zooInfoDTO.getEmail());
        zooInfo.setDescription(zooInfoDTO.getDescription());
        zooInfo.setOpeningHours(zooInfoDTO.getOpeningHours());

        return zooInfo;
    }

    @Override
    public ZooInfoDTO mapToDTO(ZooInfo zooInfo) {
        ZooInfoDTO zooInfoDTO = new ZooInfoDTO();
        zooInfoDTO.setId(zooInfo.getId());
        zooInfoDTO.setAddress(zooInfo.getAddress());
        zooInfoDTO.setPhone(zooInfo.getPhone());
        zooInfoDTO.setEmail(zooInfo.getEmail());
        zooInfoDTO.setDescription(zooInfo.getDescription());
        zooInfoDTO.setOpeningHours(zooInfo.getOpeningHours());

        return zooInfoDTO;
    }
}
