package service.utilities.mapper;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import repository.entity.Animal;
import repository.repositoryclasses.AnimalTypeDAO;
import service.dto.AnimalDTO;
import service.utilities.Mapper;

/**
 *
 * @author Kenwulf
 */
@Stateless
@LocalBean
public class AnimalMapper implements Mapper<Animal, AnimalDTO> {

    @Inject
    private AnimalTypeDAO animalTypeDAO;
    
    @Inject
    private ImageMapper imageMapper;

    @Override
    public Animal mapToEntity(AnimalDTO dto) {
        Animal entity = new Animal();
        entity.setId(dto.getId());
        entity.setAge(dto.getAge());
        entity.setDateOfArrival(dto.getDateOfArrival());
        entity.setName(dto.getName());
        entity.setProfilePictureUri(dto.getProfilePictureUri());
        entity.setSex(dto.getSex());
        entity.setType(animalTypeDAO.findByName(dto.getType()));
        if (dto.getImageDto() != null) {
            entity.setImage(imageMapper.mapToEntity(dto.getImageDto()));
        }
        return entity;
    }

    @Override
    public AnimalDTO mapToDTO(Animal entity) {
        AnimalDTO dto = new AnimalDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAge(entity.getAge());
        dto.setDateOfArrival(entity.getDateOfArrival());
        dto.setProfilePictureUri(entity.getProfilePictureUri());
        dto.setSex(entity.getSex());
        dto.setType(entity.getType().getType());
        if (entity.getImage() != null) {
            dto.setImageDto(imageMapper.mapToDTO(entity.getImage()));
        }
        return dto;
    }

}
