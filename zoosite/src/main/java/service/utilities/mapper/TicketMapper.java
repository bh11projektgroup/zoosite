/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.utilities.mapper;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import repository.entity.Ticket;
import repository.entity.id.TicketID;
import repository.repositoryclasses.ProgramDAO;
import repository.repositoryclasses.UserDAO;
import service.dto.TicketDTO;
import service.utilities.Mapper;

/**
 *
 * @author Kenwulf
 */
@Stateless
@LocalBean
public class TicketMapper implements Mapper<Ticket, TicketDTO> {
    
    @Inject
    ProgramDAO pDao;
    @Inject
    UserDAO uDao;
    @Inject
    ProgramMapper programMapper;
    @Inject
    UserMapper userMapper;
    
    @Override
    public Ticket mapToEntity(TicketDTO dto) {
        Ticket entity = new Ticket();
        TicketID id = new TicketID();
        id.setId(dto.getTicketId());
        id.setProgramId(dto.getProgramDTO().getId());
        entity.setId(id);
        entity.setProgram(pDao.findById(id.getProgramId()).get());
        entity.setUser(uDao.findById(dto.getUserDTO().getId()).get());
        return entity;
    }
    
    @Override
    public TicketDTO mapToDTO(Ticket entity) {
        TicketDTO dto = new TicketDTO();
        dto.setTicketId(entity.getId().getId());
        dto.setProgramDTO(programMapper.mapToDTO(entity.getProgram()));
        dto.setUserDTO(userMapper.mapToDTO(entity.getUser()));
        return dto;
    }
    
}
