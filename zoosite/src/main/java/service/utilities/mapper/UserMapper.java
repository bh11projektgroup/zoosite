package service.utilities.mapper;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import repository.entity.User;
import service.dto.UserDTO;
import service.utilities.Mapper;

/**
 *
 * @author tamas
 */
@Stateless
@LocalBean
@Slf4j
public class UserMapper implements Mapper<User, UserDTO> {

    @SneakyThrows
    @Override
    public User mapToEntity(UserDTO dto) {
        User entity = new User();
        BeanUtils.copyProperties(entity, dto);
        return entity;
    }

    @SneakyThrows
    @Override
    public UserDTO mapToDTO(User entity) {
        UserDTO dto = new UserDTO();
        BeanUtils.copyProperties(dto, entity);
        return dto;
    }

}
