package service.serviceclasses;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;
import repository.repositoryclasses.TicketDAO;
import service.dto.TicketDTO;
import service.utilities.mapper.TicketMapper;
import service.utilities.TicketPDFGenerator;

/**
 *
 * @author Kenwulf
 */
@Stateless
public class TicketService {
    
    @Inject
    private TicketDAO dao;
    @Inject
    private ProgramService pSer;
    @Inject
    private UserService uSer;
    @Inject
    private TicketPDFGenerator tGen;
    @Inject
    private TicketMapper mapper;

    /**
     * Takes parameters in String, then based on parameters, registers the
     * tickets in the database, and generates a byte[] of the tickets
     *
     * @param tickets contains information about the tickets to be generated in
     * the following format:
     * programID1:numberOfTickets1,programID2:numberOfTicket2...
     * @param userID the user's ID to whom the tickets will be registered for
     * @return the byte[] containing the PDF version of the generated tickets
     */
    public byte[] generateTicketsInPDF(String tickets, long userID) {
        List<TicketDTO> ticketList = getTicketDTOList(parseParams(tickets), userID);
        registerTickets(ticketList);
        return tGen.getPDFByteArray(ticketList);
    }
    
    private HashMap<Long, Long> parseParams(String params) {
        HashMap<Long, Long> result = new HashMap<>();
        String[] pairs = params.split(",");
        for (String pair : pairs) {
            String[] keyValue = pair.split(":");
            result.put(Long.valueOf(keyValue[0]), Long.valueOf(keyValue[1]));
        }
        return result;
    }
    
    private List<TicketDTO> getTicketDTOList(HashMap<Long, Long> ticketsMap, long userID) {
        List<TicketDTO> result = new LinkedList<>();
        ticketsMap.forEach((programID, numberOfTickets) -> {
            for (int i = 0; i < numberOfTickets; i++) {
                TicketDTO dto = new TicketDTO();
                dto.setTicketId(UUID.randomUUID().toString());
                dto.setProgramDTO(pSer.findByID(programID));
                dto.setUserDTO(uSer.findByID(userID));
                result.add(dto);
            }
        });
        return result;
    }
    
    private void registerTickets(List<TicketDTO> ticketList) {
        dao.saveAll(ticketList.stream()
                .map(mapper::mapToEntity)
                .collect(Collectors.toList()));
    }
}
