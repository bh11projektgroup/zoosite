package service.serviceclasses;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.codec.digest.DigestUtils;
import repository.entity.User;
import repository.repositoryclasses.UserDAO;
import repository.repositoryclasses.UserGroupDao;
import service.dto.UserDTO;
import service.dto.UserGroupDto;
import service.utilities.mapper.UserGroupMapper;
import service.utilities.mapper.UserMapper;

/**
 *
 * @author Kiki
 */
@Stateless
public class UserService {

    @Inject
    private UserDAO dao;
    @Inject
    private UserGroupDao groupDao;
    @Inject
    private UserMapper mapper;
    @Inject
    private UserGroupMapper userGroupMapper;

    public UserDTO findByID(long id) {
        Optional<User> result = dao.findById(id);
        if (result.isPresent()) {
            return mapper.mapToDTO(result.get());
        } else {
            return null;
        }

    }

    public List<UserDTO> getAllUsers() {
        return dao.findAll()
                .stream()
                .map(mapper::mapToDTO)
                .collect(Collectors.toList());
    }

    public Boolean save(UserDTO dto) {
        Object entity = dao.save(mapper.mapToEntity(dto));
        return entity != null;
    }

    public Boolean deleteById(long id) {
        dao.deleteById(id);
        return !dao.existsById(id);
    }

    public String encryptToSha256(String input) {
        return DigestUtils.sha256Hex(input);
    }

    public List<UserGroupDto> getAllUserGroups() {
        return groupDao.findAll()
                .stream()
                .map(userGroupMapper::mapToDTO)
                .collect(Collectors.toList());
    }

    public boolean deleteUserGroup(long id) {
        return groupDao.deleteById(id);
    }

    public boolean saveUserGroup(UserGroupDto dto) {
        groupDao.save(userGroupMapper.mapToEntity(dto));
        return groupDao.findByName(dto.getAccountName()).isPresent();
    }

}
