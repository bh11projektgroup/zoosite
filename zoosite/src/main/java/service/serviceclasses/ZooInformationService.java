package service.serviceclasses;

import repository.entity.ZooInfo;
import repository.repositoryclasses.ZooInfoDAO;
import service.dto.ZooInfoDTO;
import service.utilities.mapper.ZooInfoMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class ZooInformationService {

    @Inject
    private ZooInfoDAO dao;

    @Inject
    private ZooInfoMapper mapper;

    public ZooInfoDTO getZooInfo() {
        return mapper.mapToDTO(dao.getLatestZooInfo());
    }

    public List<ZooInfoDTO> getZooInfos() {
        List<ZooInfoDTO> dtos = new ArrayList<>();
        List<ZooInfo> entities = dao.findAll();

        for (ZooInfo entity : entities) {
            dtos.add(mapper.mapToDTO(entity));
        }
        return dtos;
    }

    public ZooInfoDTO getZooInfoById(long id) {
        if (!dao.existsById(id)) {
            throw new IllegalArgumentException();
        }
        ZooInfo entity = dao.findById(id).get();
        ZooInfoDTO dto = mapper.mapToDTO(entity);
        return dto;
    }

    public Boolean save(ZooInfoDTO dto) {
        Object entity = dao.save(mapper.mapToEntity(dto));
        return entity != null;
    }
}
