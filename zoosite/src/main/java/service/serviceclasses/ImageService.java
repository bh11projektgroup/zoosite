/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.serviceclasses;

import java.util.Optional;
import javax.ejb.Stateless;
import javax.inject.Inject;
import repository.entity.Image;
import repository.repositoryclasses.ImageDao;
import service.dto.ImageDTO;
import service.utilities.mapper.ImageMapper;

/**
 *
 * @author Niki
 */
@Stateless
public class ImageService {
    
    @Inject
    private ImageDao dao;
    
    @Inject
    private ImageMapper mapper;
    
    public ImageDTO findByID(long id) {
        Optional<Image> result = dao.findById(id);
        if (result.isPresent()) {
            return mapper.mapToDTO(result.get());
        } else {
            return null;
        }
    }
    
}
