package service.serviceclasses;

import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.Setter;
import repository.repositoryclasses.AnimalClassDAO;
import service.dto.AnimalClassDTO;
import service.exceptions.AnimalClassNotFoundException;
import service.utilities.mapper.AnimalClassMapper;

/**
 *
 * @author Niki
 */
@Singleton
@Setter
public class AnimalClassService {

    @Inject
    private AnimalClassDAO dao;
    @Inject
    private AnimalClassMapper mapper;

    public List<AnimalClassDTO> getAllAnimalClasses() {
        return dao.findAll()
                .stream()
                .map(mapper::mapToDTO)
                .collect(Collectors.toList());
    }

    public AnimalClassDTO getAnimalClassById(long id) {

        return mapper.mapToDTO(dao.findById(id)
                .orElseThrow(() -> new AnimalClassNotFoundException("AnimalClass not found!")));
    }

    public Boolean deleteById(long id) {
        dao.deleteById(id);
        return !dao.existsById(id);
    }

    public Boolean save(AnimalClassDTO dto) {
        Object entity = dao.save(mapper.mapToEntity(dto));
        return entity != null;
    }
}
