package service.serviceclasses;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.inject.Inject;
import repository.entity.NewsItem;
import repository.repositoryclasses.NewsItemDAO;
import service.dto.NewsItemDTO;
import service.utilities.mapper.NewsItemMapper;

/**
 *
 * @author tamas
 */
@Stateless
public class NewsItemService {

    @Inject
    private NewsItemDAO dao;
    @Inject
    private NewsItemMapper mapper;

    /**
     * Searches the DB for all the NewsItems
     *
     * @return a List containing all the NewsItemsDTOs found in the DB, or an
     * empty List if there aren't any
     */
    public List<NewsItemDTO> getAll() {
        List<NewsItemDTO> result = new LinkedList<>();
        dao.getNewsItems().forEach(ni -> result.add(mapper.mapToDTO(ni)));
        return result;
    }

    /**
     * Searches the DB for the ID given as paramater
     *
     * @param id a long type ID to search for
     * @return a single NewsItemDTO or null if not found, or the input parameter
     * was invalid
     */
    public NewsItemDTO getSingleNews(long id) {
        Optional<NewsItem> result = dao.findById(id);
        if (result.isPresent()) {
            return mapper.mapToDTO(result.get());
        } else {
            return null;
        }
    }

    /**
     * Searches the DB for the three newest NewsItems
     *
     * @return a List containing the three NewsItemDTO-s or an empty List if
     * there aren't any
     */
    public List<NewsItemDTO> getTop4() {
        List<NewsItemDTO> result = new ArrayList<>();
        dao.getTop4OrderByCreationTime().forEach(ni -> result.add(mapper.mapToDTO(ni)));
        return result;
    }

    public NewsItemDTO save(NewsItemDTO dto) {
        NewsItem entity = dao.save(mapper.mapToEntity(dto));
        NewsItemDTO newDto = mapper.mapToDTO(entity);
        return newDto;
    }

    public Boolean deleteById(long id) {
        dao.deleteById(id);
        return !dao.existsById(id);
    }

}
