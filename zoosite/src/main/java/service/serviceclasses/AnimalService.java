package service.serviceclasses;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.Setter;
import repository.entity.Animal;
import repository.repositoryclasses.AnimalDAO;
import service.dto.AnimalDTO;
import service.exceptions.AnimalNotFoundException;
import service.utilities.mapper.AnimalMapper;

/**
 *
 * @author Niki
 */
@Singleton
@Setter
public class AnimalService {

    @Inject
    private AnimalDAO dao;
    @Inject
    private AnimalMapper mapper;

    /**
     *
     * @return
     */
    public List<AnimalDTO> getAllAnimals() {
        return dao.findAll()
                .stream()
                .map(mapper::mapToDTO)
                .collect(Collectors.toList());
    }

    /**
     *
     * @param id
     * @return
     */
    public AnimalDTO getAnimalById(Long id) {

        Optional<Animal> animal = dao.findById(id);

        return mapper.mapToDTO(animal
                .orElseThrow(() -> new AnimalNotFoundException("Animal not found!")));
    }

    public Boolean deleteById(long id) {
        dao.deleteById(id);
        return !dao.existsById(id);
    }

    public AnimalDTO save(AnimalDTO dto) {
        Animal entity = dao.save(mapper.mapToEntity(dto));
        return mapper.mapToDTO(entity);
    }

}
