package service.serviceclasses;

import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.Setter;
import repository.entity.AnimalType;
import repository.repositoryclasses.AnimalTypeDAO;
import service.dto.AnimalTypeDTO;
import service.exceptions.AnimalTypeNotFoundException;
import service.utilities.mapper.AnimalTypeMapper;

/**
 *
 * @author Niki
 */
@Singleton
@Setter
public class AnimalTypeService {

    @Inject
    private AnimalTypeDAO dao;
    @Inject
    private AnimalTypeMapper mapper;

    public List<AnimalTypeDTO> getAllAnimalTypes() {
        return dao.findAll()
                .stream()
                .map(mapper::mapToDTO)
                .collect(Collectors.toList());
    }

    public AnimalTypeDTO getAnimalTypeById(Long id) {
        //TODO: return optional
        return mapper.mapToDTO(dao.findById(id)
                .orElseThrow(() -> new AnimalTypeNotFoundException("AnimalType not found")));
    }

    public Boolean deleteById(long id) {
        dao.deleteById(id);
        return !dao.existsById(id);
    }

    public AnimalTypeDTO save(AnimalTypeDTO dto) {
        AnimalType entity = dao.save(mapper.mapToEntity(dto));
        return mapper.mapToDTO(entity);        
    }

}
