package service.serviceclasses;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;
import repository.entity.Program;
import repository.repositoryclasses.ProgramDAO;
import service.dto.ProgramDTO;
import service.utilities.mapper.ProgramMapper;

/**
 *
 * @author tamas
 */
@Stateless
public class ProgramService {

    @Inject
    private ProgramDAO dao;
    @Inject
    private ProgramMapper mapper;

    public ProgramDTO findByID(long id) {
        Optional<Program> result = dao.findById(id);
        if (result.isPresent()) {
            return mapper.mapToDTO(result.get());
        } else {
            return null;
        }
    }

    public List getProgramsOrderedByDate() {
        return dao.getAllProgramsOrderedByDate()
                .stream()
                .map(mapper::mapToDTO)
                .collect(Collectors.toList());
    }

    public Boolean deleteById(long id) {
        dao.deleteById(id);
        return !dao.existsById(id);
    }

    public ProgramDTO save(ProgramDTO dto) {
        Program entity = dao.save(mapper.mapToEntity(dto));
        
        ProgramDTO pdto = mapper.mapToDTO(entity);
        
        return pdto;
    }
}
