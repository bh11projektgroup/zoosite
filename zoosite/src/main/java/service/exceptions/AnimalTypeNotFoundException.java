/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.exceptions;

/**
 *
 * @author Niki
 */
public class AnimalTypeNotFoundException extends RuntimeException {

    public AnimalTypeNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}