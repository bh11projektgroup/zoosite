<%@ page import="service.dto.ProgramDTO" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Object result = request.getAttribute("programs");
    if (result == null) {
        request.getRequestDispatcher("/programs").forward(request, response);
        return;
    }
    List<ProgramDTO> dto = (List<ProgramDTO>) result;
%>
<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>


    <!-----------------------------Bootstrap------------------------------------------>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>


    <link rel='stylesheet' type='text/css' media='screen' href='events.css'>

</head>

<body>
<!-----------------------------Navbar--------------------------------------------->
<%@include file="navbar.jsp" %>
<%@include file="loginFinal.jsp" %>
<%@include file="cart.jsp" %>
<!-----------------------------Content----------------------------------------------->
<div class="content-footer-container">

    <div class="banner-area">
        <div class="title">
            <h1>Események</h1>
        </div>
    </div>

    <div class="grass">
    </div>

    <div class="content-container">

        <div class="board">

            <div class="notices">
                <div class="card">
                    <div class="imgBox">
                        <img src="<%=dto.get(0).getPictureUri()%>">
                    </div>
                    <div class="details">
                        <h2><%=dto.get(0).getName()%></h2>
                        <p>
                            <%=dto.get(0).getDescription()%>
                        </p>
                    </div>
                </div>
                <div class="card">
                    <div class="imgBox">
                        <img src="<%=dto.get(1).getPictureUri()%>">
                    </div>
                    <div class="details">
                        <h2><%=dto.get(1).getName()%></h2>
                        <p>
                            <%=dto.get(1).getDescription()%>
                        </p>
                    </div>
                </div>
                <div class="card">
                    <div class="imgBox">
                        <img src="<%=dto.get(2).getPictureUri()%>">
                    </div>
                    <div class="details">
                        <h2><%=dto.get(2).getName()%></h2>
                        <p>
                            <%=dto.get(2).getDescription()%>
                        </p>
                    </div>
                </div>
                <div class="card">
                    <div class="imgBox">
                        <img src="<%=dto.get(3).getPictureUri()%>">
                    </div>
                    <div class="details">
                        <h2><%=dto.get(3).getName()%></h2>
                        <p>
                            <%=dto.get(3).getDescription()%>
                        </p>
                    </div>
                </div>
                <div class="card">
                    <div class="imgBox">
                        <img src="<%=dto.get(4).getPictureUri()%>">
                    </div>
                    <div class="details">
                        <h2><%=dto.get(4).getName()%></h2>
                        <p>
                            <%=dto.get(4).getDescription()%>
                        </p>
                    </div>
                </div>
                <div class="card">
                    <div class="imgBox">
                        <img src="<%=dto.get(5).getPictureUri()%>">
                    </div>
                    <div class="details">
                        <h2><%=dto.get(5).getName()%></h2>
                        <p>
                            <%=dto.get(5).getDescription()%>
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <!-----------------------------Footer--------------------------------------------->
    <%
        String uri = request.getRequestURI();
        String caller = uri.substring(uri.lastIndexOf("/") + 1);
    %>
    <%@include file="footer.jsp" %>

</div>
</body>

</html>