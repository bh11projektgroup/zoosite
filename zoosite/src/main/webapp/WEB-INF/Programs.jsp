<%-- 
    Document   : Programs
    Created on : 2020.03.15., 20:56:56
    Author     : Kenwulf
--%>

<%@page import="service.dto.ProgramDTO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Programs!</h1>
        <%
            List<ProgramDTO> programs = (List<ProgramDTO>) request.getAttribute("programs");
        %>
        <p><%=programs.get(0).toString()%></p>
    </body>
</html>
