<%-- 
    Document   : Animal
    Created on : 2020.03.01., 16:31:37
    Author     : Niki
--%>
<%@page import="service.dto.AnimalDTO"%>
<%@page import="service.dto.AnimalTypeDTO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
        
        <%
        AnimalTypeDTO animalType = (AnimalTypeDTO) request.getAttribute("animalTypes");
        %>
        <h1><%= animalType.getType() %></h1>
        <p><%= animalType.getDescription() %></p>
        <h2>Állataink: </h2>
        <%
            for (AnimalDTO animal : animalType.getAnimals()) {
               
                    %>
        <div class="card" idth="200px">
                    
            <img class="card-img-top" width="200px" src="<%= animal.getProfilePictureUri() %>" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><%=animal.getName()%></h5>
                        <p class="card-text"><small class="text-muted">Kor: <%= animal.getAge()%> </small></p>
                        <p class="card-text"><small class="text-muted">Nem: <%= animal.getSex()%> </small></p>
                        <p class="card-text"><small class="text-muted">Érkezett: <%= animal.getDateOfArrival()%> </small></p>
                    </div>
                </div>
        
        <%
                    
                }
            %>
        
    
    
        </div>
</body>
</html>
