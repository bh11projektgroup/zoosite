<%@page import="service.dto.AnimalTypeDTO"%>
<%@page import="java.util.List"%>
<%@page import="service.dto.AnimalClassDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List<AnimalClassDTO> animalClasses = (List<AnimalClassDTO>) request.getAttribute("animalClasses");
    List<AnimalTypeDTO> animalTypes = (List<AnimalTypeDTO>) request.getAttribute("animalTypes");

    AnimalTypeDTO animalType = new AnimalTypeDTO();
    if (request.getAttribute("animalType") != null) {
        animalType = (AnimalTypeDTO) request.getAttribute("animalType");
    }

%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="head.jsp" %>
    </head>
    <body>
        <div class="wrapper">
            <!-- Sidebar  -->
            <%@include file="sidebar.jsp" %>
            <!-- Page Content  -->
            <div id="content">
                <!-- Top Navbar  -->
                <%@include file="navbar.jsp" %>

                <div class="p-3 mb-2 bg-white border">
                    <h3 class="mb-2 font-weight-bold">Fajok szerkesztése</h3>
                    <div class="line mb-3"></div>
                    <nav class="navbar">

                        <form class="form-inline" method="get" action="<%=request.getContextPath()%>/edit_animaltypes" >
                            <label for="id">Faj kiválasztása: </label>
                            <select name="id" class="custom-select mr-sm-2">
                                <% for (AnimalTypeDTO type : animalTypes) {%>
                                <option value="<%= type.getId()%>"><%= type.getType()%></option>
                                <% }%>
                            </select>
                            <input class="btn btn-sm btn-primary" type="submit" value="Kiválaszt"/>
                        </form>
                        <a href="<%=request.getContextPath()%>/edit_animaltypes" class="btn btn-sm btn-primary">Új faj hozzáadása</a>
                    </nav>
                    <form method="POST" action="<%=request.getContextPath()%>/animals" enctype="multipart/form-data" >
                        <input type="hidden" name="caller" value="<%=request.getContextPath()%>/edit_animaltypes" />
                        <input type="hidden" name="dataCategory" value="ANIMALTYPE" />
                        <input type="hidden" name="id" value="<%= animalType.getId()%>" />                        
                        <input type="hidden" name="profilePictureUri" value="somedummy/image" />                        
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Faj</label>
                                                <input type="text" class="form-control" name="type" id="" aria-describedby="" placeholder="" value="<%= animalType.getType()%>">
                                                <!--    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="classId">Kategória: </label>
                                            <select name="classId" class="custom-select mr-sm-2">
                                                <% for (AnimalClassDTO c : animalClasses) {%>
                                                <option value="<%= c.getId()  %>" 
                                                        <%
                                                            if (animalType.getClassId() == c.getId()) { out.print("selected"); }
                                                            %>
                                                        ><%= c.getAnimalClass()%></option>
                                                <% }%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Leírás</label>
                                        <textarea class="form-control" name="description"><%= animalType.getDescription() %></textarea>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <label>Kép feltöltés</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="customFile" name="animalTypeImg">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    <script>
<!-- Makes the filename appear on the select -->
                                        $(".custom-file-input").on("change", function () {
                                            var fileName = $(this).val().split("\\").pop();
                                            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                                        });
                                    </script>
                                    <img id="img_preview" src="
                                     <% if (animalType.getImageDto() == null) {
                                             out.print("Photos/dummy_img.jpg");
                                         } else {
                                            out.print(request.getContextPath() + "/images?id=" + animalType.getImageDto().getId());
                                         }
                                     %> 
                                     " style="max-height: 300px; max-width: 300px;" alt="dsf image" class="img-thumbnail mt-2 mb-2"/>
                                    <script>
                                         <!--    Loads image preview -->
                                         function readURL(input) {
                                         if (input.files && input.files[0]) {
                                         var reader = new FileReader();
                                         reader.onload = function (e) {
                                         $('#img_preview').attr('src', e.target.result);
                                         }

                                         reader.readAsDataURL(input.files[0]);
                                         }
                                         }

                                         $("#customFile").change(function () {
                                         readURL(this);
                                         });
                                         </script>
                                </div>                  
                            </div>      
                            <button type="submit" class="btn btn-primary">Mentés</button>
                        </div>        
                    </form>           
                </div>                  
            </div>                  
        </div>                  
    </body>
</html>