<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="head.jsp" %>
    </head>
    <body>
        <div class="wrapper">
            <!-- Sidebar  -->
            <%@include file="sidebar.jsp" %>
            <!-- Page Content  -->
            <div id="content">
                <!-- Top Navbar  -->
                <%@include file="navbar.jsp" %>

                <div class="p-3 mb-2 bg-white border">
                    <h3 class="mb-2 font-weight-bold">Felhasználók</h3>
                    <div class="line mb-3"></div>
                    <div class="container-fluid">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Group</th>
                                    <th scope="col">Felhasználónév</th>
                                    <th scope="col">Név</th>
                                    <th scope="col">Cím</th>
                                    <th scope="col">E-mail</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row" class="align-middle">1</th>
                                    <td class="align-middle">Admin</td>
                                    <td class="align-middle">k.gizi</td>
                                    <td class="align-middle">Kiss Gizella</td>
                                    <td class="align-middle">1111 Budapest XY utca 50.</td>
                                    <td class="align-middle">k.gizi@email.hu</td>
                                    <td class="align-middle"><a type="button" href="#" class="btn btn-primary btn-sm mb-2"><i class='fas fa-edit'></i></a></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="align-middle">2</th>
                                    <td class="align-middle">Guest</td>
                                    <td class="align-middle">k.geza</td>
                                    <td class="align-middle">Kis Géza</td>
                                    <td class="align-middle">2580 Kistarcsa X utca 1.</td>
                                    <td class="align-middle">k.geza@email.hu</td>
                                    <td class="align-middle"><a type="button" href="#" class="btn btn-primary btn-sm mb-2"><i class='fas fa-edit'></i></a></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="align-middle">3</th>
                                    <td class="align-middle">Guest</td>
                                    <td class="align-middle">n.bela</td>
                                    <td class="align-middle">Nagy Béla</td>
                                    <td class="align-middle">1587 Budapest X utca 100.</td>
                                    <td class="align-middle">n.bela@email.hu</td>
                                    <td class="align-middle"><a type="button" href="#" class="btn btn-primary btn-sm mb-2"><i class='fas fa-edit'></i></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>