<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="service.dto.ZooInfoDTO"%>
<%
    ZooInfoDTO zooInfo = new ZooInfoDTO();
    if (request.getAttribute("zooInfo") != null) {
        zooInfo = (ZooInfoDTO) request.getAttribute("zooInfo");
    }
%>
<%-- 
    Document   : editZooInfo
    Created on : 2020.03.18., 14:02:50
    Author     : Niki
--%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="head.jsp" %>
    </head>
    <body>
        <div class="wrapper">
            <!-- Sidebar  -->
            <%@include file="sidebar.jsp" %>
            <!-- Page Content  -->
            <div id="content">
                <!-- Top Navbar  -->
                <%@include file="navbar.jsp" %>

                <div class="p-3 mb-2 bg-white border">
                    <h3 class="mb-2 font-weight-bold">Elérhetőség</h3>
                    <div class="line mb-3"></div>
                    <form id="editZooInfoForm" method="POST" action="<%=request.getContextPath()%>/info" >
                        <input type="hidden" name="caller" value="<%=request.getContextPath()%>/edit_zooinfo"/>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Cím</label>
                                        <input type="text" name="address" class="form-control" id="" aria-describedby="" placeholder="" value="<%= zooInfo.getAddress()%>">
                                        <!--    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Cím</label>
                                        <input type="text" name="email" class="form-control" id="" aria-describedby="" placeholder="" value="<%= zooInfo.getEmail()%>">
                                        <!--    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Telefon</label>
                                        <input type="text" name="phone" class="form-control" id="" aria-describedby="" placeholder="" value="<%= zooInfo.getPhone()%>">
                                        <!--    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Leírás</label>
                                        <textarea name="description" class="form-control"><%= zooInfo.getDescription()%></textarea>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Leírás</label>
                                        <div id="editor" class="form-group mb-2">
                                            <%= zooInfo.getOpeningHours()%>
                                        </div>
                                        <input type="hidden" id="cke" name="openingHours" value="OpeningHoursDefaultValue." />
                                        <script>
                                            var editor;
                                            ClassicEditor
                                                    .create(document.querySelector('#editor'))
                                                    .then(newEditor => {
                                                        editor = newEditor;
                                                    })
                                                    .catch(error => {
                                                        console.error(error);
                                                    });
                                            $('#editZooInfoForm').one('submit', function (e) {
                                                e.preventDefault();
                                                var ckeditorData = editor.getData();
                                                $("#cke").val(ckeditorData);
                                                $(this).submit();
                                            });

                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" >Mentés</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>