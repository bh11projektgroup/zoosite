<nav class="navbar navbar-expand-sm">
    <div class="container-fluid">

        <button type="button" id="sidebarCollapse" class="btn btn-primary btn-sm">
            <i class="fas fa-align-left"></i>
            <!--    <span>Toggle Sidebar</span> -->
        </button>
        <button class="btn btn-dark btn-sm d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-align-justify"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item active">
                    <form action="${pageContext.request.contextPath}/logout" method="post">
                        <input type="submit" class="btn btn-secondary" value="Logout" />
                    </form>
                    
                </li>
            </ul>
        </div>
    </div>
</nav>