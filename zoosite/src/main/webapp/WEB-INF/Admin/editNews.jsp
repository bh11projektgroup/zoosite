<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="service.dto.NewsItemDTO"%>
<%
    NewsItemDTO newsItem = new NewsItemDTO();
    if (request.getAttribute("newsItem") != null) {
        newsItem = (NewsItemDTO) request.getAttribute("newsItem");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="head.jsp" %>
    </head>
    <body>
        <div class="wrapper">
            <!-- Sidebar  -->
            <%@include file="sidebar.jsp" %>
            <!-- Page Content  -->
            <div id="content">
                <!-- Top Navbar  -->
                <%@include file="navbar.jsp" %>

                <div class="p-3 mb-2 bg-white border">
                    <h3 class="mb-2 font-weight-bold">
                        <%
                            if (newsItem.getId() == null) {
                                out.println("Új hír létrehozása");
                            } else {
                                out.println("Hír szerkesztése");
                            }
                            %>
                    </h3>
                    <div class="line mb-3"></div>
                    <form id="newsForm" method="POST"  action="<%=request.getContextPath()%>/news" enctype="multipart/form-data">
                        <input type="hidden" name="caller" value="<%=request.getContextPath()%>/edit_news" />
                        <input type="hidden" name="id" value="<%= newsItem.getId() %>" />
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-8">
                                    <div class="form-group">
                                        <label>Cím</label>
                                        <input type="text" name="title" class="form-control" id="" aria-describedby="" placeholder="" value="<%= newsItem.getTitle() %>" required>
                                        <!--    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                    <div class="form-group">
                                        <label>Leírás</label>
                                        <textarea name="description" class="form-control" required><%= newsItem.getDescription() %></textarea>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <label>Kép feltöltés</label>
                                    <input type="hidden" value="dummy/imageUri" name="imageUri" />
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="customFile" name="newsImg">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    <script>
<!-- Makes the filename appear on the select -->
                                        $(".custom-file-input").on("change", function () {
                                            var fileName = $(this).val().split("\\").pop();
                                            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                                        });
                                    </script>
                                    <img id="img_preview" src="
                                        <% if (newsItem.getImageDto() == null) {
                                             out.print("Photos/dummy_img.jpg");
                                         } else {
                                            out.print(request.getContextPath() + "/images?id=" + newsItem.getImageDto().getId());
                                         }
                                     %>
                                        " style="max-height: 300px; max-width: 300px;" alt="dsf image" class="img-thumbnail mt-2 mb-2"/>
                                        <script>
        <!--    Loads image preview -->
                                            function readURL(input) {
                                                if (input.files && input.files[0]) {
                                                    var reader = new FileReader();

                                                    reader.onload = function (e) {
                                                        $('#img_preview').attr('src', e.target.result);
                                                    }

                                                    reader.readAsDataURL(input.files[0]);
                                                }
                                            }

                                            $("#customFile").change(function () {
                                                readURL(this);
                                            });
                                            </script>
                                        </div>
                                    </div>
                                </div>

                                <div id="editor" class="form-group mb-2">
                                ${newsItem.body}
                            </div>
                            <input type="hidden" id="cke" name="bodyContent" value="asdf" />
           
                                <br />
                                <button type="submit" class="btn btn-primary" >Mentés</button>
                            </form>
                        </div>
                    </div>
                </div>
                             <script>
                                var editor;
                                ClassicEditor
                                        .create(document.querySelector('#editor'))
                                        .then(newEditor =>{
                                            editor=newEditor;
                                        })
                                        .catch(error => {
                                            console.error(error);
                                        });
                                        $('#newsForm').one('submit', function(e) {
                                            e.preventDefault();
                                            var ckeditorData = editor.getData();
                                            $("#cke").val(ckeditorData);
                                            $(this).submit();
                                        });
                                 
                                </script>
            </body>
</html>