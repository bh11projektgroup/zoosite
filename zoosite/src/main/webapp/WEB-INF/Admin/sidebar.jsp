<%@page contentType="text/html" pageEncoding="UTF-8"%>
<nav id="sidebar">
    <div class="sidebar-header">
        <h3><a href="<%=request.getContextPath()%>/admin">Zoosite Admin UI</a></h3>
    </div>

    <ul class="list-unstyled components">
        <li class="active">
            <a href="#newsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-newspaper"></i> Hírek</a>
            <ul class="collapse list-unstyled" id="newsSubmenu">
                <li>
                    <a href="<%=request.getContextPath()%>/edit_news">Új hír létrehozása</a>
                </li>
                <li>
                    <a href="<%=request.getContextPath()%>/select_news">Korábbi hírek</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#animalsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-paw"></i> Állatok</a>
            <ul class="collapse list-unstyled" id="animalsSubmenu">
                <li>
                    <a href="<%=request.getContextPath()%>/edit_animaltypes">Fajok</a>
                </li>
                <li>
                    <a href="<%=request.getContextPath()%>/edit_animals">Állatok</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#eventsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-table"></i> Események</a>
            <ul class="collapse list-unstyled" id="eventsSubmenu">
                <li>
                    <a href="<%=request.getContextPath()%>/edit_program">Új esemény létrehozása</a>
                </li>
                <li>
                    <a href="<%=request.getContextPath()%>/select_program">Események kezelése</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="<%=request.getContextPath()%>/edit_users"><i class="fas fa-user"></i> Felhasználók</a>
        </li>
        <li>
            <a href="<%=request.getContextPath()%>/edit_zooinfo"><i class="fas fa-info-circle"></i> Elérhetőség</a>
        </li>
    </ul>
</nav>