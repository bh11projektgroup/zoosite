<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="service.dto.ProgramDTO"%>
<%
    ProgramDTO program = new ProgramDTO();
    if (request.getAttribute("program") != null) {
        program = (ProgramDTO) request.getAttribute("program");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="head.jsp" %>
    </head>
    <body>
        <div class="wrapper">
            <!-- Sidebar  -->
            <%@include file="sidebar.jsp" %>
            <!-- Page Content  -->
            <div id="content">
                <!-- Top Navbar  -->
                <%@include file="navbar.jsp" %>

                <div class="p-3 mb-2 bg-white border">
                    <h3 class="mb-2 font-weight-bold"><% 
                        if (program.getId() == null) {
                            out.println("Esemény létrehozása");
                        } else {
                            out.println("Esemény szerkesztése");
                        }
                        %></h3>
                    <div class="line mb-3"></div>
                    <form id="newsForm" method="POST" action="<%=request.getContextPath()%>/programs" enctype="multipart/form-data">
                        <input type="hidden" name="caller" value="<%=request.getContextPath()%>/edit_program" />
                        <input type="hidden" name="id" value="<%= program.getId()%>" />
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-8">
                                    <div class="form-group">
                                        <label>Név</label>
                                        <input type="text" name="name" class="form-control" id="" aria-describedby="" placeholder="" value="<%= program.getName()%>" required>
                                        <!--    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                    <div class="form-group">
                                        <label>Leírás</label>
                                        <textarea name="description" class="form-control" required><%= program.getDescription()%></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Hely</label>
                                        <input type="text" name="location" class="form-control" id="" aria-describedby="" placeholder="" value="<%= program.getLocation()%>" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Kezdés</label>
                                        <input type="text" name="startTime" class="form-control" id="" aria-describedby="" placeholder="" value="<%= program.getStartTime()%>" required>
                                        <!--    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>
                                        <div class="form-group">
                                        <label>Időtartam</label>
                                        <input type="text" name="duration" class="form-control" id="" aria-describedby="" placeholder="" value="<%= program.getDuration()%>" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Helyek száma</label>
                                        <input type="text" name="capacity" class="form-control" id="" aria-describedby="" placeholder="" value="<%= program.getCapacity()%>"required>
                                    </div>
                                    <div class="form-group">
                                        <label>Jegyár (HUF)</label>
                                        <input type="text" name="ticketPrice" class="form-control" id="" aria-describedby="" placeholder="" value="<%= program.getTicketPrice()%>" required>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <label>Kép feltöltés</label>
                                    <input type="hidden" value="dummy/imageUri" name="pictureUri" />
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="customFile" name="programImg" value="">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    <script>
<!-- Makes the filename appear on the select -->
                                        $(".custom-file-input").on("change", function () {
                                            var fileName = $(this).val().split("\\").pop();
                                            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                                        });
                                    </script>
                                    <img id="img_preview" src="
                                     <% if (program.getImageDto() == null) {
                                             out.print("Photos/dummy_img.jpg");
                                         } else {
                                            out.print(request.getContextPath() + "/images?id=" + program.getImageDto().getId());
                                         }
                                     %> 
                                     " style="max-height: 300px; max-width: 300px;" alt="dsf image" class="img-thumbnail mt-2 mb-2"/>
                                     <script>
                                         <!--    Loads image preview -->
                                         function readURL(input) {
                                         if (input.files && input.files[0]) {
                                         var reader = new FileReader();
                                         reader.onload = function (e) {
                                         $('#img_preview').attr('src', e.target.result);
                                         }

                                         reader.readAsDataURL(input.files[0]);
                                         }
                                         }

                                         $("#customFile").change(function () {
                                         readURL(this);
                                         });
                                         </script>
                                </div>
                            </div>
                        </div>

                        <div id="editor" class="form-group mb-2">
                            ${newsItem.body}
                        </div>
                        <button type="submit" class="btn btn-primary" >Mentés</button>
                    </form>
                </div>
            </div>
        </div>
</body>
</html>
