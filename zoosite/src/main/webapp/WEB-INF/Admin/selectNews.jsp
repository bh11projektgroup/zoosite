<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="service.dto.NewsItemDTO"%>
<%
    List<NewsItemDTO> newsItems = (List<NewsItemDTO>) request.getAttribute("NewsItems");
%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="head.jsp" %>
        <style>
            @media (min-width: 34em) {
                .card-columns {
                    -webkit-column-count: 3;
                    -moz-column-count: 3;
                    column-count: 3;
                }
            }

            @media (min-width: 48em) {
                .card-columns {
                    -webkit-column-count: 4;
                    -moz-column-count: 4;
                    column-count: 4;
                }
            }

            @media (min-width: 62em) {
                .card-columns {
                    -webkit-column-count: 5;
                    -moz-column-count: 5;
                    column-count: 5;
                }
            }

            @media (min-width: 75em) {
                .card-columns {
                    -webkit-column-count: 6;
                    -moz-column-count: 6;
                    column-count: 6;
                }
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <!-- Sidebar  -->
            <%@include file="sidebar.jsp" %>
            <!-- Page Content  -->
            <div id="content">
                <!-- Top Navbar  -->
                <%@include file="navbar.jsp" %>
                <div class="card-columns">
                    <%
                        for (NewsItemDTO newsItem : newsItems) {
                    %>
                    <div class="card">
                        <div class="embed-responsive embed-responsive-16by9">
                            <img class="card-img-top embed-responsive-item" src="<%= request.getContextPath() + "/images?id=" + newsItem.getImageDto().getId() %>" alt="Card image cap">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><%=newsItem.getTitle()%></h5>
                            <p class="card-text"><small class="text-muted"><%=newsItem.getCreationDate()%></small></p>
                            <a type="button" href="<%=request.getContextPath()%>/edit_news?id=<%=newsItem.getId()%>" class="btn btn-primary btn-sm mb-2">Szerkesztés</a><br/>
                            <a type="button" onclick="return confirm('Biztos törölni akarod?')" href="<%=request.getContextPath()%>/delete_news?id=<%=newsItem.getId()%>" class="btn btn-danger btn-sm" >Törlés</a>
                        </div>
                    </div>
                    <%
                        }
                    %>

                </div>
            </div>
        </div>        
    </body>
</html>