<%@page import="com.sun.jmx.remote.internal.ArrayQueue"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.stream.Collectors"%>
<%@page import="java.util.Optional"%>
<%@page import="service.dto.AnimalClassDTO"%>
<%@page import="service.dto.AnimalTypeDTO"%>
<%@page import="java.util.List"%>
<%@page import="service.dto.AnimalDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
        request.setAttribute("caller", request.getContextPath() + "/edit_animals");

        List<AnimalClassDTO> animalClasses = (List<AnimalClassDTO>) request.getAttribute("animalClasses");
        
        AnimalDTO animal = (AnimalDTO) request.getAttribute("animal");
        
        List<AnimalDTO> animals = (List<AnimalDTO>) request.getAttribute("animals");
        
        List<AnimalTypeDTO> animalTypes = (List<AnimalTypeDTO>) request.getAttribute("animalTypes");
       
%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="head.jsp" %>
    </head>
    <body>
        <div class="wrapper">
            <!-- Sidebar  -->
            <%@include file="sidebar.jsp" %>
            <!-- Page Content  -->
            <div id="content">
                <!-- Top Navbar  -->
                <%@include file="navbar.jsp" %>

                <div class="p-3 mb-2 bg-white border">
                    <h3 class="mb-2 font-weight-bold">Állatok szerkesztése</h3>
                    <div class="line mb-3"></div>
                    <nav class="navbar">

                        <form class="form-inline" method="get" action="<%=request.getContextPath()%>/edit_animals" >
                            <label for="id">Állat kiválasztása: </label>
                            <select name="id" class="custom-select mr-sm-2 mb-2">
                                <%
                                    for (AnimalDTO a : animals
                                        ) {
                                %>
                                <option value="<%= a.getId()%>"><%= a.getName()%> (<%= a.getType() %>)</option>
                                <%
                                    }
                                %>
                            </select>
                            <input class="btn btn-sm btn-primary mb-2" type="submit" value="Kiválaszt"/>
                        </form>
                        <a href="<%=request.getContextPath()%>/edit_animals" class="btn btn-sm btn-primary mb-2">Új állat hozzáadása</a>
                    </nav>
                    <form method="post" enctype="multipart/form-data" action="<%=request.getContextPath()%>/animals">
                        <input type="hidden" name="dataCategory" value="ANIMAL" />
                        <input type="hidden" name="id" value="<%= animal.getId()%>" />
                        <input type="hidden" name="profilePictureUri" value="somedummy/image" /> 
                        <input type="hidden" name="caller" value="<%=request.getContextPath()%>/edit_animals" />
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Név</label>
                                                <input type="text" name="name" class="form-control" id="" aria-describedby="" placeholder="" value="<%= animal.getName()%>">
                                                <!--    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="id">Faj</label>
                                            <select name="type" class="custom-select mr-sm-2">
                                                <%
                                                    for (AnimalTypeDTO type : animalTypes

                                                    
                                                        ) {
                                                %>
                                                <option 
                                                    value="<%= type.getType() %>"<% if (type.getType() == animal.getType()) {
                                                                out.println(" selected");
                                                            }%>>
                                                    <%= type.getType()%>
                                                </option>
                                                <%
                                                    }
                                                %>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Nem</label>
                                                <input type="text" name="sex" class="form-control" id="" aria-describedby="" placeholder="" value="<%=animal.getSex()%>">
                                                <!--    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Életkor</label>
                                                <input type="text" name="age" class="form-control" id="" aria-describedby="" placeholder="" value="<%=animal.getAge()%>">
                                                <!--    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <label>Kép feltöltés</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="customFile" name="animalImg">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                    <script>
<!-- Makes the filename appear on the select -->
                                        $(".custom-file-input").on("change", function () {
                                            var fileName = $(this).val().split("\\").pop();
                                            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                                        });
                                    </script>
                                    <img id="img_preview" src="
                                     <% if (animal.getImageDto() == null) {
                                             out.print("Photos/dummy_img.jpg");
                                         } else {
                                            out.print(request.getContextPath() + "/images?id=" + animal.getImageDto().getId());
                                         }
                                     %> 
                                     " style="max-height: 300px; max-width: 300px;" alt="dsf image" class="img-thumbnail mt-2 mb-2"/>
                                    <script>
                                         <!--    Loads image preview -->
                                         function readURL(input) {
                                         if (input.files && input.files[0]) {
                                         var reader = new FileReader();
                                         reader.onload = function (e) {
                                         $('#img_preview').attr('src', e.target.result);
                                         }

                                         reader.readAsDataURL(input.files[0]);
                                         }
                                         }

                                         $("#customFile").change(function () {
                                         readURL(this);
                                         });
                                         </script>
                                </div>       
                            </div>
                            <button type="submit" class="btn btn-primary">Mentés</button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </body>
</html>