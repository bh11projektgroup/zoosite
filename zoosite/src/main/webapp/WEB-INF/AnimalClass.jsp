<%-- 
    Document   : News
    Created on : 2020.02.24., 15:23:15
    Author     : Kenwulf
--%>

<%@page import="repository.entity.AnimalType"%>
<%@page import="service.dto.AnimalTypeDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="service.dto.AnimalClassDTO"%>
<%@page import="service.dto.NewsItemDTO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <title>News Test</title>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
        <script src='main.js'></script>
        ​
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <link rel='stylesheet' type='text/css' media='screen' href='css/design-template.css'>

    </head>
    <body>

        ​
        <!------------------------Banner & Navbar------------------------------------>
        ​
        ​
        <!--------------------jumbotron------------------------->
        <div class="container">
            <%
                AnimalClassDTO animalClass = (AnimalClassDTO) request.getAttribute("animalClass");

                out.print("<h1>" + animalClass.getAnimalClass() + "</h1>");
                out.print("<p>" + animalClass.getDescription() + "</p>");
                List<AnimalTypeDTO> types = animalClass.getAnimalTypes();


            %>
            <div class="card-group">
                <%                    for (AnimalTypeDTO t : types) {

                %>
                <a href="/zoosite/animaltype?id=<%= t.getId()%>">
                    <div class="card" idth="200px">

                        <img class="card-img-top" src="<%=t.getProfilePictureUri()%>" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title"><%=t.getType()%></h5>

                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                </a>



                <%
                } %>
            </div>
            <%


            %>
        </div>
        <!--------------------Footer------------------------->

        <div class="footer-dark">
            <footer>
                <img class="footer-png" src="https://hemkerzoo.com/wp-content/uploads/2019/02/jungle-silhouette-Hemker-Park-and-Zoo.png" alt="">
                ​
                <div class="container">
                    ​
                    <div class="row">
                        <div class="col-sm-6 col-md-3 item">
                            <h3>Elérhetõségek</h3>
                            <ul>
                                <li>Telefon: +0724958398 </li>
                                <li>Telefon: +3670304050 </li>
                                <li>Email: zooSite@gmail.com </li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-4 item">
                            <h3>Nyitvatartás</h3>
                            <ul>
                                <li>December 1-től február 28-ig: 10. 00 - 16. 00</li>
                                <li>Március 1-től március 30-ig: 10. 00 - 18. 00</li>
                                <li>Március 31-től szeptember 30-ig: 10. 00 - 19. 00</li>
                            </ul>
                        </div>
                        <div class="col-md-5 item text">
                            <h3>Company Name</h3>
                            <p>Praesent sed lobortis mi. Suspendisse vel placerat ligula. Vivamus ac sem lacus. Ut vehicula rhoncus elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit pulvinar dictum vel in justo.</p>
                        </div>
                    </div>
                    <p class="copyright">ZooSite © 2020</p>
                </div>
            </footer>
        </div>

    </body>
</html>