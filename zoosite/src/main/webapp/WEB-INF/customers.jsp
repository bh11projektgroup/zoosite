<%-- 
    Document   : customers
    Created on : 2020. márc. 7., 17:21:57
    Author     : Kiki
--%>

<%@page import="service.dto.CustomerDTO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Customer</h1>
        <% List<CustomerDTO> dtos = (List<CustomerDTO>) request.getAttribute("Customers"); 
        out.print(dtos.size());
        for (CustomerDTO dto : dtos) {
                out.print(dto.getAccountName());
            }
        %>
    </body>
</html>
