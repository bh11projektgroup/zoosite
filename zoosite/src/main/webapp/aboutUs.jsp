<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <!-----------------------------Bootstrap------------------------------------------>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    <link rel='stylesheet' type='text/css' media='screen' href='swiper.min.css'>
    <link rel='stylesheet' type='text/css' media='screen' href='aboutUs.css'>

</head>

<body>
<!-----------------------------Navbar--------------------------------------------->
<%@include file="navbar.jsp" %>
<%@include file="loginFinal.jsp" %>
<%@include file="cart.jsp" %>
<!-----------------------------Content----------------------------------------------->
<div class="content-footer-container">
    <div class="content-container">

        <div class="bannerParallax" data-stellar-background-ratio="0.5">
            <h2>Csapatunk</h2>
        </div>

        <div class="sec1">

            <div class="background">
                <div class="swiper-container">

                    <div class="swiper-wrapper">

                        <div class="swiper-slide">
                            <div class="imgBx">
                                <img src="Photos/aboutUs/keeper1.jpg">
                            </div>
                            <div class="details">
                                <h3>John Doe<br><span>Állatgondozónk</span></h3>
                            </div>
                        </div>

                        <div class="swiper-slide">
                            <div class="imgBx">
                                <img src="Photos/aboutUs/keeper2.jpg">
                            </div>
                            <div class="details">
                                <h3>Maria Gonzales<br><span>Állatgondozónk</span></h3>
                            </div>
                        </div>

                        <div class="swiper-slide">
                            <div class="imgBx">
                                <img src="Photos/aboutUs/keeper3.jpg">
                            </div>
                            <div class="details">
                                <h3>Francesco Miguel<br><span>Állatgondozónk</span></h3>
                            </div>
                        </div>

                        <div class="swiper-slide">
                            <div class="imgBx">
                                <img src="Photos/aboutUs/keeper4.jpg">
                            </div>
                            <div class="details">
                                <h3>Lisa Heinz<br><span>Állatgondozónk</span></h3>
                            </div>
                        </div>

                        <div class="swiper-slide">
                            <div class="imgBx">
                                <img src="Photos/aboutUs/keeper5.jpg">
                            </div>
                            <div class="details">
                                <h3>Sophia Briggs<br><span>Állatgondozónk</span></h3>
                            </div>
                        </div>

                        <div class="swiper-slide">
                            <div class="imgBx">
                                <img src="Photos/aboutUs/keeper6.jpg">
                            </div>
                            <div class="details">
                                <h3>Ben Parker<br><span>Állatgondozónk</span></h3>
                            </div>
                        </div>

                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                </div>

            </div>

        </div>

        <div class="parallax" data-stellar-background-ratio="0.5">
            <h2>Történetünk</h2>
        </div>

        <div class="sec2">

            <ul>
                <li>
                    <div class="li-content">
                        <h3>Alapos Tervezés</h3>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type
                            specimen book.</p>
                    </div>
                </li>
                <li>
                    <div class="li-content">
                        <h3>Kivitelezés</h3>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type
                            specimen book.</p>
                    </div>
                </li>
                <li>
                    <div class="li-content">
                        <h3>Jelenlegi helyzetünk</h3>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type
                            specimen book. It has survived not only five centuries, but also the leap into
                            electronic typesetting, remaining essentially unchanged.</p>
                    </div>
                </li>
                <li>
                    <div class="li-content">
                        <h3>Jelenlegi helyzetünk</h3>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type
                            specimen book.</p>
                    </div>
                </li>
                <li>
                    <div class="li-content">
                        <h3>A jövőre tekíntve</h3>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type
                            specimen book.</p>
                    </div>
                </li>
            </ul>
        </div>

        <div class="parallaxFooter" data-stellar-background-ratio="0.5"></div>


    </div>

    <script type="text/javascript" src="JavaScripts/swiper.min.js"></script>

    <script src="https://code.jquery.com/jquery-2.2.4.js"></script>
    <script src="JavaScripts/jquery.stellar.js"></script>

    <script>
        var swiper = new Swiper('.swiper-container', {
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            coverflowEffect: {
                rotate: 20,
                stretch: 0,
                depth: 500,
                modifier: 1,
                slideShadows: true,
            },
            pagination: {
                el: '.swiper-pagination',
            },
        });
    </script>

    <script type="text/javascript">
        $(window).stellar({
            scrollProperty: 'scroll',
            horizontalScrolling: true,
            positionProperty: 'position'
        });
    </script>


    <!-----------------------------Footer--------------------------------------------->
    <%
        String uri = request.getRequestURI();
        String caller = uri.substring(uri.lastIndexOf("/") + 1);
    %>
    <%@include file="footer.jsp" %>

</div>
</body>

</html>