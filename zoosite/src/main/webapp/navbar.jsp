<%--
  Created by IntelliJ IDEA.
  User: Kiki
  Date: 2020. 03. 25.
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel='stylesheet' type='text/css' media='screen' href='navbar.css'>
<header>
    <ul>
        <li><a href="main-page.jsp">Főoldal</a></li>
        <li><a class="animals" onclick="toggleMenuFunction()">Állataink</a>
            <ul id="animals-dropdown" class="animals-dropdown">
                <li><a href="predators.jsp">Ragadozók</a></li>
                <li><a href="reptiles.jsp">Hüllők</a></li>
                <li><a href="mammals.jsp">Emlősök</a></li>
                <li><a href="birds.jsp">Madarak</a></li>
            </ul>
        </li>
        <li><a href="map.jsp">Térkép</a></li>
        <li><a href="events.jsp">Események</a></li>
        <li><a href="aboutUs.jsp">Rólunk</a></li>
        <button id="login-button" class="login-button">Bejelentkezés</button>
        <button id="cart-button" class="cart-button">Kosár</button>
    </ul>
</header>

<div id="banner" class="banner"></div>


<script type="text/javascript">
    function toggleMenuFunction() {
        document.getElementById("animals-dropdown").classList.toggle("show");
    }
</script>


<script type="text/javascript">
    window.addEventListener("scroll", function () {
        var header = document.querySelector("header");
        var banner = document.getElementById("banner");


        header.classList.toggle("sticky", window.scrollY > 0)
        banner.classList.toggle("sticky-banner", window.scrollY > 0)

    })
</script>


