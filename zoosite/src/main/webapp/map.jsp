<%@ page import="repository.entity.ZooInfo" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="service.dto.*" %><%--
  Created by IntelliJ IDEA.
  User: Csongi
  Date: 2020. 03. 22.
  Time: 6:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Object result = request.getAttribute("animalClasses");
    if (result == null) {
        request.setAttribute("caller", "map.jsp");
        request.getRequestDispatcher("/animals").forward(request, response);
        return;
    }
    List<AnimalClassDTO> dto = (List<AnimalClassDTO>) result;
%>

<% List<AnimalTypeDTO> predators = dto.get(0).getAnimalTypes(); %>
<% List<AnimalTypeDTO> birds = dto.get(1).getAnimalTypes(); %>
<% List<AnimalTypeDTO> mammals = dto.get(2).getAnimalTypes(); %>
<% List<AnimalTypeDTO> reptiles = dto.get(3).getAnimalTypes(); %>

<%List<AnimalDTO> pingus = birds.get(1).getAnimals();%>
<%List<AnimalDTO> tigers = predators.get(2).getAnimals();%>
<%List<AnimalDTO> lions = predators.get(0).getAnimals();%>
<%List<AnimalDTO> flamingos = birds.get(0).getAnimals();%>
<%List<AnimalDTO> monkeys = mammals.get(2).getAnimals();%>
<%List<AnimalDTO> bears = predators.get(3).getAnimals();%>
<%List<AnimalDTO> snakes = reptiles.get(1).getAnimals();%>
<%List<AnimalDTO> crocodiles = reptiles.get(0).getAnimals();%>
<%List<AnimalDTO> giraffes = mammals.get(1).getAnimals();%>
<%List<AnimalDTO> elephantes = mammals.get(0).getAnimals();%>

<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <!-----------------------------Bootstrap------------------------------------------>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>


    <link rel='stylesheet' type='text/css' media='screen' href='map.css'>
    <link rel="stylesheet" href="./card.css">


</head>


<body>
<!-----------------------------Navbar--------------------------------------------->

<%@include file="loginFinal.jsp" %>
<%@ include file="navbar.jsp" %>
<%@include file="cart.jsp" %>

<!-----------------------------Map----------------------------------------------->
<div class="map-footer-container">
    <div class="content" id="blur">
        <div class="board">
            <div class="pingu" onclick="selectPingus()"></div>
            <div class="bear" onclick="selectBears()"></div>
            <div class="chimpanzee" onclick="blurFunction()"></div>
            <div class="crocodile" onclick="blurFunction()"></div>
            <div class="elephant" onclick="blurFunction()"></div>
            <div class="flamingo" onclick="blurFunction()"></div>
            <div class="giraffe" onclick="blurFunction()"></div>
            <div class="lion" onclick="blurFunction()"></div>
            <div class="tiger" onclick="blurFunction();selectTiger()"></div>
            <div class="snake" onclick="blurFunction()"></div>


        </div>
    </div>

    <%--    <div class="popup" id="popup">--%>
    <%--        <img class="close-png" onclick="blurFunction()"--%>
    <%--             src="Photos/AnimalMap/Exit.png" width="50px">--%>
    <%--        <%@ include file="card.jsp" %>--%>
    <%--        <div id="popup-text"></div>--%>
    <%--    </div>--%>

<%--    <script type="text/javascript">--%>
<%--        function blurFunction() {--%>
<%--            var blur = document.getElementById('blur');--%>
<%--            blur.classList.toggle('active');--%>
<%--            var popup = document.getElementsByClassName('.popup');--%>
<%--            popup.classList.toggle('active');--%>
<%--        }--%>
<%--    </script>--%>

    <script type="text/javascript">
        function selectPingus() {
            var blur = document.getElementById('blur');
            blur.classList.toggle('active');
            var popup = document.getElementById('popup-pingus');
            popup.classList.toggle('active');
        }

        function selectBears() {
            var blur = document.getElementById('blur');
            blur.classList.toggle('active');
            var popup = document.getElementById('popup-bears');
            popup.classList.toggle('active');
        }

    </script>

    <div class="popup" id="popup-pingus">
        <img class="close-png" onclick="selectPingus()"
             src="Photos/AnimalMap/Exit.png" width="50px">
        <%@ include file="card-pingus.jsp" %>
        <div id="popup-text"></div>
    </div>
    <div class="popup" id="popup-bears">
        <img class="close-png" onclick="selectBears()"
             src="Photos/AnimalMap/Exit.png" width="50px">
        <%@ include file="card-bears.jsp" %>
        <div id="popup-text"></div>
    </div>

    <script src='js1.js'></script>
    <script src='js2.js'></script>
    <script  src="./card.js"></script>
    <!-----------------------------Sunrise--------------------------------------------->

    <div id="sunrise" class="sunrise"></div>
    <div id="sun" class="sun"></div>


    <script type="text/javascript">

        window.addEventListener("scroll", function () {
            myFunction()
        });

        var sunrise = document.getElementById('sunrise'),
            style = window.getComputedStyle(sunrise),
            bottom = style.getPropertyValue('bottom');


        var sun = document.getElementById('sun'),
            style = window.getComputedStyle(sun),
            bottomSun = style.getPropertyValue('bottom');

        var lastScroll = 0;
        var scroll = parseInt(bottom);
        var scrollForSun = parseInt(bottomSun);

        function myFunction() {

            var st = window.pageYOffset;

            console.log("last:" + lastScroll);
            console.log("this:" + st);
            var sum = Math.round(lastScroll - st);
            console.log("sum: " + sum);


            if (st > lastScroll) {
                // downscroll code
                scroll = scroll + st - lastScroll;
                scrollForSun = scrollForSun + st - lastScroll;


                lastScroll = st;


                document.getElementById('sunrise').style.top = -scroll / 26 + '%';
                document.getElementById('sun').style.top = -scrollForSun / 26 + '%';

            }
            if (st < lastScroll) {
                // upscroll code
                scroll = scroll + st - lastScroll;
                scrollForSun = scrollForSun + st - lastScroll;


                lastScroll = st;


                document.getElementById('sunrise').style.top = -scroll / 26 + '%';
                document.getElementById('sun').style.top = -scrollForSun / 26 + '%';

                console.log("nem megy");
            }
        }


    </script>
    <!-----------------------------Footer--------------------------------------------->

    <%
        String uri = request.getRequestURI();
        String caller = uri.substring(uri.lastIndexOf("/") + 1);
    %>
    <%--    <jsp:include page="footer.jsp" >--%>
    <%--    <jsp:param name="caller" value= '<%= caller %>'/>--%>
    <%--    </jsp:include>--%>
    <%@ include file="footer.jsp" %>


</div>
</body>

</html>
