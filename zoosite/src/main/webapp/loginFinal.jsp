<%--
  Created by IntelliJ IDEA.
  User: Kiki
  Date: 2020. 03. 27.
  Time: 23:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel='stylesheet' type='text/css' media='screen' href='loginFinal.css'>

<div class="hero">
    <div class="form-box active">
        <button id="close-button" class="delButton"></button>
        <div class="button-box">
            <div id="btn"></div>
            <button type="button" class="toggle-btn" onclick="login()">Bejelentkezés</button>
            <button type="button" class="toggle-btn" onclick="register()">Regisztráció</button>
        </div>
        <div class="icons">
            <img src="Photos/animalicon1.png">
            <img src="Photos/animalicon2.png">
            <img src="Photos/animalicon3.png">
        </div>
        <form id="login" class="input-group">
            <input type="text" class="input-field" placeholder="Felhasználónév" required>
            <input type="text" class="input-field" placeholder="Jelszó" required>
            <input type="checkbox" class="check-box"><span>Jelszó mentés</span>
            <button type="submit" class="submit-btn">Bejelentkezés</button>
        </form>
        <form id="register" class="input-group">
            <input type="text" class="input-field" placeholder="Email" required>
            <input type="text" class="input-field" placeholder="Felhasználónév" required>
            <input type="text" class="input-field" placeholder="Jelszó" required>
            <input type="checkbox" class="check-box" required><span>Beleegyezek az Aszf-be</span>
            <button type="submit" class="submit-btn">Regisztráció</button>
        </form>
    </div>
</div>


<script>
    var x = document.getElementById("login");
    var y = document.getElementById("register");
    var z = document.getElementById("btn");

    function register() {
        x.style.left = "-400px";
        y.style.left = "50px";
        z.style.left = "160px";
    }

    function login() {
        x.style.left = "50px";
        y.style.left = "450px";
        z.style.left = "0px";
    }

</script>

<script>
    document.getElementById("login-button").addEventListener("click", function () {
        document.querySelector(".hero").style.display = "flex";
    })
    document.getElementById("close-button").addEventListener("click", function () {
        document.querySelector(".hero").style.display = "none";
    })

</script>

