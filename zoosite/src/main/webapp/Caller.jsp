<%-- 
    Document   : Caller
    Created on : 2020.03.15., 16:56:15
    Author     : Kenwulf
--%>

<%@page import="service.dto.AnimalClassDTO"%>
<%@page import="java.util.List"%>
<%@page import="service.dto.ZooInfoDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Caller Test</title>
    </head>
    <body>
        <form action="/zoosite/news" method="post">
            <input type="hidden" name="caller" value="Caller.jsp">
            Title:<input type="text" name="title"><br>
            Desc:<input type="text" name="description"><br>
            Body:<input type="text" name="body"><br>
            Image:<input type="text" name="imageUri"><br>
            <input type="submit">
        </form>
        <%
            Boolean result = (Boolean) request.getAttribute("result");
            if (result != null) {
                out.print(result);
            }
        %>
        
        <h1 style="color: green">Call Success!</h1>

    </body>
</html>