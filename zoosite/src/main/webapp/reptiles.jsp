<%@ page import="service.dto.AnimalClassDTO" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Kiki
  Date: 2020. 03. 24.
  Time: 8:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Object result = request.getAttribute("animalClasses");
    if (result == null) {
        request.setAttribute("caller", "reptiles.jsp");
        request.getRequestDispatcher("/animals").forward(request, response);
        return;
    }
    List<AnimalClassDTO> dto = (List<AnimalClassDTO>) result;
%>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <!-----------------------------Bootstrap------------------------------------------>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>


    <link rel='stylesheet' type='text/css' media='screen' href='reptiles.css'>

    <style>
        .content {
            width: 100%;
            height: auto;
            padding: 40px;
            box-sizing: border-box;
            text-align: justify;
        }

        section {
            width: 100%;
            height: 100vh;
        }

        section.sec1 {
            background: url("<%=dto.get(3).getAnimalTypes().get(0).getProfilePictureUri()%>");
            background-size: cover;
            background-attachment: fixed;
            background-repeat: no-repeat;
        }

        section.sec2 {
            background: url("<%=dto.get(3).getAnimalTypes().get(1).getProfilePictureUri()%>");
            background-size: cover;
            background-attachment: fixed;
            background-repeat: no-repeat;
        }

        section.sec3 {
            background: url("<%=dto.get(3).getAnimalTypes().get(2).getProfilePictureUri()%>");
            background-size: cover;
            background-attachment: fixed;
            background-repeat: no-repeat;
        }

    </style>

</head>

<body>
<!-----------------------------Navbar--------------------------------------------->
<%@include  file="navbar.jsp" %>
<%@include file="loginFinal.jsp" %>
<%@include file="cart.jsp" %>
<!-----------------------------Content----------------------------------------------->
<div class="content-footer-container">
    <div class="content-container">

        <div class="container">

            <section class="sectionHeader">
                <div class="sectionContent">

                    <h2><%=dto.get(3).getAnimalClass()%></h2>
                    <p><%=dto.get(3).getDescription()%></p>
                </div>
            </section>

            <div class="content">
                <h1><%=dto.get(3).getAnimalTypes().get(0).getType()%></h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore
                    agna aliqua. Nisl tincidunt eget nullam non. Quis hendrerit dolor magna eget est lorem ipsum
                    dolor sit.
                    Volutpat odio facilisis mauris sit amet massa. Commodo odio aenean sed adipiscing diam donec
                    adipiscing
                    tristique. Mi eget mauris pharetra et. Non tellus orci ac auctor augue. Elit at imperdiet dui
                    accumsan sit.
                    Ornare arcu dui vivamus arcu felis. Egestas integer eget aliquet nibh praesent. In hac habitasse
                    platea
                    dictumst quisque sagittis purus. Pulvinar elementum integer enim neque volutpat ac.
                </p>
            </div>
        </div>
        <section class="sec1" data-stellar-background-ratio="0.5"></section>

        <div class="container">
            <div class="content">
                <h1><%=dto.get(3).getAnimalTypes().get(1).getType()%></h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore
                    agna aliqua. Nisl tincidunt eget nullam non. Quis hendrerit dolor magna eget est lorem ipsum
                    dolor sit.
                    Volutpat odio facilisis mauris sit amet massa. Commodo odio aenean sed adipiscing diam donec
                    adipiscing
                    tristique. Mi eget mauris pharetra et. Non tellus orci ac auctor augue. Elit at imperdiet dui
                    accumsan sit.
                    Ornare arcu dui vivamus arcu felis. Egestas integer eget aliquet nibh praesent. In hac habitasse
                    platea
                    dictumst quisque sagittis purus. Pulvinar elementum integer enim neque volutpat ac.
                </p>
            </div>
        </div>

        <section class="sec2" data-stellar-background-ratio="0.5"></section>

        <div class="container">
            <div class="content">
                <h1><%=dto.get(3).getAnimalTypes().get(2).getType()%></h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    labore et dolore
                    agna aliqua. Nisl tincidunt eget nullam non. Quis hendrerit dolor magna eget est lorem ipsum
                    dolor sit.
                    Volutpat odio facilisis mauris sit amet massa. Commodo odio aenean sed adipiscing diam donec
                    adipiscing
                    tristique. Mi eget mauris pharetra et. Non tellus orci ac auctor augue. Elit at imperdiet dui
                    accumsan sit.
                    Ornare arcu dui vivamus arcu felis. Egestas integer eget aliquet nibh praesent. In hac habitasse
                    platea
                    dictumst quisque sagittis purus. Pulvinar elementum integer enim neque volutpat ac.
                </p>
            </div>
        </div>

        <section class="sec3" data-stellar-background-ratio="0.5"></section>

    </div>


    <script src="https://code.jquery.com/jquery-2.2.4.js"></script>
    <script src="JavaScripts/jquery.stellar.js"></script>

    <script type="text/javascript">
        $(window).stellar({
            scrollProperty: 'scroll',
            horizontalScrolling: true,
            positionProperty: 'position'
        });
    </script>


    <!-----------------------------Footer--------------------------------------------->
    <%
        String uri = request.getRequestURI();
        String caller = uri.substring(uri.lastIndexOf("/") + 1);
    %>
    <%@include file="footer.jsp" %>
</div>
</body>
</html>
