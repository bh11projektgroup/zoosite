<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="service.dto.AnimalClassDTO"%>
<%
    List<AnimalClassDTO> animalClasses = new ArrayList<>();
    if (request.getAttribute("animalClasses") != null) {
        animalClasses = (List<AnimalClassDTO>) request.getAttribute("animalClasses");
    } else {
        request.getRequestDispatcher("/navigation").forward(request, response);
    }
%>
<section id="banner">                  
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light mx-auto">

            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="main-page.jsp">Fooldal <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/zoosite/News">H�rek</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="/zoosite/AnimalClassServlet" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            �llataink
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <%
                                for (AnimalClassDTO animalClass : animalClasses) { %>
                                <a class="dropdown-item" href="animalclass?id=<%= animalClass.getId()
                                   %>"><%
                                    out.print(animalClass.getAnimalClass());
                                    %></a> 
                                    <%
                                }
                            %>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="/zoosite/AnimalClassServlet" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            R�lunk
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                           
                                <a class="dropdown-item" href="#">K�sz�ntj�k</a>
                                <a class="dropdown-item" href="#">T�rt�net�nk</a>
                                <a class="dropdown-item" href="#">Something</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Esem�nyek</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${pageContext.request.contextPath}/map">T�rk�p</a>
                    </li>


                </ul>
            </div>
        </nav>
    </div>
</section>