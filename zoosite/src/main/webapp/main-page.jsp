<%@ page import="service.dto.NewsItemDTO" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    Object result = request.getAttribute("newsItems");
    if (result == null) {
        request.setAttribute("caller", "main-page.jsp");
        request.setAttribute("top4", true);

        request.getRequestDispatcher("/news").forward(request, response);
        return; //this is important!!!
    }
    List<NewsItemDTO> dto = (List<NewsItemDTO>) result;
%>

<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <!-----------------------------Bootstrap------------------------------------------>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>


    <link rel='stylesheet' type='text/css' media='screen' href='main-page.css'>

</head>

<body>
<!-----------------------------Parallax1------------------------------------------->

<div class="Parallax1">
    <section class="zoom">
        <img src="Photos/Parallax1/bushes.png" id="layer1">
        <img src="Photos/Parallax1/bushes2.png" id="layer2">
        <img src="Photos/Parallax1/text.png" id="text">
    </section>
    <script type="text/javascript">
        var layer1 = document.getElementById('layer1')
        scroll = window.pageXOffset;
        document.addEventListener('scroll', function (e) {
            var offset = window.pageYOffset;
            scroll = offset;
            layer1.style.width = (100 + scroll / 5) + '%';
            layer1.style.left = (40 - scroll / 10) + '%';

        });

        var layer2 = document.getElementById('layer2')
        scroll = window.pageXOffset;
        document.addEventListener('scroll', function (e) {
            var offset = window.pageYOffset;
            scroll = offset;
            layer2.style.width = (100 + scroll / 5) + '%';
            layer2.style.left = scroll / 30 + '%';

        });

        var text = document.getElementById('text')
        scroll = window.pageXOffset;
        document.addEventListener('scroll', function (e) {
            var offset = window.pageYOffset;
            scroll = offset;
            text.style.top = -scroll / 28 + '%';
        });


    </script>
</div>

<!-----------------------------Navbar--------------------------------------------->
<%@include file="loginFinal.jsp" %>

<div class="banner">
    <header class="head">
        <ul>
            <li><a href="main-page.jsp">Főoldal</a></li>
            <li><a class="animals"  onclick="toggleMenuFunction()">Állataink</a>
                <ul id="animals-dropdown" class="animals-dropdown">
                    <li><a href="predators.jsp">Ragadozók</a></li>
                    <li><a href="reptiles.jsp">Hüllők</a></li>
                    <li><a href="mammals.jsp">Emlősök</a></li>
                    <li><a href="birds.jsp">Madarak</a></li>
                </ul>
            </li>
            <li><a href="map.jsp">Térkép</a></li>
            <li><a href="events.jsp">Események</a></li>
            <li><a href="aboutUs.jsp">Rólunk</a></li>
            <button id="login-button" class="login-button">Bejelentkezés</button>
        </ul>
    </header>


    <script type="text/javascript">
        function toggleMenuFunction() {
            document.getElementById("animals-dropdown").classList.toggle("show");
        }
    </script>

    <script type="text/javascript">

        window.addEventListener("scroll", function () {
            var header = document.querySelector("header");


            header.classList.toggle("sticky", window.scrollY > 0)

        })
    </script>

    <script type="text/javascript">
        var height = $('.Parallax1').height();
        $(window).scroll(function () {
            if ($(this).scrollTop() > height) {
                $('.head').addClass('sticky');
                $('header').addClass('sticky');
            } else $('.head').removeClass('sticky');
        })
    </script>
</div>
<!-----------------------------Parallax2------------------------------------------>
<div class="News">
    <div class="cave1"></div>
    <div class="cave2 rellax" data-rellax-speed="-6" data-rellax-percentage="0.7"></div>

    <script src="JavaScripts/rellax.min.js"></script>
    <script type="text/javascript">
        var rellax = new Rellax('.rellax');
    </script>


    <!-----------------------------News----------------------------------------------->


    <div class="container-hero" style="background:url(<%=dto.get(0).getImageUri()%>) center center / cover fixed">
        <div class="column active">
            <div id="1" class="content-hero">
                <h1>1</h1>
                <div id="box0" class="box">
                    <h2><%out.print(dto.get(0).getTitle());%></h2>
                    <p><% out.print(dto.get(0).getBody());%></p>
                </div>
            </div>
        </div>
        <div class="column">
            <div id="2" class="content-hero">
                <h1>2</h1>
                <div id="box1" class="box">
                    <h2><%out.print(dto.get(1).getTitle());%></h2>
                    <p><% out.print(dto.get(1).getBody());%></p>
                </div>
            </div>
        </div>
        <div class="column">
            <div id="3" class="content-hero">
                <h1>3</h1>
                <div id="box2" class="box">
                    <h2><%out.print(dto.get(2).getTitle());%></h2>
                    <p><% out.print(dto.get(2).getBody());%></p>
                </div>
            </div>
        </div>
        <div class="column">
            <div id="4" class="content-hero">
                <h1>4</h1>
                <div id="box3" class="box">
                    <h2><%out.print(dto.get(3).getTitle());%></h2>
                    <p><% out.print(dto.get(3).getBody());%></p>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript">
        $(document).on('mouseover', '.container-hero .column', function () {
            $(this).addClass('active').siblings().removeClass('active')
        });

        $("#1").mouseenter(
            function () {
                $('.container-hero').css("background", "url(<%=dto.get(0).getImageUri()%>)");
                $('.container-hero').css("background-size", "cover");
                $('.container-hero').css("background-attachment", "fixed");
                $('.container-hero').css("background-position", "center");
                $('.container-hero').css("transition", " 0.5s");
            });

        $("#1").mouseleave(
            function () {
                $('.container-hero').css("background-size", "cover");
                $('.container-hero').css("background-attachment", "fixed");
                $('.container-hero').css("background-position", "center");
                $('.container-hero').css("transition", " 0.5s");
            });



        $("#2").mouseenter(
            function () {
                $('.container-hero').css("background", "url(<%=dto.get(1).getImageUri()%>)");
                $('.container-hero').css("background-size", "cover");
                $('.container-hero').css("background-attachment", "fixed");
                $('.container-hero').css("background-position", "center");
                $('.container-hero').css("transition", " 0.5s");
            });

        $("#2").mouseleave(
            function () {
                $('.container-hero').css("background-size", "cover");
                $('.container-hero').css("background-attachment", "fixed");
                $('.container-hero').css("background-position", "center");
                $('.container-hero').css("transition", " 0.5s");
            });

        $("#3").mouseenter(
            function () {
                $('.container-hero').css("background", "url(<%=dto.get(2).getImageUri()%>)");
                $('.container-hero').css("background-size", "cover");
                $('.container-hero').css("background-attachment", "fixed");
                $('.container-hero').css("background-position", "center");
                $('.container-hero').css("transition", " 0.5s");
            });

        $("#3").mouseleave(
            function () {
                $('.container-hero').css("background-size", "cover");
                $('.container-hero').css("background-attachment", "fixed");
                $('.container-hero').css("background-position", "center");
                $('.container-hero').css("transition", " 0.5s");
            });

        $("#4").mouseenter(
            function () {
                $('.container-hero').css("background", "url(<%=dto.get(3).getImageUri()%>)");
                $('.container-hero').css("background-size", "cover");
                $('.container-hero').css("background-attachment", "fixed");
                $('.container-hero').css("background-position", "center");
                $('.container-hero').css("transition", " 0.5s");
            });

        $("#4").mouseleave(
            function () {
                $('.container-hero').css("background-size", "cover");
                $('.container-hero').css("background-attachment", "fixed");
                $('.container-hero').css("background-position", "center");
                $('.container-hero').css("transition", " 0.5s");
            });

    </script>

    <!-----------------------------Footer--------------------------------------------->

<%--    <%--%>
<%--        String uri = request.getRequestURI();--%>
<%--        String caller= uri.substring(uri.lastIndexOf("/")+1); %>--%>
<%--    <jsp:include page="footer.jsp" flush="false" >--%>
<%--        <jsp:param name="caller" value= '<%= caller %>'/>--%>
<%--    </jsp:include>--%>


<%--    <%String caller= "caller"; %>--%>
    <%
        String uri = request.getRequestURI();
        String caller= uri.substring(uri.lastIndexOf("/")+1);
        session.setAttribute("caller", "map.jsp");
    %>
    <%@ include file="footer.jsp" %>



</div>
</body>

</html>