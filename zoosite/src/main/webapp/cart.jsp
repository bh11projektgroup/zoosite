<%--
  Created by IntelliJ IDEA.
  User: Kiki
  Date: 2020. 03. 29.
  Time: 19:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel='stylesheet' type='text/css' media='screen' href='cart.css'>

<div class="cart">
    <div class="form-box-cart active">
        <button id="close-button-cart" class="delButton"></button>
        <div class="ticket-container">
            <button type="button" onclick="addTicketOne()">
                <img src="Photos/ticket/ticket1.png">
            </button>
            <button type="button" onclick="addTicketTwo()">
                <img src="Photos/ticket/ticket2.png">
            </button>
            <button type="button" onclick="addTicketThree()">
                <img src="Photos/ticket/ticket3.png">
            </button>
            <button type="button" onclick="addTicketFour()">
                <img src="Photos/ticket/ticket4.png">
            </button>
            <button type="button" onclick="addTicketFive()">
                <img src="Photos/ticket/ticket5.png">
            </button>
            <button type="button" onclick="addTicketSix()">
                <img src="Photos/ticket/ticket6.png">
            </button>
        </div>
        <div id="cart-container" class="cart-container">

        </div>
        <button id="buy-button" class="buy-button">Vásárlás</button>
    </div>
</div>

<script>
    document.getElementById("cart-button").addEventListener("click", function () {
        document.querySelector(".cart").style.display = "flex";
    })
    document.getElementById("close-button-cart").addEventListener("click", function () {
        document.querySelector(".cart").style.display = "none";
    })
</script>

<script type="text/javascript">
    pname = []
    pprice = []

    function addTicketOne() {
        pname.push("Első Esemény")
        pprice.push(parseInt(1000))
        displayCart()

    }

    function addTicketTwo() {
        pname.push("Második Esemeny")
        pprice.push(parseInt(1500))
        displayCart()

    }

    function addTicketThree() {
        pname.push("Harmadik Esemeny")
        pprice.push(parseInt(2500))
        displayCart()

    }

    function addTicketFour() {
        pname.push("Negyedik Esemeny")
        pprice.push(parseInt(3000))
        displayCart()

    }

    function addTicketFive() {
        pname.push("Ötödik Esemeny")
        pprice.push(parseInt(800))
        displayCart()

    }

    function addTicketSix() {
        pname.push("Hatodik Esemény")
        pprice.push(parseInt(1800))
        displayCart()

    }

    function displayCart() {
        cartdata = "<table><tr><th>Esemény száma</th><th>Ár</th><th>Összeg</th></tr>";
        total = 0;
        for (i = 0; i < pname.length ; i++) {
            total = total + pprice[i]
            cartdata = cartdata + "<tr><td>" + pname[i] + "</td><td>" +
                pprice[i] + "</td><td>"+ total +"</td><td>" + "<button onclick = delTicket(" + i + ")>Törlés</button></td></tr>"
        }
        cartdata = cartdata + "<tr><td></td><td></td><td></td><td></td></tr></table>"

        document.getElementById("cart-container").innerHTML = cartdata;
    }

    function delTicket(a) {
        pname.splice(a, 1);
        pprice.splice(a, 1);
        displayCart()
    }

</script>
