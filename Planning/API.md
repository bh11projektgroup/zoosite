## /news
- GET:
	- forwards back to the caller with the attribute newsItems:List\<NewsItemDTO\> set, based on the attributes provided
	- required attributes:
		- caller:String, must contain the name of the calling JSP (for example, if the caller is index.jsp
		then caller:"index.jsp" has to be set
		- top4:boolean, if true, returns a NewsItemDTO list with only the top 4 elements ordered by date,
		otherwise returns all the currently active NewsItemDTOs ordered by date

- POST:
	- admin only
	- if the delete:String parameter is not set creates a NewsItemDTO based on the submitted paramteres, then persists it in the DB,
	or, if delete is set, deletes the NewsItem according to the id:String paramter provided,
	then forwards to the caller with the attriubte result:boolean, true if success, false otherwise
	- required parameters:
		- caller:String
		- if delete:String parameter is set:
			- id:String
		- if delete:String parameter is not set:
			- title:String
			- description:String
			- body:String
			- imageUri:String
		
## /newsitems/{id}
- GET:
	- forwards to "SingleNews.jsp" with the attribute newsItem:NewsItemDTO
	containing the NewsItemDTO matching the submitted ID

## /info
- GET:
	- forwards back to the caller with the attribute zooInfo:ZooInformationDTO set
	- required attributes:
		- caller:String
- POST:
	- admin only
	- updates the information about the Zoo based on the attributes provided
	- required parameters:
		- caller
		- address
		- phone
		- email
		- description
		- openiningHours
## /users
- GET:
	- admin only
	- sets all Users in an attribute users:List\<UserDTO\>, and forwards to the admin panel
- POST:
	- updates, creates or deletes a user based on the paramteres provided
	- required paramateres:
		- caller
		- delete
		- if delete is set:
			- id
		- if delete is not set:
			- accountName
			- email
			- password
	- optional paramateres:
		- if delete is not set:
			- city
			- address
			- postalCode
			- imageUri

## /programs
- GET:
	- sets the attribute programs:List\<ProgramDTO\> and forwards to Programs.jsp
- POST:
	- admin only
	- if the delete:String parameter is not set creates a ProgramDTO based on the submitted paramteres, then persists it in the DB,
	or, if delete is set, deletes the ProgramDTO according to the id:String paramter provided,
	then forwards to the caller with the attriubte result:boolean, true if success, false otherwise
	- required parameters:
		- caller:String
		- if delete:String parameter is set:
			- id:String
		- if delete:String parameter is not set:
			- name:String
			- description:String
			- startTime:String with format: "2018-07-14T17:45:55.9483536"
			- duration:String like: "2 óra", "2 perc"
			- ticketPrice:String, must contain a number
			- capacity:String, must contain a number
			- pictureUri:String

## /animals
- GET:
	- forwards to the caller with the the attribute animalClasses:List<AnimalClassDTO> set
	- required attributes:
		- caller:String
- POST:
	- updates, deletes, or persists an animal, animal class, or an animal type
	- required paramateres:
		- caller
		- categoryParam:
			- accept String containing an enum value of: ANIMALTYPE, ANIMALCLASS, ANIMAL
		- ANIMAL:
			- delete
			- if delete set:
				- id
			- if delete not set:
				- name
				- type
		- ANIMALTYPE:
			- delete
			- if delete set:
				- id
			- if delete not set:
				- classId
				- type
		- ANIMALCLASS:
			- delete
			- if delete set:
				- id
			- if delete not set:
				- animalClass
	- optional paramateres:
		- ANIMAL:
			- if delete not set:
				- id: if set, an update will be executed
				- age
				- sex
				- profilePictureUri
		- ANIMALTYPE:
			- if delete not set:
				- id: if set, an update will be executed
				- description
				- profilePictureUri
		- ANIMALCLASS:
			- if delete not set:
				- id: if set, an update will be executed
				- description