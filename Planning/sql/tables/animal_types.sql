INSERT INTO animal_types (class_id, type, description, profile_picture_uri) VALUES (
1,
"Oroszlán",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat tortor, mollis ut erat et, molestie aliquet urna. Aliquam tincidunt ipsum rhoncus neque dapibus, sit amet porttitor quam aliquam. Pellentesque lacinia orci sed luctus faucibus. Donec quam massa, pharetra vel massa ac, mollis suscipit nulla. <br />Etiam dignissim metus a tortor fermentum auctor. Donec velit neque, lobortis a justo a, molestie mattis leo. Sed tempor purus orci, id sagittis arcu auctor non. Nullam lacinia tortor ac est bibendum, sed pellentesque velit dignissim. Ut dignissim elit sit amet quam rhoncus feugiat. Quisque non lacus diam. Suspendisse viverra dui nec urna cursus vehicula. Pellentesque luctus semper turpis, sed eleifend lacus accumsan sed. Vivamus varius erat a tellus consectetur, eu faucibus elit consectetur. Vivamus quis pretium nisi. Donec sodales velit a efficitur cursus. Ut eu nisl vitae risus tempor cursus non non elit.",
"https://cdn.nwmgroups.hu/s/img/i/1505/20150526oroszlan-lion-tekintet-kilatas-kihivas.jpg?w=644&h=362"
);

INSERT INTO animal_types (class_id, type, description, profile_picture_uri) VALUES (
1,
"Medve",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat tortor, mollis ut erat et, molestie aliquet urna. Aliquam tincidunt ipsum rhoncus neque dapibus, sit amet porttitor quam aliquam. Pellentesque lacinia orci sed luctus faucibus. Donec quam massa, pharetra vel massa ac, mollis suscipit nulla. <br />Etiam dignissim metus a tortor fermentum auctor. Donec velit neque, lobortis a justo a, molestie mattis leo. Sed tempor purus orci, id sagittis arcu auctor non. Nullam lacinia tortor ac est bibendum, sed pellentesque velit dignissim. Ut dignissim elit sit amet quam rhoncus feugiat. Quisque non lacus diam. Suspendisse viverra dui nec urna cursus vehicula. Pellentesque luctus semper turpis, sed eleifend lacus accumsan sed. Vivamus varius erat a tellus consectetur, eu faucibus elit consectetur. Vivamus quis pretium nisi. Donec sodales velit a efficitur cursus. Ut eu nisl vitae risus tempor cursus non non elit.",
"https://pmdvod.nationalgeographic.com/NG_Video/806/755/lgpost_1564002467927.jpg"
);

INSERT INTO animal_types (class_id, type, description, profile_picture_uri) VALUES (
1,
"Farkas",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat tortor, mollis ut erat et, molestie aliquet urna. Aliquam tincidunt ipsum rhoncus neque dapibus, sit amet porttitor quam aliquam. Pellentesque lacinia orci sed luctus faucibus. Donec quam massa, pharetra vel massa ac, mollis suscipit nulla. <br />Etiam dignissim metus a tortor fermentum auctor. Donec velit neque, lobortis a justo a, molestie mattis leo. Sed tempor purus orci, id sagittis arcu auctor non. Nullam lacinia tortor ac est bibendum, sed pellentesque velit dignissim. Ut dignissim elit sit amet quam rhoncus feugiat. Quisque non lacus diam. Suspendisse viverra dui nec urna cursus vehicula. Pellentesque luctus semper turpis, sed eleifend lacus accumsan sed. Vivamus varius erat a tellus consectetur, eu faucibus elit consectetur. Vivamus quis pretium nisi. Donec sodales velit a efficitur cursus. Ut eu nisl vitae risus tempor cursus non non elit.",
"https://www.dw.com/image/45189092_303.jpg"
);

INSERT INTO animal_types (class_id, type, description, profile_picture_uri) VALUES (
2,
"Zsiráf",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat tortor, mollis ut erat et, molestie aliquet urna. Aliquam tincidunt ipsum rhoncus neque dapibus, sit amet porttitor quam aliquam. Pellentesque lacinia orci sed luctus faucibus. Donec quam massa, pharetra vel massa ac, mollis suscipit nulla. <br />Etiam dignissim metus a tortor fermentum auctor. Donec velit neque, lobortis a justo a, molestie mattis leo. Sed tempor purus orci, id sagittis arcu auctor non. Nullam lacinia tortor ac est bibendum, sed pellentesque velit dignissim. Ut dignissim elit sit amet quam rhoncus feugiat. Quisque non lacus diam. Suspendisse viverra dui nec urna cursus vehicula. Pellentesque luctus semper turpis, sed eleifend lacus accumsan sed. Vivamus varius erat a tellus consectetur, eu faucibus elit consectetur. Vivamus quis pretium nisi. Donec sodales velit a efficitur cursus. Ut eu nisl vitae risus tempor cursus non non elit.",
"https://images.immediate.co.uk/production/volatile/sites/23/2018/09/Giraffes_Masai-Mara_Manoj-Shah_getty_623-19fe90d.jpg?quality=90&resize=614%2C409"
);

INSERT INTO animal_types (class_id, type, description, profile_picture_uri) VALUES (
2,
"Zebra",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat tortor, mollis ut erat et, molestie aliquet urna. Aliquam tincidunt ipsum rhoncus neque dapibus, sit amet porttitor quam aliquam. Pellentesque lacinia orci sed luctus faucibus. Donec quam massa, pharetra vel massa ac, mollis suscipit nulla. <br />Etiam dignissim metus a tortor fermentum auctor. Donec velit neque, lobortis a justo a, molestie mattis leo. Sed tempor purus orci, id sagittis arcu auctor non. Nullam lacinia tortor ac est bibendum, sed pellentesque velit dignissim. Ut dignissim elit sit amet quam rhoncus feugiat. Quisque non lacus diam. Suspendisse viverra dui nec urna cursus vehicula. Pellentesque luctus semper turpis, sed eleifend lacus accumsan sed. Vivamus varius erat a tellus consectetur, eu faucibus elit consectetur. Vivamus quis pretium nisi. Donec sodales velit a efficitur cursus. Ut eu nisl vitae risus tempor cursus non non elit.",
"https://www.dierenbox.nl/wp-content/uploads/2013/08/Bergzebra.jpg"
);

INSERT INTO animal_types (class_id, type, description, profile_picture_uri) VALUES (
2,
"Törpenyúl",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat tortor, mollis ut erat et, molestie aliquet urna. Aliquam tincidunt ipsum rhoncus neque dapibus, sit amet porttitor quam aliquam. Pellentesque lacinia orci sed luctus faucibus. Donec quam massa, pharetra vel massa ac, mollis suscipit nulla. <br />Etiam dignissim metus a tortor fermentum auctor. Donec velit neque, lobortis a justo a, molestie mattis leo. Sed tempor purus orci, id sagittis arcu auctor non. Nullam lacinia tortor ac est bibendum, sed pellentesque velit dignissim. Ut dignissim elit sit amet quam rhoncus feugiat. Quisque non lacus diam. Suspendisse viverra dui nec urna cursus vehicula. Pellentesque luctus semper turpis, sed eleifend lacus accumsan sed. Vivamus varius erat a tellus consectetur, eu faucibus elit consectetur. Vivamus quis pretium nisi. Donec sodales velit a efficitur cursus. Ut eu nisl vitae risus tempor cursus non non elit.",
"https://i.pinimg.com/originals/c7/cb/3b/c7cb3be624510e977b13baf98a4935e7.jpg"
);

INSERT INTO animal_types (class_id, type, description, profile_picture_uri) VALUES (
3,
"Sólyom",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat tortor, mollis ut erat et, molestie aliquet urna. Aliquam tincidunt ipsum rhoncus neque dapibus, sit amet porttitor quam aliquam. Pellentesque lacinia orci sed luctus faucibus. Donec quam massa, pharetra vel massa ac, mollis suscipit nulla. <br />Etiam dignissim metus a tortor fermentum auctor. Donec velit neque, lobortis a justo a, molestie mattis leo. Sed tempor purus orci, id sagittis arcu auctor non. Nullam lacinia tortor ac est bibendum, sed pellentesque velit dignissim. Ut dignissim elit sit amet quam rhoncus feugiat. Quisque non lacus diam. Suspendisse viverra dui nec urna cursus vehicula. Pellentesque luctus semper turpis, sed eleifend lacus accumsan sed. Vivamus varius erat a tellus consectetur, eu faucibus elit consectetur. Vivamus quis pretium nisi. Donec sodales velit a efficitur cursus. Ut eu nisl vitae risus tempor cursus non non elit.",
"https://cdn.nwmgroups.hu/s/img/i/1307/20130708-solyom-hungarian-airlines-illusztracio-a12.jpg?w=645&h=430"
);

INSERT INTO animal_types (class_id, type, description, profile_picture_uri) VALUES (
4,
"Orangután",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat tortor, mollis ut erat et, molestie aliquet urna. Aliquam tincidunt ipsum rhoncus neque dapibus, sit amet porttitor quam aliquam. Pellentesque lacinia orci sed luctus faucibus. Donec quam massa, pharetra vel massa ac, mollis suscipit nulla. <br />Etiam dignissim metus a tortor fermentum auctor. Donec velit neque, lobortis a justo a, molestie mattis leo. Sed tempor purus orci, id sagittis arcu auctor non. Nullam lacinia tortor ac est bibendum, sed pellentesque velit dignissim. Ut dignissim elit sit amet quam rhoncus feugiat. Quisque non lacus diam. Suspendisse viverra dui nec urna cursus vehicula. Pellentesque luctus semper turpis, sed eleifend lacus accumsan sed. Vivamus varius erat a tellus consectetur, eu faucibus elit consectetur. Vivamus quis pretium nisi. Donec sodales velit a efficitur cursus. Ut eu nisl vitae risus tempor cursus non non elit.",
"https://upload.wikimedia.org/wikipedia/commons/9/93/Orangutans2.jpg"
);

INSERT INTO animal_types (class_id, type, description, profile_picture_uri) VALUES (
4,
"Csimpánz",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat tortor, mollis ut erat et, molestie aliquet urna. Aliquam tincidunt ipsum rhoncus neque dapibus, sit amet porttitor quam aliquam. Pellentesque lacinia orci sed luctus faucibus. Donec quam massa, pharetra vel massa ac, mollis suscipit nulla. <br />Etiam dignissim metus a tortor fermentum auctor. Donec velit neque, lobortis a justo a, molestie mattis leo. Sed tempor purus orci, id sagittis arcu auctor non. Nullam lacinia tortor ac est bibendum, sed pellentesque velit dignissim. Ut dignissim elit sit amet quam rhoncus feugiat. Quisque non lacus diam. Suspendisse viverra dui nec urna cursus vehicula. Pellentesque luctus semper turpis, sed eleifend lacus accumsan sed. Vivamus varius erat a tellus consectetur, eu faucibus elit consectetur. Vivamus quis pretium nisi. Donec sodales velit a efficitur cursus. Ut eu nisl vitae risus tempor cursus non non elit.",
"https://www.erdekesvilag.hu/wp-content/uploads/2013/03/csimp%C3%A1nzok.jpg"
);

INSERT INTO animal_types (class_id, type, description, profile_picture_uri) VALUES (
5,
"Krokodil",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat tortor, mollis ut erat et, molestie aliquet urna. Aliquam tincidunt ipsum rhoncus neque dapibus, sit amet porttitor quam aliquam. Pellentesque lacinia orci sed luctus faucibus. Donec quam massa, pharetra vel massa ac, mollis suscipit nulla. <br />Etiam dignissim metus a tortor fermentum auctor. Donec velit neque, lobortis a justo a, molestie mattis leo. Sed tempor purus orci, id sagittis arcu auctor non. Nullam lacinia tortor ac est bibendum, sed pellentesque velit dignissim. Ut dignissim elit sit amet quam rhoncus feugiat. Quisque non lacus diam. Suspendisse viverra dui nec urna cursus vehicula. Pellentesque luctus semper turpis, sed eleifend lacus accumsan sed. Vivamus varius erat a tellus consectetur, eu faucibus elit consectetur. Vivamus quis pretium nisi. Donec sodales velit a efficitur cursus. Ut eu nisl vitae risus tempor cursus non non elit.",
"https://images.immediate.co.uk/production/volatile/sites/23/2014/11/GettyImages-123529247-2a29d6c.jpg?quality=90&resize=620%2C413"
);

INSERT INTO animal_types (class_id, type, description, profile_picture_uri) VALUES (
5,
"Béka",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis erat tortor, mollis ut erat et, molestie aliquet urna. Aliquam tincidunt ipsum rhoncus neque dapibus, sit amet porttitor quam aliquam. Pellentesque lacinia orci sed luctus faucibus. Donec quam massa, pharetra vel massa ac, mollis suscipit nulla. <br />Etiam dignissim metus a tortor fermentum auctor. Donec velit neque, lobortis a justo a, molestie mattis leo. Sed tempor purus orci, id sagittis arcu auctor non. Nullam lacinia tortor ac est bibendum, sed pellentesque velit dignissim. Ut dignissim elit sit amet quam rhoncus feugiat. Quisque non lacus diam. Suspendisse viverra dui nec urna cursus vehicula. Pellentesque luctus semper turpis, sed eleifend lacus accumsan sed. Vivamus varius erat a tellus consectetur, eu faucibus elit consectetur. Vivamus quis pretium nisi. Donec sodales velit a efficitur cursus. Ut eu nisl vitae risus tempor cursus non non elit.",
"https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Hyla_arborea_hun.jpg/1280px-Hyla_arborea_hun.jpg");