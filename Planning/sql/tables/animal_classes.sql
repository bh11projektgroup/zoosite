INSERT INTO animal_classes (class, description, profile_picture_uri) VALUES (
"Ragadozók", 
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non interdum metus, vel posuere risus. Maecenas vestibulum vulputate risus, commodo auctor odio vestibulum nec. Donec accumsan scelerisque mauris, vel laoreet enim maximus in. Aliquam at pretium erat. Nulla facilisis ipsum mi, a pulvinar ligula dignissim ac. Donec iaculis nisi sit.",
"https://lh3.googleusercontent.com/proxy/ohSycy6tiIEyffP1Z8KxHoj0e_AJ3aZqJ4C8o0Vpb2j_iEk3UNdjBvLxjNHZ1EQSxYrG4T_776OVQC30decVc46JRETTdNyMQDSdnJq3kEodBqJZGxNe3R3bANwZtbu9suVw9u8"
);

INSERT INTO animal_classes (class, description, profile_picture_uri) VALUES (
"Növényevők", 
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non interdum metus, vel posuere risus. Maecenas vestibulum vulputate risus, commodo auctor odio vestibulum nec. Donec accumsan scelerisque mauris, vel laoreet enim maximus in. Aliquam at pretium erat. Nulla facilisis ipsum mi, a pulvinar ligula dignissim ac. Donec iaculis nisi sit.",
"https://www.dierenbox.nl/wp-content/uploads/2013/08/Bergzebra.jpg"
);

INSERT INTO animal_classes (class, description, profile_picture_uri) VALUES (
"Madarak", 
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non interdum metus, vel posuere risus. Maecenas vestibulum vulputate risus, commodo auctor odio vestibulum nec. Donec accumsan scelerisque mauris, vel laoreet enim maximus in. Aliquam at pretium erat. Nulla facilisis ipsum mi, a pulvinar ligula dignissim ac. Donec iaculis nisi sit.",
"https://www.imperial.ac.uk/ImageCropToolT4/imageTool/uploaded-images/newseventsimage_1557320321101_mainnews2012_x1.jpg"
);

INSERT INTO animal_classes (class, description, profile_picture_uri) VALUES (
"Főemlősök", 
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non interdum metus, vel posuere risus. Maecenas vestibulum vulputate risus, commodo auctor odio vestibulum nec. Donec accumsan scelerisque mauris, vel laoreet enim maximus in. Aliquam at pretium erat. Nulla facilisis ipsum mi, a pulvinar ligula dignissim ac. Donec iaculis nisi sit.",
"https://static.independent.co.uk/s3fs-public/thumbnails/image/2014/02/18/20/monkey.jpg?w968h681"
);

INSERT INTO animal_classes (class, description, profile_picture_uri) VALUES (
"Hüllők", 
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non interdum metus, vel posuere risus. Maecenas vestibulum vulputate risus, commodo auctor odio vestibulum nec. Donec accumsan scelerisque mauris, vel laoreet enim maximus in. Aliquam at pretium erat. Nulla facilisis ipsum mi, a pulvinar ligula dignissim ac. Donec iaculis nisi sit.",
"https://phz8.petinsurance.com/-/media/all-phz-images/2017-images-850/caring-for-reptiles-hero850.jpg"
);