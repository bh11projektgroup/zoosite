INSERT INTO news (title, description, body, creation_date, image_uri) VALUES (
"This may be the biggest turtle that ever lived",
"This jaw-droppingly huge specimen is the largest known complete turtle shell on Earth.",
"An 8-million-year-old turtle shell unearthed in Venezuela measures nearly 8 feet (2.4 meters) long, making it the largest complete turtle shell known to science, a new study reported.
<br />
This shell belonged to an extinct beast called Stupendemys geographicus, which lived in northern South America during the Miocene epoch, which lasted from 12 million to 5 million years ago.
<br />
S. geographicus weighed an estimated 2,500 lbs. (1,145 kilograms), almost 100 times the size of its closest living relative, the Amazon river turtle (Peltocephalus dumerilianus), and twice the size of the largest living turtle, the marine leatherback (Dermochelys coriacea), the researchers wrote in the study.",
"2020-01-18 09:33:00",
"https://cdn.mos.cms.futurecdn.net/rmciW9fWrtEGDFpfWxJQLi-650-80.jpg"
);

INSERT INTO news (title, description, body, creation_date, image_uri) VALUES (
"How did this rare pink manta get its color?",
"Spotted recently off the Great Barrier Reef, the little-seen fish's rosy hue is not due to infection or diet, scientists say.",
"Photographer Kristian Laine was freediving recently off the southernmost island of Australia’s Great Barrier Reef when a bright pink manta ray glided by. He thought for sure that his camera was malfunctioning.
<br />
“I had no idea there were pink mantas in the world, so I was confused and thought my strobes were broken or doing something weird,” says Laine, whose photographs posted on Instagram this week have gone viral. Laine later realized he’d spotted an 11-foot male reef manta ray named after Inspector Clouseau, the bumbling detective of the Pink Panther movies. The fish, who cruises the waters around Lady Elliot Island, is the only known pink manta ray in the world.
<br />
First spotted in 2015, Inspector Clouseau has been seen fewer than 10 times since. “I feel humbled and extremely lucky,” says Laine, who photographed him amid a group of seven other males, all of them vying for a female. (Read how manta rays form close friendships, shattering misconceptions.)",
"2020-02-13 09:45:00",
"https://headtopics.com/images/2020/2/18/globalnews/rare-pink-manta-ray-spotted-by-unsuspecting-photographer-in-great-barrier-reef-1229849663446056960.webp"
);

INSERT INTO news (title, description, body, creation_date, image_uri) VALUES (
"Bumblebees are going extinct in a time of ‘climate chaos’",
"Loss of the vital pollinators, due in part to temperature extremes and fluctuations, could have dire consequences for ecosystems and agriculture.",
"BUMBLEBEES, AMONG THE most important pollinators, are in trouble. Fuzzy and buzzy, they excel at spreading pollen and fertilizing many types of wild flora, as well as crucial agricultural crops like tomatoes, blueberries, and squash.
<br />
But their numbers are dropping. New research using a massive dataset found that the insects are far less common than they used to be; in North America, you are nearly 50 percent less likely to see a bumblebee in any given area than you were prior to 1974.
<br />
Moreover, several once-common species have disappeared from many areas they were once found, becoming locally extinct in those places. For example, the rusty patched bumblebee, which used to flourish in Ontario, is no longer found in all of Canada—in the U.S., it’s endangered.",
"2020-01-18 09:40:00",
"https://www.centerforfoodsafety.org/thumbs/1000x562x50/files/zc/wbid-cms_55978.jpg"
);