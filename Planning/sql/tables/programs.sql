INSERT INTO programs (name, description, location, start_time, duration, picture_uri, capacity) VALUES (
"Delfin show",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae mattis nisl, eu bibendum magna. Morbi sem purus, venenatis vel nunc vel, tristique cursus quam.",
"Lorem ipsum's location.",
"2020-02-13 09:45:00",
"100 perc",
"https://cdn.pixabay.com/photo/2014/07/31/20/21/dolphin-406748_1280.jpg",
200
);

INSERT INTO programs (name, description, location, start_time, duration, picture_uri, capacity) VALUES (
"Állatkertek Éjszakája",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae mattis nisl, eu bibendum magna. Morbi sem purus, venenatis vel nunc vel, tristique cursus quam.",
"Lorem ipsum's location.",
"2020-04-13 20:00:00",
"240 perc",
"https://zoobudapest.com/uploads/collection/1/image/DSC_7750.JPG",
2000
);

INSERT INTO programs (name, description, location, start_time, duration, picture_uri, capacity) VALUES (
"World Water Day",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae mattis nisl, eu bibendum magna. Morbi sem purus, venenatis vel nunc vel, tristique cursus quam.",
"Lorem ipsum's location.",
"2020-03-22 08:00:00",
"",
"https://constructionreviewonline.com/wp-content/uploads/2017/03/UN-World-Water-Day-Summit-commences-in-South-Africa.jpg",
2000
);