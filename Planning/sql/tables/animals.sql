INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
1,
"Gréta",
"female",
12,
"2011-07-12",
"https://cdn.nwmgroups.hu/s/img/i/1505/20150526oroszlan-lion-tekintet-kilatas-kihivas.jpg?w=644&h=362"
);

INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
1,
"Réka",
"female",
18,
"2011-07-12",
"https://cdn.nwmgroups.hu/s/img/i/1505/20150526oroszlan-lion-tekintet-kilatas-kihivas.jpg?w=644&h=362"
);

INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
2,
"Balázs",
"male",
22,
"2013-04-13",
"https://pmdvod.nationalgeographic.com/NG_Video/806/755/lgpost_1564002467927.jpg"
);

INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
3,
"Romolus",
"male",
19,
"2018-04-13",
"https://www.dw.com/image/45189092_303.jpg"
);

INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
3,
"Remus",
"male",
18,
"2018-04-13",
"https://www.dw.com/image/45189092_303.jpg"
);

INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
4,
"Réka",
"female",
18,
"2013-09-13",
"https://images.immediate.co.uk/production/volatile/sites/23/2018/09/Giraffes_Masai-Mara_Manoj-Shah_getty_623-19fe90d.jpg?quality=90&resize=614%2C409"
);

INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
5,
"Ramóna",
"female",
15,
"2019-09-13",
"https://www.dierenbox.nl/wp-content/uploads/2013/08/Bergzebra.jpg"
);

INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
6,
"Megatron",
"male",
4,
"2020-01-13",
"https://i.pinimg.com/originals/c7/cb/3b/c7cb3be624510e977b13baf98a4935e7.jpg"
);

INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
6,
"Tapsi",
"male",
5,
"2019-02-13",
"https://i.pinimg.com/originals/c7/cb/3b/c7cb3be624510e977b13baf98a4935e7.jpg"
);

INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
8,
"Robin",
"female",
10,
"2016-05-13",
"https://cdn.nwmgroups.hu/s/img/i/1307/20130708-solyom-hungarian-airlines-illusztracio-a12.jpg?w=645&h=430"
);

INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
9,
"Béla",
"male",
10,
"2015-05-13",
"https://upload.wikimedia.org/wikipedia/commons/9/93/Orangutans2.jpg"
);

INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
10,
"Bogyó",
"female",
12,
"2012-08-13",
"https://www.erdekesvilag.hu/wp-content/uploads/2013/03/csimp%C3%A1nzok.jpg"
);

INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
11,
"Jani",
"male",
23,
"2015-08-13",
"https://images.immediate.co.uk/production/volatile/sites/23/2014/11/GettyImages-123529247-2a29d6c.jpg?quality=90&resize=620%2C413"
);

INSERT INTO animals (type_id,  name, sex, age, date_of_arrival, profile_picture_uri) VALUES (
12,
"Zsolti",
"male",
1,
"2017-04-10",
"https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Hyla_arborea_hun.jpg/1280px-Hyla_arborea_hun.jpg"
);