-- MySQL Workbench Synchronization
-- Generated: 2020-02-25 11:34
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Niki

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE SCHEMA IF NOT EXISTS `zoosite_db` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE IF NOT EXISTS `zoosite_db`.`zoo_info` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `address` VARCHAR(150) NULL DEFAULT NULL,
  `phone` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(100) NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `opening_hours` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `zoosite_db`.`news` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(200) NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `body` TEXT NULL DEFAULT NULL,
  `creation_date` DATETIME NULL DEFAULT NULL,
  `image_uri` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `zoosite_db`.`staff` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NULL DEFAULT NULL,
  `account_name` VARCHAR(45) NULL DEFAULT NULL,
  `password` VARCHAR(100) NULL DEFAULT NULL,
  `email` VARCHAR(100) NULL DEFAULT NULL,
  `profile_picture_uri` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `account_name_UNIQUE` (`account_name` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `zoosite_db`.`cities` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `postal_code` VARCHAR(10) NULL DEFAULT NULL,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `zoosite_db`.`customers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `account_name` VARCHAR(45) NULL DEFAULT NULL,
  `password` VARCHAR(100) NULL DEFAULT NULL,
  `name` VARCHAR(250) NULL DEFAULT NULL,
  `postal_code` VARCHAR(10) NULL DEFAULT NULL,
  `city` VARCHAR(100) NULL DEFAULT NULL,
  `address` VARCHAR(250) NULL DEFAULT NULL,
  `email` VARCHAR(100) NULL DEFAULT NULL,
  `time_of_registration` DATETIME NULL DEFAULT NULL,
  `profile_picture_uri` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `account_name_UNIQUE` (`account_name` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `zoosite_db`.`animal_classes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `class` VARCHAR(45) NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `profile_picture_uri` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `zoosite_db`.`animal_types` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `class_id` INT(11) NULL DEFAULT NULL,
  `type` VARCHAR(100) NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `profile_picture_uri` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `type_UNIQUE` (`type` ASC) VISIBLE,
  INDEX `class_id_idx` (`class_id` ASC) VISIBLE,
  CONSTRAINT `class_id`
    FOREIGN KEY (`class_id`)
    REFERENCES `zoosite_db`.`animal_classes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `zoosite_db`.`animals` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `type_id` INT(11) NULL DEFAULT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `sex` VARCHAR(10) NULL DEFAULT NULL,
  `age` INT(11) NULL DEFAULT NULL,
  `date_of_arrival` DATE NULL DEFAULT NULL,
  `profile_picture_uri` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `type_id_idx` (`type_id` ASC) VISIBLE,
  CONSTRAINT `type_id`
    FOREIGN KEY (`type_id`)
    REFERENCES `zoosite_db`.`animal_types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `zoosite_db`.`tickets` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `barcode` VARCHAR(100) NOT NULL,
  `program_id` INT(11) NOT NULL,
  `price_huf` INT(11) NULL DEFAULT NULL,
  `customer_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `program_id_idx` (`program_id` ASC) VISIBLE,
  INDEX `fk_tickets_customers1_idx` (`customer_id` ASC) VISIBLE,
  CONSTRAINT `program_id`
    FOREIGN KEY (`program_id`)
    REFERENCES `zoosite_db`.`programs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tickets_customers1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `zoosite_db`.`customers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `zoosite_db`.`programs` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NOT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `location` VARCHAR(200) NULL DEFAULT NULL,
  `start_time` DATETIME NULL DEFAULT NULL,
  `duration` VARCHAR(45) NULL DEFAULT NULL,
  `picture_uri` TEXT NULL DEFAULT NULL,
  `capacity` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `zoosite_db`.`pages` (
  `url` VARCHAR(100) NOT NULL,
  `source_file` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`url`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `zoosite_db`.`images` (
  `name` VARCHAR(100) NOT NULL,
  `alt` VARCHAR(100) NULL DEFAULT NULL,
  `path` VARCHAR(250) NULL DEFAULT NULL,
  `thumbnail_path` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`name`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
